import PyQt5.QtWidgets as QtW
import PyQt5.QtCore as QtC
import PyQt5.QtGui as QtG
from enum import Enum
import sys


class Ticket:
    class Zone(Enum):
        Aglo = 0
        City = 1
    class Discount(Enum):
        Discounted = 0
        Normal = 1
    class TType(Enum):
        t1_ride = 0
        t1_or_40min = 1
        t20min = 2
        t60min = 3
        t90min = 4
        t24h = 5
        t48h = 6
        t72h = 7
        t7days = 8
        tweekend_family = 9
        tgroup = 10
        t2_rides = 11

    prices = None
    names = {
        TType.t1_ride : "Jednorazowy",
        TType.t1_or_40min : "Jednorazowy\nlub 40 minut",
        TType.t20min : "20-minutowy",
        TType.t60min : "60-minutowy",
        TType.t90min : "90-minutowy",
        TType.t24h : "24-godzinny",
        TType.t48h : "48-godzinny",
        TType.t72h : "72-godzinny",
        TType.t7days : "7-dniowy",
        TType.tweekend_family : "Weekendowy rodzinny",
        TType.tgroup : "Grupowy",
        TType.t2_rides : "2-przejazdowy"
    }

    def __init__(self, zone, discount, t_type):
        self.zone = zone
        self.discount = discount
        self.t_type = t_type
        self.price = Ticket.prices[(self.zone, self.discount, self.t_type)]

    def token(self):
        return self.zone, self.discount, self.t_type

    @staticmethod
    def get_ticket_prices():
        ticket_bin = dict()
        # --->1 ride
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Normal, Ticket.TType.t1_ride)] = 400
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Discounted, Ticket.TType.t1_ride)] = 200
        # --->1 ride / 40 minutes
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Normal, Ticket.TType.t1_or_40min)] = 380
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.t1_or_40min)] = 190
        # --->20 minutes
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Normal, Ticket.TType.t20min)] = 280
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.t20min)] = 140
        # --->60 minutes
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Normal, Ticket.TType.t60min)] = 500
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.t60min)] = 250
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Normal, Ticket.TType.t60min)] = 500
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Discounted, Ticket.TType.t60min)] = 250
        # --->90 minutes
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Normal, Ticket.TType.t90min)] = 600
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.t90min)] = 300
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Normal, Ticket.TType.t90min)] = 600
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Discounted, Ticket.TType.t90min)] = 300
        # --->1 day
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Normal, Ticket.TType.t24h)] = 1500
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.t24h)] = 750
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Normal, Ticket.TType.t24h)] = 2000
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Discounted, Ticket.TType.t24h)] = 1000
        # --->2 days
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Normal, Ticket.TType.t48h)] = 2400
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.t48h)] = 1200
        # --->3 days
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Normal, Ticket.TType.t72h)] = 3600
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.t72h)] = 1800
        # --->7 days
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Normal, Ticket.TType.t7days)] = 4800
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.t7days)] = 2400
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Normal, Ticket.TType.t7days)] = 6200
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Discounted, Ticket.TType.t7days)] = 3100
        # --->Family
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Normal, Ticket.TType.tweekend_family)] = 1600
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.tweekend_family)] = 1600
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Normal, Ticket.TType.tweekend_family)] = 1600
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Discounted, Ticket.TType.tweekend_family)] = 1600
        # --->Group
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Normal, Ticket.TType.tgroup)] = 3600
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.tgroup)] = 1800
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Normal, Ticket.TType.tgroup)] = 4600
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Discounted, Ticket.TType.tgroup)] = 2300
        # --->2 rides
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Normal, Ticket.TType.t2_rides)] = 720
        ticket_bin[(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.t2_rides)] = 360
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Normal, Ticket.TType.t2_rides)] = 760
        ticket_bin[(Ticket.Zone.Aglo, Ticket.Discount.Discounted, Ticket.TType.t2_rides)] = 380
        return ticket_bin

    def get_label_string(self):
        price = Ticket.prices[self.token()]
        price_f = TicketMachine.format_money(price)

        type_name = Ticket.names[self.t_type]

        if self.zone == Ticket.Zone.Aglo:
            zone_name = "Strefa I+II Aglomeracja"
        else:
            zone_name = "Strefa I Miasto Kraków"

        if self.discount == Ticket.Discount.Discounted:
            discount_name = "Ulgowy"
        else:
            discount_name = "Normalny"

        full_ticket_name_string = type_name + "\n" + zone_name + "\n" + discount_name + "\n" + "(" + price_f + ")"

        return full_ticket_name_string

    def get_choice_label_string(self):
        type_name = Ticket.names[self.t_type]
        price = Ticket.prices[self.token()]
        price_f = TicketMachine.format_money(price)

        return type_name + "\n" + "(" + price_f + ")"

    def get_output_string(self):
        price = Ticket.prices[self.token()]
        price_f = TicketMachine.format_money(price)

        type_name = Ticket.names[self.t_type]

        if self.discount == Ticket.Discount.Discounted:
            discount_name = "Ulgowy"
        else:
            discount_name = "Normalny"

        if self.zone == Ticket.Zone.Aglo:
            zone_name = "Aglomeracyjny"
        else:
            zone_name = "Miejski"

        full_output_string = type_name + ", " + discount_name + ", " + zone_name
        return full_output_string


class TicketMachine(QtW.QWidget):
    cart = dict()
    pillables = list()
    total = 0
    money_in = dict()
    money_difference = 0


    # ~* Stylesheets *~
    language_stylesheet_pl_text = """
        QRadioButton::unchecked {border-image: url(./images/button-flag-pl-grey.png)}
        QRadioButton::checked {border-image: url(./images/button-flag-pl.png)}
        QRadioButton::indicator { background-color: transparent; background: transparent }
        """

    language_stylesheet_en_text = """
        QRadioButton::unchecked {border-image: url(./images/button-flag-en-grey.png)}
        QRadioButton::checked {border-image: url(./images/button-flag-en.png)}
        QRadioButton::indicator { background-color: transparent; background: transparent }
        """

    ticket_city_stylesheet_text = """
                        border-image: url(./images/button-ticket-I.png)
                        """

    ticket_aglo_stylesheet_text = """
                        border-image: url(./images/button-ticket-II.png)
                        """

    button_stylesheet_text = """
                        border-image: url(./images/button-white2.png); color: black; font-size: 18px; 
                        """

    arrow_button_stylesheet_text = """
                        color: black; font-size: 58px;
                        border-image: url(./images/button-white2.png)
                        """

    label_stylesheet_text = """
                        border-image: url(./images/desciption.png); color: black; font-size: 18px;
                        """

    label_wybrane_stylesheet_text = """                        
                        QLabel { color: white; font-size: 42px; text-align: right;
                                border-image: url(./images/desciption2.png)
                                } 
                        """

    label_total_white_bg_stylesheet_text = """                        
                        QLabel { color: black; font-size: 18px; text-align: right;
                                border-image: url(./images/tm-price-white.png)
                                } 
                        """

    ticket_count_stylesheet_text = """
                                color: black; font-size: 18px;
                                border-image: url(./images/button-num-of-tickets.png)
                                """

    button_one_by_one_stylesheet_text = """
                                color: black; font-size: 58px;
                                border-image: url(./images/button-plus.png)
                                """

    button_long_red_stylesheet_text = """
                                color: white; font-size: 18px;
                                border-image: url(./images/button-total-cancel.png)
                                """

    button_short_red_remove_stylesheet_text = """
                                color: white; font-size: 58px;
                                border-image: url(./images/button-cancel.png)
                                """

    card_empty_stylesheet_text = """
                                QPushButton::enabled {border-image: url(./images/tm-card-no-money.png)}
                                QPushButton::disabled {border-image: url(./images/tm-card-no-money2.png)}
                                """

    card_full_stylesheet_text = """
                                QPushButton::enabled {border-image: url(./images/tm-card-money.png)}
                                QPushButton::disabled {border-image: url(./images/tm-card-money2.png)}
                                """

    left_to_pay_stylesheet_text = """
                        border-image: url(./images/price.png); color: black; font-size: 18px;
                        """


    def __init__(self, flags, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)
        # ~* Set up static ticket properties *~
        Ticket.prices = Ticket.get_ticket_prices()

        self.output_line = 0

        # ~* Initialize cart with zeroes *~
        self.purge_cart(True)
        self.reset_money()
        print(TicketMachine.money_in)

        # ~* Create and set the layout *~
        tm_all_layout = QtW.QHBoxLayout()
        self.setLayout(tm_all_layout)

        # ~* Init the virtual screen *~
        page_width = 650
        page_height = 980

        self.tm_virtual_screen = QtW.QStackedWidget()
        self.choice_page = ChoicePage(flags=None)
        self.choice_page.setFixedSize(page_width, page_height)
        self.summary_page = SummaryPage(flags=None, i_tm_link=self)
        self.summary_page.setFixedSize(page_width, page_height)
        self.payment_page = PaymentPage(flags=None)
        self.payment_page.setFixedSize(page_width, page_height)
        self.result_page = ResultOfOperationPage(self)
        self.result_page.setFixedSize(page_width, page_height)

        self.tm_virtual_screen.addWidget(self.choice_page)
        self.tm_virtual_screen.addWidget(self.summary_page)
        self.tm_virtual_screen.addWidget(self.payment_page)
        self.tm_virtual_screen.addWidget(self.result_page)
        self.tm_virtual_screen.setCurrentWidget(self.choice_page)  # STARTING PAGE!

        tm_all_layout.addWidget(self.tm_virtual_screen)  # Add to Layout

        # ~* Connect SIGNALS! *~
        self.summary_page.add_ticket_button.clicked.connect(
            lambda: self.tm_virtual_screen.setCurrentWidget(self.choice_page))
        self.summary_page.add_ticket_button_red.clicked.connect(
            lambda: self.tm_virtual_screen.setCurrentWidget(self.choice_page))
        self.choice_page.go_to_summary_button.clicked.connect(self.go_to_summary)

        self.summary_page.pay_cash_button.clicked.connect(lambda: self.go_to_payment(True))
        self.summary_page.pay_card_button.clicked.connect(lambda: self.go_to_payment(False))

        self.payment_page.back_to_summary_button.clicked.connect(self.go_to_summary)


        self.choice_page.ticket_add.clicked.connect(lambda:
                                                self.add_ticket(self.choice_page.selected_ticket.token()))
        self.choice_page.ticket_remove.clicked.connect(lambda:
                                                       self.remove_ticket(self.choice_page.selected_ticket.token()))
        self.choice_page.ticket_clear.clicked.connect(lambda:
                                                      self.purge_ticket(self.choice_page.selected_ticket.token()))

        # ~* Money section *~
        coin_stylesheet_text = """
        QPushButton::enabled {border-image: url(./images/tm-coin.png); color: black; font-size: 26px; }
        QPushButton::disabled {border-image: url(./images/tm-coin-grey.png); color: black; font-size: 26px; }
        """

        tm_money_section = QtW.QGridLayout()
        self.money_items = list()
        self.coins_items = list()
        self.cards_items = list()
        tm_all_layout.addLayout(tm_money_section)

        coin_10gr = QtW.QPushButton("10gr")
        coin_10gr.setFixedSize(100, 100)
        coin_10gr.setStyleSheet(coin_stylesheet_text)
        tm_money_section.addWidget(coin_10gr, 0, 0, 1, 1)
        self.money_items.append(coin_10gr)
        self.coins_items.append(coin_10gr)
        coin_10gr.clicked.connect(lambda: self.insert_money(10))

        coin_20gr = QtW.QPushButton("20gr")
        coin_20gr.setFixedSize(100, 100)
        coin_20gr.setStyleSheet(coin_stylesheet_text)
        tm_money_section.addWidget(coin_20gr, 0, 1, 1, 1)
        self.money_items.append(coin_20gr)
        self.coins_items.append(coin_20gr)
        coin_20gr.clicked.connect(lambda: self.insert_money(20))

        coin_50gr = QtW.QPushButton("50gr")
        coin_50gr.setFixedSize(100, 100)
        coin_50gr.setStyleSheet(coin_stylesheet_text)
        tm_money_section.addWidget(coin_50gr, 0, 2, 1, 1)
        self.money_items.append(coin_50gr)
        self.coins_items.append(coin_50gr)
        coin_50gr.clicked.connect(lambda: self.insert_money(50))

        coin_1zl = QtW.QPushButton("1zł")
        coin_1zl.setFixedSize(100, 100)
        coin_1zl.setStyleSheet(coin_stylesheet_text)
        tm_money_section.addWidget(coin_1zl, 1, 0, 1, 1)
        self.money_items.append(coin_1zl)
        self.coins_items.append(coin_1zl)
        coin_1zl.clicked.connect(lambda: self.insert_money(100))

        coin_2zl = QtW.QPushButton("2zł")
        coin_2zl.setFixedSize(100, 100)
        coin_2zl.setStyleSheet(coin_stylesheet_text)
        tm_money_section.addWidget(coin_2zl, 1, 1, 1, 1)
        self.money_items.append(coin_2zl)
        self.coins_items.append(coin_2zl)
        coin_2zl.clicked.connect(lambda: self.insert_money(200))

        coin_5zl = QtW.QPushButton("5zł")
        coin_5zl.setFixedSize(100, 100)
        coin_5zl.setStyleSheet(coin_stylesheet_text)
        tm_money_section.addWidget(coin_5zl, 1, 2, 1, 1)
        self.money_items.append(coin_5zl)
        self.coins_items.append(coin_5zl)
        coin_5zl.clicked.connect(lambda: self.insert_money(500))

        card_empty = get_styled_button("", 1, TicketMachine.card_empty_stylesheet_text)
        tm_money_section.addWidget(card_empty, 2, 0, 1, 1)
        self.money_items.append(card_empty)
        self.cards_items.append(card_empty)
        card_empty.clicked.connect(self.transaction_card_failure)

        card_full = get_styled_button("", 1, TicketMachine.card_full_stylesheet_text)
        tm_money_section.addWidget(card_full, 2, 1, 1, 1)
        self.money_items.append(card_full)
        self.cards_items.append(card_full)
        card_full.clicked.connect(self.transaction_done_card_success)

        # ~* Machine output *~
        self.tm_output_part = QtW.QPlainTextEdit()
        self.tm_output_part.setReadOnly(True)
        tm_all_layout.addWidget(self.tm_output_part)

        # ~* Connect LANGUAGE buttons *~
        self.choice_page.language_polish_button.toggled.connect(lambda tog: self.set_language(tog))
        self.choice_page.language_english_button.toggled.connect(lambda tog: self.set_language(not tog))

        self.summary_page.language_polish_button.toggled.connect(lambda tog: self.set_language(tog))
        self.summary_page.language_english_button.toggled.connect(lambda tog: self.set_language(not tog))

        self.payment_page.language_polish_button.toggled.connect(lambda tog: self.set_language(tog))
        self.payment_page.language_english_button.toggled.connect(lambda tog: self.set_language(not tog))

        # ~* Ready state up *~
        self.set_language(True)
        self.summary_page.update_pills()
        self.payment_page.update_pills()
        self.update_total_and_display()
        self.update_balance_and_display()
        self.choice_page.update_ticket_button_labels()
        for mi in self.money_items:
            mi.setEnabled(False)

    def set_money_section_enabled(self, isEnabled):
        for money_item in self.money_items:
            money_item.setEnabled(isEnabled)

    def set_language(self, isPolish : bool):
        if self.choice_page.language_polish_button.isChecked() != isPolish\
                or self.summary_page.language_polish_button.isChecked() != isPolish\
                or self.payment_page.language_polish_button.isChecked() != isPolish:
            self.choice_page.language_polish_button.setChecked(isPolish)
            self.summary_page.language_polish_button.setChecked(isPolish)
            self.payment_page.language_polish_button.setChecked(isPolish)

            self.choice_page.language_english_button.setChecked(not isPolish)
            self.summary_page.language_english_button.setChecked(not isPolish)
            self.payment_page.language_english_button.setChecked(not isPolish)


    def purge_cart(self, just_init = False):
        for valid_ticket_type in Ticket.prices.keys():
            self.cart[valid_ticket_type] = 0
            if not just_init:
                self.choice_page.update_ticket_count()
                self.update_total_and_display()
                self.update_balance_and_display()
                self.update_pillables()
                self.tm_virtual_screen.setCurrentWidget(self.choice_page)

    @staticmethod
    def get_total():
        total = 0
        for ticket in Ticket.prices.keys():
            total += TicketMachine.cart[ticket] * Ticket.prices[ticket]
        TicketMachine.total = total
        return total

    def add_ticket(self, ticket_token):
        TicketMachine.cart[ticket_token] += 1
        self.choice_page.update_ticket_count()
        self.update_total_and_display()
        self.update_balance_and_display()
        self.update_summary_button()
        print(" + " +str(ticket_token), TicketMachine.cart[ticket_token])

    def remove_ticket(self, ticket_token):
        if TicketMachine.cart[ticket_token] > 0:
            TicketMachine.cart[ticket_token] -= 1
            self.choice_page.update_ticket_count()
            self.update_total_and_display()
            self.update_balance_and_display()
            self.update_summary_button()
            print(" - " + str(ticket_token), TicketMachine.cart[ticket_token])

    def purge_ticket(self, ticket_token):
        TicketMachine.cart[ticket_token] = 0
        self.choice_page.update_ticket_count()
        self.update_total_and_display()
        self.update_balance_and_display()
        self.update_pillables()
        self.update_summary_button()
        print(" x " + str(ticket_token), TicketMachine.cart[ticket_token])

    def reset_money(self):
        denominations = (10, 20, 50, 100, 200, 500)
        for cur_den in denominations:
            TicketMachine.money_in[cur_den] = 0
        TicketMachine.total = 0

    def update_summary_button(self):
        total = TicketMachine.get_total()
        if total > 0:
            self.choice_page.go_to_summary_button.setVisible(True)
        else:
            self.choice_page.go_to_summary_button.setVisible(False)

    def insert_money(self, money_denomination):
        TicketMachine.money_in[money_denomination] += 1
        print("Money inserted: " + str(money_denomination))
        print("Money in: " + str(self.get_balance()))
        # Perform a chack! If enough has been paid, finalize transaction
        self.check_money_in_tm()

    def check_money_in_tm(self):
        money_in = TicketMachine.get_balance()
        self.update_total_and_display()
        self.update_balance_and_display()
        if money_in >= TicketMachine.total:
            self.transaction_done_money_success()

    @staticmethod
    def get_balance():
        balance = 0
        for coin in TicketMachine.money_in.keys():
            balance += coin * TicketMachine.money_in[coin]
        return balance

    def update_balance_and_display(self):
        TicketMachine.total = self.get_total()
        TicketMachine.money_difference = TicketMachine.total - self.get_balance()

        self.payment_page.to_pay_box.setText(TicketMachine.format_money(TicketMachine.money_difference))

    def update_total_and_display(self):
        TicketMachine.total = self.get_total()
        formatted_total = TicketMachine.format_money(TicketMachine.total)
        self.summary_page.total_display.setText(formatted_total)
        self.payment_page.total_price_box.setText(formatted_total)


    @staticmethod
    def format_money(money_int):
        zl_val = money_int//100
        gr_val = money_int % 100

        zl_part = str(zl_val)
        gr_part = str(gr_val).zfill(2)
        total_str = zl_part + "," + gr_part + "zł"

        # if zl_val > 0:
        #     zl_part = str(zl_val) + "zł"
        # if gr_val > 0:
        #     gr_part = str(gr_val) + "gr"
        # total_str = zl_part + " " +gr_part
        # total_str = total_str.strip()

        return total_str

    def print_output(self, string_to_print):
        o_line_str = str(self.output_line).zfill(4)
        output_cur_text = self.tm_output_part.toPlainText()
        self.tm_output_part.setPlainText(o_line_str + "|" + string_to_print + "\n" + output_cur_text)
        self.output_line += 1

    def give_change(self, money):
        denominations = (1, 2, 5, 10, 20, 50, 100, 200, 500)
        change_dict = dict()
        for cur_denom in range(len(denominations)-1, -1, -1):
            coin_value = denominations[cur_denom]
            change_dict[coin_value] = 0
            while(money >= coin_value):
                change_dict[coin_value] += 1
                money -= coin_value
            if (change_dict[coin_value] > 0):
                # TODO: use OUTPUT to give this out somehow
                coin_value_formatted = TicketMachine.format_money(coin_value)
                change_string = "Wydana reszta: " + str(coin_value_formatted)\
                                + " x" + str(change_dict[coin_value])
                self.print_output(change_string)

    def give_tickets(self):
        for ticket in TicketMachine.cart.keys():
            while TicketMachine.cart[ticket] > 0:
                ticket_string = "Wydano bilet: " + Ticket(*ticket).get_output_string()
                self.print_output(ticket_string)
                self.remove_ticket(ticket)

    def transaction_done_money_success(self):
        # Disable all money
        for mi in self.money_items:
            mi.setEnabled(False)

        # Eat money
        change = TicketMachine.get_balance() - TicketMachine.total
        self.reset_money()
        # Spit out tickets
        self.give_tickets()
        # Give change
        self.give_change(change)
        # Empty cart
        self.purge_cart()
        # Print separator
        self.print_output("~* KONIEC TRANSAKCJI *~")
        self.print_output("=================")
        # Back to selection
        self.result_page.morph(ResultOfOperationPage.ResultType.CoinsSuccess)
        self.tm_virtual_screen.setCurrentWidget(self.result_page)

    def transaction_done_card_success(self):
        # Disable all money
        for mi in self.money_items:
            mi.setEnabled(False)

        # Give change
        change = TicketMachine.get_balance() - TicketMachine.total
        self.reset_money()
        self.give_change(change)

        self.give_tickets()
        self.purge_cart()
        # Print separator
        self.print_output("~* KONIEC TRANSAKCJI *~")
        self.print_output("=================")
        # Back to selection
        self.result_page.morph(ResultOfOperationPage.ResultType.CardSuccess)
        self.tm_virtual_screen.setCurrentWidget(self.result_page)

    def transaction_card_failure(self):
        # Disable all money
        for mi in self.money_items:
            mi.setEnabled(False)

        self.result_page.morph(ResultOfOperationPage.ResultType.CardFailure)
        self.tm_virtual_screen.setCurrentWidget(self.result_page)

    def go_to_summary(self):
        for mi in self.money_items:
            mi.setEnabled(False)
        self.update_pillables()
        self.summary_page.update_pills()
        # self.summary_page.update()
        self.summary_page.update_arrow_existence()
        self.summary_page.update_payment_possibility()
        if len(self.pillables) > 0:
            self.tm_virtual_screen.setCurrentWidget(self.summary_page)
        else:
            print("No tickets selected!")

    def go_to_payment(self, usesCoins: bool):
        coin_payment_text = "Płatność\ngotówką"
        card_payment_text = "Płatność\nkartą"

        self.update_pillables()
        self.summary_page.update_pills()
        self.payment_page.update_pills()
        if usesCoins:
            payment_type_text = coin_payment_text
            for coin in self.coins_items:
                coin.setEnabled(True)
            self.payment_page.usesCoins = True
        else:
            payment_type_text = card_payment_text
            for card in self.cards_items:
                card.setEnabled(True)
            self.payment_page.usesCoins = False

        self.payment_page.type_of_payment.setText(payment_type_text)
        self.tm_virtual_screen.setCurrentWidget(self.payment_page)
        self.payment_page.update_arrow_existence()
        self.check_money_in_tm()

    def update_pillables(self):
        # print("Pillable list:")
        TicketMachine.pillables = list()
        for ticket in self.cart.keys():
            if self.cart[ticket] > 0:
                # print(ticket)
                TicketMachine.pillables.append(ticket)


class ChoicePage(QtW.QWidget):
    def __init__(self, flags, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)
        self.topsel = 0
        self.midsel = 0
        self.selected_ticket = Ticket(Ticket.Zone.City, Ticket.Discount.Discounted, Ticket.TType.t24h)

        self.tm_choice_page_layout = QtW.QGridLayout()
        self.setLayout(self.tm_choice_page_layout)

        qradiobutton_stylesheet_text = """
            QRadioButton::unchecked {border-image: url(./images/button-white2.png); color: black; text-align: center; }
            QRadioButton::checked {border-image: url(./images/button-blue2.png); color: white; text-align: center; }
            QRadioButton::indicator { background-color: transparent; background: transparent }
            QRadioButton { font-size: 18px; text-align: center; } 
            """

        language_stylesheet_pl_text = """
            QRadioButton::unchecked {border-image: url(./images/button-flag-pl-grey.png)}
            QRadioButton::checked {border-image: url(./images/button-flag-pl.png)}
            QRadioButton::indicator { background-color: transparent; background: transparent }
            """

        language_stylesheet_en_text = """
            QRadioButton::unchecked {border-image: url(./images/button-flag-en-grey.png)}
            QRadioButton::checked {border-image: url(./images/button-flag-en.png)}
            QRadioButton::indicator { background-color: transparent; background: transparent }
            """

        # ~* TOP STUFF *~
        # =====================================================
        # ~* Language Buttons *~
        self.BG_language_buttons = QtW.QButtonGroup(self)
        # --> POLISH
        self.language_polish_button = get_styled_radio_button("", 2, TicketMachine.language_stylesheet_pl_text)
        self.BG_language_buttons.addButton(self.language_polish_button)
        self.tm_choice_page_layout.addWidget(self.language_polish_button, 0, 0, 1, 2)
        # --> ENGLISH
        self.language_english_button = get_styled_radio_button("", 2, TicketMachine.language_stylesheet_en_text)
        self.BG_language_buttons.addButton(self.language_english_button)
        self.tm_choice_page_layout.addWidget(self.language_english_button, 0, 4, 1, 2)

        # =====================================================
        # ~* Zone Buttons *~
        self.BG_zone_buttons = QtW.QButtonGroup(self)
        # --> AGLO
        self.zone_aglo_button = QtW.QRadioButton("Aglomeracja")
        self.zone_aglo_button.setFixedSize(200, 100)
        self.BG_zone_buttons.addButton(self.zone_aglo_button)
        self.tm_choice_page_layout.addWidget(self.zone_aglo_button, 1, 0, 1, 2)
        self.zone_aglo_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.zone_aglo_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        # --> CITY
        self.zone_city_button = QtW.QRadioButton("Miasto")
        self.zone_city_button.setFixedSize(200, 100)
        self.BG_zone_buttons.addButton(self.zone_city_button)
        self.tm_choice_page_layout.addWidget(self.zone_city_button, 1, 2, 1, 2)
        self.zone_city_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.zone_city_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)

        self.zone_aglo_button.clicked.connect(self.zone_button_clicked)
        self.zone_city_button.clicked.connect(self.zone_button_clicked)

        # =====================================================
        # ~* Discount Buttons *~
        self.BG_disc_buttons = QtW.QButtonGroup(self)
        # --> DISCOUNTED
        self.discount_disc_button = QtW.QRadioButton("Ulgowy")
        self.discount_disc_button.setFixedSize(200, 100)
        self.BG_disc_buttons.addButton(self.discount_disc_button)
        self.tm_choice_page_layout.addWidget(self.discount_disc_button, 2, 0, 1, 2)
        self.discount_disc_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.discount_disc_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        # --> NORMAL
        self.discount_normal_button = QtW.QRadioButton("Normalny")
        self.discount_normal_button.setFixedSize(200, 100)
        self.BG_disc_buttons.addButton(self.discount_normal_button)
        self.tm_choice_page_layout.addWidget(self.discount_normal_button, 2, 2, 1, 2)
        self.discount_normal_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.discount_normal_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)

        self.discount_disc_button.clicked.connect(self.select_discout)
        self.discount_normal_button.clicked.connect(self.select_discout)

        # =====================================================
        # ~* Top Selection Buttons *~
        self.BG_topsel_buttons = QtW.QButtonGroup(self)
        # --> 1-time
        topsel_one_button = QtW.QRadioButton("1-przejazdowe")
        topsel_one_button.setFixedSize(200, 100)
        self.BG_topsel_buttons.addButton(topsel_one_button)
        self.tm_choice_page_layout.addWidget(topsel_one_button, 3, 0, 1, 2)
        topsel_one_button.setStyleSheet(qradiobutton_stylesheet_text)
        topsel_one_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)

        topsel_one_button.clicked.connect(lambda: self.topsel_selected(0))

        # --> 2-time
        self.topsel_two_button = QtW.QRadioButton("2-przejazdowy")
        self.topsel_two_button.setFixedSize(200, 100)
        self.BG_topsel_buttons.addButton(self.topsel_two_button)
        self.tm_choice_page_layout.addWidget(self.topsel_two_button, 3, 2, 1, 2)
        self.topsel_two_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.topsel_two_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)

        self.topsel_two_button.clicked.connect(lambda: self.topsel_selected(1))

        # --> Group
        topsel_group_button = QtW.QRadioButton("Grupowe")
        topsel_group_button.setFixedSize(200, 100)
        topsel_group_button.setSizePolicy(QtW.QSizePolicy.Fixed, QtW.QSizePolicy.Fixed)
        self.BG_topsel_buttons.addButton(topsel_group_button)
        self.tm_choice_page_layout.addWidget(topsel_group_button, 3, 4, 1, 2)
        topsel_group_button.setStyleSheet(qradiobutton_stylesheet_text)

        topsel_group_button.clicked.connect(lambda: self.topsel_selected(2))

        # =====================================================
        # ~* Mid Selection Buttons *~
        self.BG_midsel_one_buttons = QtW.QButtonGroup(self)
        # --> 1-ride flavours
        self.one_flavours_list = list()
        self.one_one_time_button = QtW.QRadioButton("Jednorazowy")
        self.one_one_time_button.setFixedSize(200, 100)
        self.one_one_time_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.one_one_time_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.BG_midsel_one_buttons.addButton(self.one_one_time_button)
        self.one_flavours_list.append(self.one_one_time_button)
        self.tm_choice_page_layout.addWidget(self.one_one_time_button, 4, 0, 1, 2)

        self.one_one_time_button.clicked.connect(lambda: self.midsel_selected(0))

        one_time_pass_button = QtW.QRadioButton("Czasowe")
        one_time_pass_button.setFixedSize(200, 100)
        one_time_pass_button.setStyleSheet(qradiobutton_stylesheet_text)
        one_time_pass_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.one_flavours_list.append(one_time_pass_button)
        self.BG_midsel_one_buttons.addButton(one_time_pass_button)
        self.tm_choice_page_layout.addWidget(one_time_pass_button, 4, 2, 1, 2)

        one_time_pass_button.clicked.connect(lambda: self.midsel_selected(1))

        one_day_pass_button = QtW.QRadioButton("Dobowe")
        one_day_pass_button.setFixedSize(200, 100)
        one_day_pass_button.setStyleSheet(qradiobutton_stylesheet_text)
        one_day_pass_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.one_flavours_list.append(one_day_pass_button)
        self.BG_midsel_one_buttons.addButton(one_day_pass_button)
        self.tm_choice_page_layout.addWidget(one_day_pass_button, 4, 4, 1, 2)

        one_day_pass_button.clicked.connect(lambda: self.midsel_selected(2))

        # ----> 1-ride, one-time
        # one_time_ticket_button = QtW.QPushButton("Jednorazowy")
        # self.tm_choice_page_layout.addWidget(one_time_ticket_button, 5, 0, 1, 2)

        # one_time_40min_ticket_button = QtW.QPushButton("Jednorazowy lub 40 minut")
        # self.tm_choice_page_layout.addWidget(one_time_40min_ticket_button, 5, 0, 1, 2)
        # ----> 1-ride, time-pass
        self.BG_botsel_time_buttons = QtW.QButtonGroup(self)
        self.time_pass_flavours_list = list()

        self.one_20_minutes_ticket_button = QtW.QRadioButton("20-minutowy")
        self.one_20_minutes_ticket_button.setFixedSize(200, 100)
        self.one_20_minutes_ticket_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.one_20_minutes_ticket_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.BG_botsel_time_buttons.addButton(self.one_20_minutes_ticket_button)
        self.time_pass_flavours_list.append(self.one_20_minutes_ticket_button)
        self.tm_choice_page_layout.addWidget(self.one_20_minutes_ticket_button, 5, 0, 1, 2)

        self.one_40_minutes_ticket_button = QtW.QRadioButton("40 minut\nlub Jednorazowy")
        self.one_40_minutes_ticket_button.setFixedSize(200, 100)
        self.one_40_minutes_ticket_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.one_40_minutes_ticket_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.BG_botsel_time_buttons.addButton(self.one_40_minutes_ticket_button)
        self.time_pass_flavours_list.append(self.one_40_minutes_ticket_button)
        self.tm_choice_page_layout.addWidget(self.one_40_minutes_ticket_button, 5, 2, 1, 2)

        self.one_60_minutes_ticket_button = QtW.QRadioButton("60-minutowy")
        self.one_60_minutes_ticket_button.setFixedSize(200, 100)
        self.one_60_minutes_ticket_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.one_60_minutes_ticket_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.BG_botsel_time_buttons.addButton(self.one_60_minutes_ticket_button)
        self.time_pass_flavours_list.append(self.one_60_minutes_ticket_button)
        self.tm_choice_page_layout.addWidget(self.one_60_minutes_ticket_button, 5, 4, 1, 2)

        self.one_90_minutes_ticket_button = QtW.QRadioButton("90-minutowy")
        self.one_90_minutes_ticket_button.setFixedSize(200, 100)
        self.one_90_minutes_ticket_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.one_90_minutes_ticket_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.BG_botsel_time_buttons.addButton(self.one_90_minutes_ticket_button)
        self.time_pass_flavours_list.append(self.one_90_minutes_ticket_button)
        self.tm_choice_page_layout.addWidget(self.one_90_minutes_ticket_button, 6, 0, 1, 2)
        # ----> 1-ride, day-pass
        self.BG_botsel_day_buttons = QtW.QButtonGroup(self)
        self.day_pass_flavours_list = list()

        self.one_24h_ticket_button = QtW.QRadioButton("24-godzinny")
        self.one_24h_ticket_button.setFixedSize(200, 100)
        self.one_24h_ticket_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.one_24h_ticket_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.day_pass_flavours_list.append(self.one_24h_ticket_button)
        self.BG_botsel_day_buttons.addButton(self.one_24h_ticket_button)
        self.tm_choice_page_layout.addWidget(self.one_24h_ticket_button, 5, 0, 1, 2)

        self.one_48h_ticket_button = QtW.QRadioButton("48-godzinny")
        self.one_48h_ticket_button.setFixedSize(200, 100)
        self.one_48h_ticket_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.one_48h_ticket_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.day_pass_flavours_list.append(self.one_48h_ticket_button)
        self.BG_botsel_day_buttons.addButton(self.one_48h_ticket_button)
        self.tm_choice_page_layout.addWidget(self.one_48h_ticket_button, 5, 2, 1, 2)

        self.one_72h_ticket_button = QtW.QRadioButton("72-godzinny")
        self.one_72h_ticket_button.setFixedSize(200, 100)
        self.one_72h_ticket_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.one_72h_ticket_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.day_pass_flavours_list.append(self.one_72h_ticket_button)
        self.BG_botsel_day_buttons.addButton(self.one_72h_ticket_button)
        self.tm_choice_page_layout.addWidget(self.one_72h_ticket_button, 5, 4, 1, 2)

        self.one_7day_ticket_button = QtW.QRadioButton("7-dniowy")
        self.one_7day_ticket_button.setFixedSize(200, 100)
        self.one_7day_ticket_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.one_7day_ticket_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.day_pass_flavours_list.append(self.one_7day_ticket_button)
        self.BG_botsel_day_buttons.addButton(self.one_7day_ticket_button)
        self.tm_choice_page_layout.addWidget(self.one_7day_ticket_button, 6, 0, 1, 2)
        # --> Group flavours
        self.group_flavours_list = list()
        self.group_family_ticket_button = QtW.QRadioButton("Rodzinny")
        self.group_family_ticket_button.setFixedSize(200, 100)
        self.group_family_ticket_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.group_family_ticket_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.group_flavours_list.append(self.group_family_ticket_button)
        self.tm_choice_page_layout.addWidget(self.group_family_ticket_button, 4, 0, 1, 2)

        self.group_group_ticket_button = QtW.QRadioButton("Do 20 osób")
        self.group_group_ticket_button.setFixedSize(200, 100)
        self.group_group_ticket_button.setStyleSheet(qradiobutton_stylesheet_text)
        self.group_group_ticket_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.group_flavours_list.append(self.group_group_ticket_button)
        self.tm_choice_page_layout.addWidget(self.group_group_ticket_button, 4, 2, 1, 2)

        # ---> CONNECT ticket selection signals to ticket selection slot!

        #one_one_time_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.t1_or_40min))

        self.one_20_minutes_ticket_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.t20min))
        self.one_40_minutes_ticket_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.t1_or_40min))
        self.one_60_minutes_ticket_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.t60min))
        self.one_90_minutes_ticket_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.t90min))

        self.one_24h_ticket_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.t24h))
        self.one_48h_ticket_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.t48h))
        self.one_72h_ticket_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.t72h))
        self.one_7day_ticket_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.t7days))

        self.topsel_two_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.t2_rides))

        self.group_family_ticket_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.tweekend_family))
        self.group_group_ticket_button.clicked.connect(lambda: self.select_ticket(Ticket.TType.tgroup))

        # Aglo-exclusion group
        self.aglo_exclusion_list = list()
        self.aglo_exclusion_list.extend([self.one_20_minutes_ticket_button, self.one_40_minutes_ticket_button,
                                         self.one_48h_ticket_button, self.one_72h_ticket_button])

        # The blanks
        self.blank_one = QtW.QRadioButton()
        self.blank_one.setFixedSize(200, 100)
        self.blank_one.setStyleSheet(qradiobutton_stylesheet_text)
        self.blank_one.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.blank_one.setCheckable(False)
        self.tm_choice_page_layout.addWidget(self.blank_one, 4, 0, 1, 2)

        self.blank_two = QtW.QRadioButton()
        self.blank_two.setFixedSize(200, 100)
        self.blank_two.setStyleSheet(qradiobutton_stylesheet_text)
        self.blank_two.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.blank_two.setCheckable(False)
        self.tm_choice_page_layout.addWidget(self.blank_two, 4, 2, 1, 2)

        self.blank_three = QtW.QRadioButton()
        self.blank_three.setFixedSize(200, 100)
        self.blank_three.setStyleSheet(qradiobutton_stylesheet_text)
        self.blank_three.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.blank_three.setCheckable(False)
        self.tm_choice_page_layout.addWidget(self.blank_three, 4, 4, 1, 2)

        self.blank_four = QtW.QRadioButton()
        self.blank_four.setFixedSize(200, 100)
        self.blank_four.setStyleSheet(qradiobutton_stylesheet_text)
        self.blank_four.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.blank_four.setCheckable(False)
        self.tm_choice_page_layout.addWidget(self.blank_four, 5, 0, 1, 2)

        self.blank_five = QtW.QRadioButton()
        self.blank_five.setFixedSize(200, 100)
        self.blank_five.setStyleSheet(qradiobutton_stylesheet_text)
        self.blank_five.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.blank_five.setCheckable(False)
        self.tm_choice_page_layout.addWidget(self.blank_five, 5, 2, 1, 2)

        self.blank_six = QtW.QRadioButton()
        self.blank_six.setFixedSize(200, 100)
        self.blank_six.setStyleSheet(qradiobutton_stylesheet_text)
        self.blank_six.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.blank_six.setCheckable(False)
        self.tm_choice_page_layout.addWidget(self.blank_six, 5, 4, 1, 2)

        self.blank_seven = QtW.QRadioButton()
        self.blank_seven.setFixedSize(200, 100)
        self.blank_seven.setStyleSheet(qradiobutton_stylesheet_text)
        self.blank_seven.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.blank_seven.setCheckable(False)
        self.tm_choice_page_layout.addWidget(self.blank_seven, 6, 0, 1, 2)

        self.blank_eight = QtW.QRadioButton()
        self.blank_eight.setFixedSize(200, 100)
        self.blank_eight.setStyleSheet(qradiobutton_stylesheet_text)
        self.blank_eight.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.blank_eight.setCheckable(False)
        self.tm_choice_page_layout.addWidget(self.blank_eight, 6, 2, 1, 2)

        self.blank_nine = QtW.QRadioButton()
        self.blank_nine.setFixedSize(200, 100)
        self.blank_nine.setStyleSheet(qradiobutton_stylesheet_text)
        self.blank_nine.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        self.blank_nine.setCheckable(False)
        self.tm_choice_page_layout.addWidget(self.blank_nine, 6, 4, 1, 2)

        # =====================================================
        # ~* Ticket Buttons *~
        self.ticket_ticket = get_styled_label("Bilet", 2, TicketMachine.ticket_city_stylesheet_text)
        self.ticket_ticket.setFixedSize(200, 100)
        self.tm_choice_page_layout.addWidget(self.ticket_ticket, 7, 1, 1, 2)

        self.ticket_count = get_styled_label("Ilość", 1, TicketMachine.ticket_count_stylesheet_text)
        self.tm_choice_page_layout.addWidget(self.ticket_count, 7, 4, 1, 1)

        self.ticket_add = get_styled_button("+", 1, TicketMachine.button_one_by_one_stylesheet_text)
        self.tm_choice_page_layout.addWidget(self.ticket_add, 7, 5, 1, 1)

        self.ticket_remove = get_styled_button("-", 1, TicketMachine.button_one_by_one_stylesheet_text)
        self.ticket_remove.setFixedSize(100, 100)
        self.tm_choice_page_layout.addWidget(self.ticket_remove, 7, 3, 1, 1)

        self.ticket_clear = get_styled_button("x", 1, TicketMachine.button_short_red_remove_stylesheet_text)
        self.ticket_clear.setFixedSize(100, 100)
        self.tm_choice_page_layout.addWidget(self.ticket_clear, 7, 0, 1, 1)

        # =====================================================
        # ~* Bottom Buttons *~

        self.go_to_summary_button = get_styled_button("Przejdź do\npodsumowania", 2, TicketMachine.button_stylesheet_text)
        self.go_to_summary_button.setFixedSize(200, 100)
        self.tm_choice_page_layout.addWidget(self.go_to_summary_button, 8, 4, 1, 2)

        # =====================================================
        for row in range(9):
            self.tm_choice_page_layout.setRowMinimumHeight(row, 100)

        # =====================================================
        # ~* Make the initial SELECTIONS! *~
        self.language_polish_button.setChecked(True)
        self.zone_city_button.setChecked(True)
        self.discount_disc_button.setChecked(True)
        self.topsel_selected(0)
        self.group_family_ticket_button.setChecked(True)
        topsel_one_button.setChecked(True)
        self.one_one_time_button.setChecked(True)
        self.one_60_minutes_ticket_button.setChecked(True)
        self.one_24h_ticket_button.setChecked(True)
        self.midsel_selected(0)
        self.go_to_summary_button.setVisible(False)


    def topsel_selected(self, column):
        self.topsel = column
        # SELECTING GROUP
        # Hide everything from 1, display 7 blanks and 2 sub-group buttons
        if column == 2:
            for button in self.one_flavours_list:
                button.hide()
            for button in self.day_pass_flavours_list:
                button.hide()

            self.blank_one.hide()
            self.blank_two.hide()
            self.blank_three.show()
            self.blank_four.show()
            self.blank_five.show()
            self.blank_six.show()
            self.blank_seven.show()

            for button in self.group_flavours_list:
                button.show()

            # AUTOSELECT
            if self.group_family_ticket_button.isChecked():
                self.select_ticket(Ticket.TType.tweekend_family)
            else:
                self.select_ticket(Ticket.TType.tgroup)

        # SELECTING 2-RIDE
        # Hide everything from 1, hide group stuff, show all blanks. It's that simple.
        elif column == 1:
            for button in self.one_flavours_list:
                button.hide()
            for button in self.day_pass_flavours_list:
                button.hide()

            self.blank_one.show()
            self.blank_two.show()
            self.blank_three.show()
            self.blank_four.show()
            self.blank_five.show()
            self.blank_six.show()
            self.blank_seven.show()

            for button in self.group_flavours_list:
                button.hide()
        # SELECTING 1-RIDE
        # Hide everything from group, display bot-sel buttons, go from there.
        elif column == 0:
            for button in self.one_flavours_list:
                button.show()
            for button in self.day_pass_flavours_list:
                button.hide()

            self.blank_one.hide()
            self.blank_two.hide()
            self.blank_three.hide()
            self.blank_four.hide()
            self.blank_five.hide()
            self.blank_six.hide()
            self.blank_seven.hide()

            for button in self.group_flavours_list:
                button.hide()
            self.midsel_selected(self.midsel)


        # self.blank_one.hide()
        # self.blank_two.hide()
        # self.blank_three.hide()
        # self.blank_four.hide()
        # self.blank_five.hide()
        # self.blank_six.hide()
        # self.blank_seven.hide()

    def midsel_selected(self, column):
        self.midsel = column
        # 1-TIME SELECTED
        # Hide everything, show blanks
        if column == 0:
            for button in self.time_pass_flavours_list:
                button.hide()
            for button in self.day_pass_flavours_list:
                button.hide()
            self.blank_one.hide()
            self.blank_two.hide()
            self.blank_three.hide()
            self.blank_four.show()
            self.blank_five.show()
            self.blank_six.show()
            self.blank_seven.show()

            # Select the proper ticket (this is different than all the other times!)
            if self.get_selected_zone() == Ticket.Zone.Aglo:
                self.select_ticket(Ticket.TType.t1_ride)
            else:
                self.select_ticket(Ticket.TType.t1_or_40min)


        elif column == 1:
            if self.get_selected_zone() == Ticket.Zone.Aglo:
                self.blank_four.show()
                self.blank_five.show()
            else:
                self.blank_four.hide()
                self.blank_five.hide()

            for button in self.time_pass_flavours_list:
                if (self.get_selected_zone() != Ticket.Zone.Aglo) or (button not in self.aglo_exclusion_list):
                    button.show()
                else:
                    button.hide()

            for button in self.day_pass_flavours_list:
                button.hide()
            self.blank_one.hide()
            self.blank_two.hide()
            self.blank_three.hide()

            self.blank_six.hide()
            self.blank_seven.hide()

            # AUTOSELECT
            if self.one_20_minutes_ticket_button.isChecked():
                self.select_ticket(Ticket.TType.t20min)
            elif self.one_40_minutes_ticket_button.isChecked():
                self.select_ticket(Ticket.TType.t1_or_40min)
            elif self.one_60_minutes_ticket_button.isChecked():
                self.select_ticket(Ticket.TType.t60min)
            elif self.one_90_minutes_ticket_button.isChecked():
                self.select_ticket(Ticket.TType.t90min)

        elif column == 2:
            if self.get_selected_zone() == Ticket.Zone.Aglo:
                self.blank_five.show()
                self.blank_six.show()
            else:
                self.blank_five.hide()
                self.blank_six.hide()

            for button in self.time_pass_flavours_list:
                button.hide()
            for button in self.day_pass_flavours_list:
                if (self.get_selected_zone() != Ticket.Zone.Aglo) or (button not in self.aglo_exclusion_list):
                    button.show()
                else:
                    button.hide()
            self.blank_one.hide()
            self.blank_two.hide()
            self.blank_three.hide()
            self.blank_four.hide()
            self.blank_seven.hide()

            # AUTOSELECT
            if self.one_24h_ticket_button.isChecked():
                self.select_ticket(Ticket.TType.t24h)
            elif self.one_48h_ticket_button.isChecked():
                self.select_ticket(Ticket.TType.t48h)
            elif self.one_72h_ticket_button.isChecked():
                self.select_ticket(Ticket.TType.t72h)
            elif self.one_7day_ticket_button.isChecked():
                self.select_ticket(Ticket.TType.t7days)

    def select_ticket(self, i_ttype):
        self.selected_ticket = Ticket(self.get_selected_zone(), self.get_selected_discout(), i_ttype)
        # ~*Update ticket name and count boxes!*~
        # Set BG color (Aglo/City?)
        if self.get_selected_zone() == Ticket.Zone.Aglo:
            self.ticket_ticket.setStyleSheet(TicketMachine.ticket_aglo_stylesheet_text)
        else:
            self.ticket_ticket.setStyleSheet(TicketMachine.ticket_city_stylesheet_text)

        # Set ticket name
        cur_ticket_ticket = Ticket(self.get_selected_zone(), self.get_selected_discout(), i_ttype)

        ticket_name = cur_ticket_ticket.get_label_string()
        self.ticket_ticket.setText(ticket_name)

        # Set ticket count
        cur_ticket_count = TicketMachine.cart[cur_ticket_ticket.token()]
        self.ticket_count.setText(str(cur_ticket_count))

        print("Selected ticket: " + ticket_name)

    def update_ticket_count(self):
        cur_ticket_count = TicketMachine.cart[self.selected_ticket.token()]
        self.ticket_count.setText(str(cur_ticket_count))

    def get_selected_zone(self):
        if self.BG_zone_buttons.checkedButton() == self.zone_aglo_button:
            return Ticket.Zone.Aglo
        else:
            return Ticket.Zone.City

    def get_selected_discout(self):
        if self.BG_disc_buttons.checkedButton() == self.discount_disc_button:
            return Ticket.Discount.Discounted
        else:
            return Ticket.Discount.Normal

    def zone_button_clicked(self):
        # If switching to Aglo
        if self.get_selected_zone() == Ticket.Zone.Aglo:
            # Hide unavailable
            for button in self.aglo_exclusion_list:
                button.hide()
            # Display blanks
            if self.topsel == 0:
                if self.midsel == 1:
                    self.blank_four.show()
                    self.blank_five.show()
                elif self.midsel == 2:
                    self.blank_five.show()
                    self.blank_six.show()
            # Switch selections if necessary
            if self.one_48h_ticket_button.isChecked() or self.one_72h_ticket_button.isChecked():
                self.one_24h_ticket_button.click()
            if self.one_20_minutes_ticket_button.isChecked() or self.one_40_minutes_ticket_button.isChecked():
                self.one_60_minutes_ticket_button.click()

        elif self.topsel == 0:
            if self.midsel == 1:
                self.blank_four.hide()
                self.blank_five.hide()
                self.one_20_minutes_ticket_button.show()
                self.one_40_minutes_ticket_button.show()
            elif self.midsel == 2:
                self.blank_five.hide()
                self.blank_six.hide()
                self.one_48h_ticket_button.show()
                self.one_72h_ticket_button.show()

        # Select right ticket
        if self.get_selected_zone() == Ticket.Zone.Aglo and self.selected_ticket.t_type == Ticket.TType.t1_or_40min:
            self.selected_ticket.t_type = Ticket.TType.t1_ride
        elif self.get_selected_zone() == Ticket.Zone.City and self.selected_ticket.t_type == Ticket.TType.t1_ride:
            self.selected_ticket.t_type = Ticket.TType.t1_or_40min

        self.select_ticket(self.selected_ticket.t_type)
        self.update_ticket_button_labels()

    def select_discout(self):
        # Select right ticket
        self.select_ticket(self.selected_ticket.t_type)
        self.update_ticket_button_labels()

    def update_ticket_button_labels(self):
        cur_zone = self.get_selected_zone()
        cur_disc = self.get_selected_discout()

        # ~* 1-Ride *~
        if cur_zone == Ticket.Zone.Aglo:
            one_one_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.t1_ride).get_choice_label_string()
        else:
            one_one_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.t1_or_40min).get_choice_label_string()

            one_20min_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.t20min).get_choice_label_string()
            one_40min_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.t1_or_40min).get_choice_label_string()
            one_48h_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.t48h).get_choice_label_string()
            one_72h_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.t72h).get_choice_label_string()

            self.one_20_minutes_ticket_button.setText(one_20min_ticket_text)
            self.one_40_minutes_ticket_button.setText(one_40min_ticket_text)
            self.one_48h_ticket_button.setText(one_48h_ticket_text)
            self.one_72h_ticket_button.setText(one_72h_ticket_text)

        self.one_one_time_button.setText(one_one_ticket_text)

        one_60min_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.t60min).get_choice_label_string()
        one_90min_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.t90min).get_choice_label_string()
        one_24h_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.t24h).get_choice_label_string()
        one_7days_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.t7days).get_choice_label_string()

        self.one_60_minutes_ticket_button.setText(one_60min_ticket_text)
        self.one_90_minutes_ticket_button.setText(one_90min_ticket_text)
        self.one_24h_ticket_button.setText(one_24h_ticket_text)
        self.one_7day_ticket_button.setText(one_7days_ticket_text)

        # ~* 2-Ride *~
        two_ride_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.t2_rides).get_choice_label_string()
        self.topsel_two_button.setText(two_ride_ticket_text)

        # ~* Group Buttons *~
        group_family_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.tweekend_family).get_choice_label_string()
        group_group_ticket_text = Ticket(cur_zone, cur_disc, Ticket.TType.tgroup).get_choice_label_string()

        self.group_family_ticket_button.setText(group_family_ticket_text)
        self.group_group_ticket_button.setText(group_group_ticket_text)


class SummaryPage(QtW.QWidget):
    tm_link = None
    page = 0
    pills = list()
    full_pills = list()

    def __init__(self, flags, i_tm_link : TicketMachine = None, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)
        self.tm_link = i_tm_link
        self.cart_list = list()

        self.summary_layout_static_part = QtW.QGridLayout()
        self.setLayout(self.summary_layout_static_part)

        # ~* Language Selection *~
        self.BG_language_buttons = QtW.QButtonGroup(self)
        # --> POLISH
        self.language_polish_button = get_styled_radio_button("", 2, TicketMachine.language_stylesheet_pl_text)
        self.BG_language_buttons.addButton(self.language_polish_button)
        self.summary_layout_static_part.addWidget(self.language_polish_button, 0, 0, 1, 2)
        # --> ENGLISH
        self.language_english_button = get_styled_radio_button("", 2, TicketMachine.language_stylesheet_en_text)
        self.BG_language_buttons.addButton(self.language_english_button)
        self.summary_layout_static_part.addWidget(self.language_english_button, 0, 4, 1, 2)
        # ~* Top buttons *~
        self.add_ticket_button = get_styled_button("Dodaj\nkolejny bilet", 2, TicketMachine.button_stylesheet_text)
        self.summary_layout_static_part.addWidget(self.add_ticket_button, 1, 0, 1, 2)

            # Redundant add
        self.add_ticket_button_red = get_styled_button("Dodaj\nkolejny bilet", 2, TicketMachine.button_stylesheet_text)
        self.summary_layout_static_part.addWidget(self.add_ticket_button_red, 8, 2, 1, 2)

        self.pay_cash_button = get_styled_button("Zapłać gotówką", 2, TicketMachine.button_stylesheet_text)
        self.summary_layout_static_part.addWidget(self.pay_cash_button, 1, 2, 1, 2)
        self.pay_card_button = get_styled_button("Zapłać kartą", 2, TicketMachine.button_stylesheet_text)
        self.summary_layout_static_part.addWidget(self.pay_card_button, 1, 4, 1, 2)

        # ~* Price/Cancel Row *~
        self.total_label = get_styled_label("Cena łączna", 2, TicketMachine.label_stylesheet_text)
        self.summary_layout_static_part.addWidget(self.total_label, 2, 0, 1, 2)
        self.total_display = get_styled_label("", 2, TicketMachine.label_total_white_bg_stylesheet_text)
        self.summary_layout_static_part.addWidget(self.total_display, 2, 2, 1, 2)
        self.cancel_everything = get_styled_button("Usuń wszystkie\nbilety", 2, TicketMachine.button_long_red_stylesheet_text)
        self.summary_layout_static_part.addWidget(self.cancel_everything, 2, 4, 1, 2)

        # ~* Huge Label Divider *~
        self.label_divider_chosen_tickets = get_styled_label("Wybrane bilety", 6, TicketMachine.label_wybrane_stylesheet_text)
        self.summary_layout_static_part.addWidget(self.label_divider_chosen_tickets, 3, 0, 1, 6)

        # ~* The Pill Shells *~
        self.pill_count = 4
        self.pill_start = 4
        for pill_n in range(self.pill_start, self.pill_start + self.pill_count):
            cur_pill = list()
            cur_full_pill = list()
            ticket_type_label = get_styled_label("TICKET", 2, TicketMachine.ticket_city_stylesheet_text)
            cur_pill.append(ticket_type_label)
            cur_full_pill.append(ticket_type_label)
            number_of_tickets_label = get_styled_label("???", 1, TicketMachine.ticket_count_stylesheet_text)
            cur_pill.append(number_of_tickets_label)
            cur_full_pill.append(number_of_tickets_label)
            add_button = get_styled_button("+", 1, TicketMachine.button_one_by_one_stylesheet_text)
            cur_full_pill.append(add_button)
            remove_button = get_styled_button("-", 1, TicketMachine.button_one_by_one_stylesheet_text)
            cur_full_pill.append(remove_button)
            clear_button = get_styled_button("x", 1, TicketMachine.button_short_red_remove_stylesheet_text)
            cur_full_pill.append(clear_button)
            self.pills.append(cur_pill)
            self.full_pills.append(cur_full_pill)

            self.summary_layout_static_part.addWidget(ticket_type_label, pill_n, 1, 1, 2)
            self.summary_layout_static_part.addWidget(number_of_tickets_label, pill_n, 4, 1, 1)
            self.summary_layout_static_part.addWidget(add_button, pill_n, 5, 1, 1)
            self.summary_layout_static_part.addWidget(remove_button, pill_n, 3, 1, 1)
            self.summary_layout_static_part.addWidget(clear_button, pill_n, 0, 1, 1)

            #~* SIGNALS *~
            self.cancel_everything.clicked.connect(self.purge_everything)

            slot_n = pill_n - self.pill_start

            add_command = lambda clicked = True, slot_n = slot_n: self.pill_add(slot_n)
            add_button.clicked.connect(add_command)

            remove_command = lambda clicked = True, slot_n = slot_n: self.pill_remove(slot_n)
            remove_button.clicked.connect(remove_command)

            clear_command = lambda clicked = True, slot_n = slot_n: self.pill_purge(slot_n)
            clear_button.clicked.connect(clear_command)

        # ~* Previous/Next Page Buttons *~
        self.pill_previous = get_styled_button("↑", 2, TicketMachine.arrow_button_stylesheet_text)
        self.summary_layout_static_part.addWidget(self.pill_previous, 8, 0, 1, 2)
        self.pill_previous.clicked.connect(self.previous_page)

        self.pill_next = get_styled_button("↓", 2, TicketMachine.arrow_button_stylesheet_text)
        self.summary_layout_static_part.addWidget(self.pill_next, 8, 4, 1, 2)
        self.pill_next.clicked.connect(self.next_page)

        # ~* Set minimum height to hide pills properly *~
        for row in range(9):
            self.summary_layout_static_part.setRowMinimumHeight(row, 100)

        self.update_arrow_existence()

    def create_ticket_pill(self, ticket_type):
        ticket_type_label = QtW.QLabel(Ticket(*ticket_type).get_label_string())
        number_of_tickets_label = get_styled_label(str(TicketMachine.cart[ticket_type]), 1, TicketMachine.ticket_count_stylesheet_text)
        add_button = get_styled_button("+", 1, TicketMachine.button_one_by_one_stylesheet_text)
        remove_button = get_styled_button("-", 1, TicketMachine.button_one_by_one_stylesheet_text)
        clear_button = get_styled_button("x", 1, TicketMachine.button_one_by_one_stylesheet_text)
        layout_pill = QtW.QHBoxLayout()
        layout_pill.addWidget(ticket_type_label)
        layout_pill.addWidget(number_of_tickets_label)
        layout_pill.addWidget(add_button)
        layout_pill.addWidget(remove_button)
        layout_pill.addWidget(clear_button)
        return (ticket_type_label, number_of_tickets_label, add_button, remove_button, clear_button, layout_pill)

    def update_pills(self):
        pills_per_page = 4
        pill_count = len(TicketMachine.pillables)
        pill_start = self.page * pills_per_page

        for pill_n in range(pill_start, pill_start + pills_per_page):
            pill_mod = pill_n % pills_per_page

            if pill_n >= pill_count:
                self.pills[pill_mod][0].setText("")
                self.pills[pill_mod][0].setStyleSheet(TicketMachine.label_stylesheet_text)
                self.pills[pill_mod][1].setText("")

                # Invisible pills
                for element in self.full_pills[pill_mod]:
                    element.setVisible(False)

            else:
                # Visible pills
                for element in self.full_pills[pill_mod]:
                    element.setVisible(True)

                cur_pill_ticket = TicketMachine.pillables[pill_n]
                cur_ticket_count = TicketMachine.cart[cur_pill_ticket]

                # Set BG color (Aglo/City?)
                if cur_pill_ticket[0] == Ticket.Zone.Aglo:
                    self.pills[pill_mod][0].setStyleSheet(TicketMachine.ticket_aglo_stylesheet_text)
                else:
                    self.pills[pill_mod][0].setStyleSheet(TicketMachine.ticket_city_stylesheet_text)

                # Set ticket name
                ticket_name = Ticket(*cur_pill_ticket).get_label_string()
                self.pills[pill_mod][0].setText(ticket_name)

                # Set ticket count
                self.pills[pill_mod][1].setText(str(cur_ticket_count))

    def previous_page(self):
        if self.page > 0:
            self.page -= 1
            self.update_pills()
            self.update_arrow_existence()
            print("Page: " + str(self.page))

    def next_page(self):
        page_mult = 4
        if len(TicketMachine.pillables) > (self.page + 1) * page_mult:
            self.page += 1
            self.update_pills()
            self.update_arrow_existence()
            print("Page: " + str(self.page))

    def pill_add(self, pill_n_mod):
        pill_per_page = 4 # Holy shit... That "single source of truth" thing never sunk in, huh?
        pill_n = self.page * pill_per_page + pill_n_mod
        pill_total = len(TicketMachine.pillables)

        if pill_n < pill_total:
            cur_ticket = TicketMachine.pillables[pill_n]
            self.tm_link.add_ticket(cur_ticket)
            self.update_pills()
        self.update_payment_possibility()

    def pill_remove(self, pill_n_mod):
        pill_per_page = 4  # Holy shit... That "single source of truth" thing never sunk in, huh?
        pill_n = self.page * pill_per_page + pill_n_mod
        pill_total = len(TicketMachine.pillables)

        if pill_n < pill_total:
            cur_ticket = TicketMachine.pillables[pill_n]

            if TicketMachine.cart[cur_ticket] > 0:
                self.tm_link.remove_ticket(cur_ticket)
                self.update_pills()
        self.update_payment_possibility()

    def pill_purge(self, pill_n_mod):
        pill_per_page = 4  # Holy shit... That "single source of truth" thing never sunk in, huh?
        pill_n = self.page * pill_per_page + pill_n_mod
        pill_total = len(TicketMachine.pillables)

        if pill_n < pill_total:
            cur_ticket = TicketMachine.pillables[pill_n]
            self.tm_link.purge_ticket(cur_ticket)
            self.update_pills()

        pill_total = len(TicketMachine.pillables)
        if pill_total <= self.page * pill_per_page:
            self.previous_page()
        if pill_total == 0:
            self.tm_link.tm_virtual_screen.setCurrentWidget(self.tm_link.choice_page)

        self.update_arrow_existence()
        self.update_payment_possibility()

    def purge_everything(self):
        self.tm_link.purge_cart()
        self.update_pills()

    def update_arrow_existence(self):
        page_mult = 4

        if self.page == 0:
            self.pill_previous.setVisible(False)
        else:
            self.pill_previous.setVisible(True)

        print(str(len(TicketMachine.pillables)), str((self.page + 1) * page_mult))

        if len(TicketMachine.pillables) > (self.page + 1) * page_mult:
            self.pill_next.setVisible(True)
        else:
            self.pill_next.setVisible(False)

    def update_payment_possibility(self):
        total = self.tm_link.get_total()
        if total > 0:
            self.pay_cash_button.setVisible(True)
            self.pay_card_button.setVisible(True)
        else:
            self.pay_cash_button.setVisible(False)
            self.pay_card_button.setVisible(False)


class PaymentPage(QtW.QWidget):
    page = 0
    pills = list()

    def __init__(self, flags, *args, **kwargs):
        super().__init__(flags, *args, **kwargs)
        self.usesCoins = True

        # Setup language buttons
        language_stylesheet_pl_text = """
                    QRadioButton::unchecked {border-image: url(./images/button-flag-pl-grey.png)}
                    QRadioButton::checked {border-image: url(./images/button-flag-pl.png)}
                    QRadioButton::indicator { background-color: transparent; background: transparent }
                    """

        language_stylesheet_en_text = """
                    QRadioButton::unchecked {border-image: url(./images/button-flag-en-grey.png)}
                    QRadioButton::checked {border-image: url(./images/button-flag-en.png)}
                    QRadioButton::indicator { background-color: transparent; background: transparent }
                    """

        self.tm_payment_page_layout = QtW.QGridLayout()
        self.setLayout(self.tm_payment_page_layout)

        self.BG_language_buttons = QtW.QButtonGroup(self)
        # --> POLISH
        self.language_polish_button = QtW.QRadioButton()
        self.language_polish_button.setFixedSize(200, 100)
        self.BG_language_buttons.addButton(self.language_polish_button)
        self.tm_payment_page_layout.addWidget(self.language_polish_button, 0, 0, 1, 2)
        self.language_polish_button.setStyleSheet(language_stylesheet_pl_text)
        # self.language_polish_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)
        # --> ENGLISH
        self.language_english_button = QtW.QRadioButton()
        self.language_english_button.setFixedSize(200, 100)
        self.BG_language_buttons.addButton(self.language_english_button)
        self.tm_payment_page_layout.addWidget(self.language_english_button, 0, 4, 1, 2)
        self.language_english_button.setStyleSheet(language_stylesheet_en_text)
        # self.language_english_button.setSizePolicy(QtW.QSizePolicy.Expanding, QtW.QSizePolicy.Expanding)

        # BACK Button

        self.back_to_summary_button = get_styled_button("Cofnij", 2, TicketMachine.button_stylesheet_text)
        self.tm_payment_page_layout.addWidget(self.back_to_summary_button, 1, 0, 1, 2)

        # To-pay Label
        self.to_pay_label = get_styled_label("Do zapłaty:", 2, TicketMachine.label_stylesheet_text)
        self.tm_payment_page_layout.addWidget(self.to_pay_label, 1, 4, 1, 2)

        # Left To Pay Box
        self.to_pay_box = get_styled_label("???", 2, TicketMachine.left_to_pay_stylesheet_text)
        self.tm_payment_page_layout.addWidget(self.to_pay_box, 2, 4, 1, 2)

        # Total Price Label
        self.total_price_label = get_styled_label("  Cena łączna:", 2, TicketMachine.label_stylesheet_text)
        self.tm_payment_page_layout.addWidget(self.total_price_label, 1, 2, 1, 2)

        # Total Price Box
        self.total_price_box = QtW.QLabel("  ???")
        self.total_price_box.setStyleSheet(TicketMachine.label_total_white_bg_stylesheet_text)
        self.total_price_box.setAlignment(QtC.Qt.AlignCenter)
        self.tm_payment_page_layout.addWidget(self.total_price_box, 2, 2, 1, 2)

        # !!! Type of payment! !!!
        self.type_of_payment = get_styled_label("Płatność", 2, TicketMachine.label_stylesheet_text)
        self.tm_payment_page_layout.addWidget(self.type_of_payment, 2, 0, 1, 2)

        # Chosen ones
        self.chosen_ones_label = get_styled_label("Wybrane bilety:", 6, TicketMachine.label_wybrane_stylesheet_text)
        self.tm_payment_page_layout.addWidget(self.chosen_ones_label, 3, 0, 1, 6)

        # ~* PILLS HERE! *~
        for pill_n in range(8):
            cur_pill = list()
            cur_pill_ticket = get_styled_label("???", 2, TicketMachine.ticket_city_stylesheet_text)
            cur_pill_count = get_styled_label("$$$", 1, TicketMachine.ticket_count_stylesheet_text)
            cur_pill.append(cur_pill_ticket)
            cur_pill.append(cur_pill_count)
            self.pills.append(cur_pill)

            cur_pill_row = 4 + (pill_n // 2)
            cur_pill_col = (pill_n % 2) * 3
            cur_pill_col_count = cur_pill_col + 2
            self.tm_payment_page_layout.addWidget(cur_pill_ticket, cur_pill_row, cur_pill_col, 1, 2)
            self.tm_payment_page_layout.addWidget(cur_pill_count, cur_pill_row, cur_pill_col_count, 1, 1)

        # ~* Pill scroll buttons *~
        self.pill_previous = get_styled_button("↑", 2, TicketMachine.arrow_button_stylesheet_text)
        self.tm_payment_page_layout.addWidget(self.pill_previous, 8, 0, 1, 2)
        self.pill_previous.clicked.connect(self.previous_page)

        self.pill_next = get_styled_button("↓", 2, TicketMachine.arrow_button_stylesheet_text)
        self.tm_payment_page_layout.addWidget(self.pill_next, 8, 4, 1, 2)
        self.pill_next.clicked.connect(self.next_page)

        # ~* UPDATE STUFF! *~
        for row in range(9):
            self.tm_payment_page_layout.setRowMinimumHeight(row, 100)

        self.update_pills()
        self.update_arrow_existence()

    def previous_page(self):
        if self.page > 0:
            self.page -= 1
            self.update_pills()
            self.update_arrow_existence()
            print("Page: " + str(self.page))

    def next_page(self):
        page_mult = 8
        if len(TicketMachine.pillables) > (self.page + 1) * page_mult:
            self.page += 1
            self.update_pills()
            self.update_arrow_existence()
            print("Page: " + str(self.page))

    def update_arrow_existence(self):
        page_mult = 8

        if self.page == 0:
            self.pill_previous.setVisible(False)
        else:
            self.pill_previous.setVisible(True)

        if len(TicketMachine.pillables) > (self.page + 1) * page_mult:
            self.pill_next.setVisible(True)
        else:
            self.pill_next.setVisible(False)


    def update_pills(self):
        pills_per_page = 8
        pill_count = len(TicketMachine.pillables)
        pill_start = self.page * pills_per_page

        for pill_n in range(pill_start, pill_start + pills_per_page):
            pill_mod = pill_n % pills_per_page

            if pill_n >= pill_count:
                self.pills[pill_mod][0].setVisible(False)
                self.pills[pill_mod][0].setStyleSheet(TicketMachine.label_stylesheet_text)
                self.pills[pill_mod][1].setVisible(False)
            else:
                self.pills[pill_mod][0].setVisible(True)
                self.pills[pill_mod][1].setVisible(True)

                cur_pill_ticket = TicketMachine.pillables[pill_n]
                cur_ticket_count = TicketMachine.cart[cur_pill_ticket]

                # Set BG color (Aglo/City?)
                if cur_pill_ticket[0] == Ticket.Zone.Aglo:
                    self.pills[pill_mod][0].setStyleSheet(TicketMachine.ticket_aglo_stylesheet_text)
                else:
                    self.pills[pill_mod][0].setStyleSheet(TicketMachine.ticket_city_stylesheet_text)

                # Set ticket name
                ticket_name = Ticket(*cur_pill_ticket).get_label_string()
                self.pills[pill_mod][0].setText(ticket_name)

                # Set ticket count
                self.pills[pill_mod][1].setText(str(cur_ticket_count))

class ResultOfOperationPage(QtW.QPushButton):
    class ResultType(Enum):
        CoinsSuccess = 0
        CardSuccess  = 1
        CardFailure  = 2

    bg_plain_stylesheet_text = """
                               border-image: url(./images/tm-summary-field.png); color: black; font-size: 26px;
                               """

    bg_change_stylesheet_text = """
                                border-image: url(./images/tm-summary-field2.png); color: black; font-size: 26px;
                                
                                """

    card_empty_text = "Niewystarczająca ilość środków na koncie.\n\nDotknij ekranu, aby powrócić do płatności."

    card_full_text = "Przypominamy, że bilet jest ważny\ndopiero po skasowaniu.\n\nDziękujemy za zakupy."

    timeout_ms = 5000

    def __init__(self, parent_tm : TicketMachine):
        super().__init__()
        self.parent_tm = parent_tm
        self.expiration_timer = QtC.QTimer(self)
        self.expiration_timer.setSingleShot(True)
        self.return_done = False

        # A dummy connection
        self.clicked.connect(self.back_to_choice)

    def morph(self, target_type: ResultType):
        self.return_done = False

        if target_type == ResultOfOperationPage.ResultType.CoinsSuccess:
            self.setStyleSheet(ResultOfOperationPage.bg_change_stylesheet_text)
            self.setText("")
            self.clicked.disconnect()
            self.clicked.connect(self.back_to_choice)
            self.start_countdown(self.back_to_choice)

        elif target_type == ResultOfOperationPage.ResultType.CardSuccess:
            self.setStyleSheet(ResultOfOperationPage.bg_plain_stylesheet_text)
            self.setText(ResultOfOperationPage.card_full_text)
            self.clicked.disconnect()
            self.clicked.connect(self.back_to_choice)
            self.start_countdown(self.back_to_choice)

        elif target_type == ResultOfOperationPage.ResultType.CardFailure:
            self.setStyleSheet(ResultOfOperationPage.bg_plain_stylesheet_text)
            self.setText(ResultOfOperationPage.card_empty_text)
            self.clicked.disconnect()
            self.clicked.connect(self.back_to_payment_method)
            self.start_countdown(self.back_to_payment_method)

    def start_countdown(self, timeout_callback):
        self.expiration_timer.singleShot(ResultOfOperationPage.timeout_ms, timeout_callback)

    def back_to_payment_method(self):
        if not self.return_done:
            # Enable what's there to be enabled
            if self.parent_tm.payment_page.usesCoins:
                for ci in self.parent_tm.coins_items:
                    ci.setEnabled(True)
            else:
                for ci in self.parent_tm.cards_items:
                    ci.setEnabled(True)

            self.parent_tm.tm_virtual_screen.setCurrentWidget(self.parent_tm.payment_page)
            self.return_done = True
        else:
            print("Timer Evaded!")

    def back_to_choice(self):
        if not self.return_done:
            self.parent_tm.tm_virtual_screen.setCurrentWidget(self.parent_tm.choice_page)
            self.return_done = True
        else:
            print("Timer Evaded!")


def main():
    tm_app = QtW.QApplication(sys.argv)  # The app
    tm_main_window = QtW.QMainWindow()
    tm_main_window.setWindowTitle("Ticket Machine")

    # Create the Ticket Machine widget
    tm_widget = TicketMachine(flags=None)
    tm_main_window.setCentralWidget(tm_widget)

    tm_main_window.show()
    sys.exit(tm_app.exec_())

def get_styled_button(text, width_multiplier, stylesheet):
    cur_button = QtW.QPushButton(text)
    cur_button.setFixedSize(100 * width_multiplier, 100)
    cur_button.setStyleSheet(stylesheet)
    # cur_button.setSizePolicy(QtW.QSizePolicy.Fixed, QtW.QSizePolicy.Fixed)
    return cur_button

def get_styled_radio_button(text, width_multiplier, stylesheet):
    cur_button = QtW.QRadioButton(text)
    cur_button.setFixedSize(100 * width_multiplier, 100)
    cur_button.setStyleSheet(stylesheet)
    cur_button.setSizePolicy(QtW.QSizePolicy.Fixed, QtW.QSizePolicy.Fixed)
    return cur_button

def get_styled_label(text, width_multiplier, stylesheet):
    cur_label = QtW.QLabel(text)
    cur_label.setFixedSize(100 * width_multiplier, 100)
    cur_label.setStyleSheet(stylesheet)
    cur_label.setAlignment(QtC.Qt.AlignCenter)
    return cur_label

if __name__ == "__main__":
    main()
