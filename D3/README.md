Diablo3_Random

This program should display the act, number and name of quest.
Then random selection of skills, active and passive, plus the rune numbers for each one.
There should be an animation for generating a new one (and a keyboard shortcut ?).
Up/down (modifiers? Something else) should scroll between previous/next entry on the list.
Final entry should just have "?"s and the "SPIN" button or something.


A class has skills to pick.

The games's divided into acts.
Acts are divided into quests.

There should be a number of builds per hero class and quest.

A build is just a selection of skills and runes.

A build set assigns quests to builds for a given class...
  ...there can, of course, be more than 1 of these.

Class has Skills

Act has Quests

Build has a subset of a class' skills.

There needs to be a build_set_list.
It will be a list [] of build sets.
A build set will have each quest assigned to a build and will know
  its character's class.