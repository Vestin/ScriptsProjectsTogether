# ~* Import section *~
# -->> General imports
import sys  # System used to exit the application and provide command line arguments (currently useless)
import os  # Used to get a randomizer and for path joining
import random  # Used to get random numbers and population samples
from enum import Enum  # Used for enumeration of Diablo 3 classes
import pickle
# -->> PyQt specific imports
import PyQt5.QtWidgets as QtW  # I need this for widgets and most other stuff
import PyQt5.QtCore as QtC  # Parameter constant enums are here.
import PyQt5.QtGui as QtG  # Needed to load pixmaps.
import PyQt5.QtMultimedia as QtM  # For the sound effect


# ~* Enum listing all classes in Diablo 3 *~


class DiabloClassEnum(Enum):
    Barbarian = 0
    Crusader = 1
    Demon_Hunter = 2
    Monk = 3
    Necromancer = 4
    Witch_Doctor = 5
    Wizard = 6


# ~* This is the actual DiabloClass class. It holds the skill information for each class *~

class DiabloClass:
    def __init__(self, dclass_enum: DiabloClassEnum):
        skill_size = QtC.QSize(128, 128)

        self.name = str(dclass_enum.name).replace('_', ' ')
        self.active_skills = []
        self.passive_skills = []

        # ~* Load the skill information from the file *~
        dclass_file_path = os.path.join('.', dclass_enum.name, 'skills.txt')
        dclass_file = open(dclass_file_path, 'r')
        # ~* Parse each line *~
        for skill_line in dclass_file:
            skill_line = skill_line.strip().split('|')
            pixmap_path = os.path.join('.', dclass_enum.name, 'Images', skill_line[0], skill_line[3])
            # -->> Generate a Skill entry based on the fields parsed from the text
            cur_skill = Skill(skill_line[1], skill_line[2], QtG.QPixmap(pixmap_path).scaled(skill_size))
            # -->> Assign it to either the Active of Passive group based on their type.
            if skill_line[0] == 'Active':
                self.active_skills.append(cur_skill)
            else:
                self.passive_skills.append(cur_skill)
        # -->> Once done parsing, close the file
        dclass_file.close()


# ~* Structure for holding skill information *~


class Skill:
    def __init__(self, i_type: str, i_name: str, i_image: QtG.QPixmap):
        # -->> The image has an on-mouse-over tooltip of 'self.name (self.type)'
        self.type = i_type
        self.name = i_name
        self.image = i_image


# ~* This holds the Act information *~


class Campaign(QtC.QObject):
    acts = []  # This holds the acts, which hold a name and list of quests [{name: str, quests: []}, ...]
    # >> Iterator variables
    __act = 0
    __quest = 0
    # >> Current selection variables
    sel_act = 0
    sel_quest = 0

    quest_switched = QtC.pyqtSignal()

    def __init__(self, quest_list_filename: str):
        super().__init__()  # Make signals WORK!

        quest_name_file = open(quest_list_filename, 'r')
        cur_act_quests = []
        act_number = 1
        for line in quest_name_file:
            if line[0] == '\t':
                cur_act_quests.append(line.strip())
            elif cur_act_quests:
                act_name = 'Act ' + self.get_roman_numeral(act_number)
                Campaign.acts.append({'name': act_name, 'quests': cur_act_quests})
                act_number += 1
                cur_act_quests = []
        quest_name_file.close()

    def __iter__(self):
        return self

    def __next__(self):
        try:
            quest = Campaign.acts[Campaign.__act]['quests'][Campaign.__quest]
        except IndexError:
            Campaign.__act = 0
            Campaign.__quest = 0
            raise StopIteration()
        if Campaign.__quest < len(Campaign.acts[Campaign.__act]['quests']) - 1:
            Campaign.__quest += 1
        else:
            Campaign.__quest = 0
            Campaign.__act += 1
        return quest

    def sel_prev(self):
        if Campaign.sel_quest > 0:
            Campaign.sel_quest -= 1
            self.quest_switched.emit()
        elif Campaign.sel_act > 0:
            Campaign.sel_act -= 1
            Campaign.sel_quest = len(Campaign.acts[Campaign.sel_act]['quests']) - 1
            self.quest_switched.emit()
        else:
            return False

    def sel_next(self):
        if Campaign.sel_quest < len(Campaign.acts[Campaign.sel_act]['quests']) - 1:
            Campaign.sel_quest += 1
            self.quest_switched.emit()
        elif Campaign.sel_act < len(Campaign.acts) - 1:
            Campaign.sel_act += 1
            Campaign.sel_quest = 0
            self.quest_switched.emit()
        else:
            return False

    @staticmethod
    def get_current_quest():
        return Campaign.acts[Campaign.sel_act]['quests'][Campaign.sel_quest]

    @staticmethod
    def get_campaign_text():
        return Campaign.acts[Campaign.sel_act]['name'] + '/' + str(Campaign.sel_quest + 1)\
               + '  |  ' + Campaign.get_current_quest()

    @staticmethod
    def get_roman_numeral(dec_in: int):
        rn_conv = {1000: 'M', 500: 'D', 100: 'C', 50: 'L', 10: 'X', 5: 'V', 1: 'I'}
        rn = ''
        checklist = sorted(rn_conv.keys(), reverse=True)

        for divisor in checklist:
            # Add symbols for as many of the biggest thing as necessary
            while dec_in >= divisor:
                rn += rn_conv.get(divisor)
                dec_in -= divisor
            # Try Romal numeral substraction
            rom_sub = [x for x in rn_conv.keys() if (divisor // 10 == x or divisor // 5 == x)]
            if len(rom_sub) != 0:
                rom_sub = max(rom_sub)
                if dec_in >= (divisor - rom_sub):
                    rn += rn_conv.get(rom_sub) + rn_conv.get(divisor)
                    dec_in = dec_in - (divisor - rom_sub)
        return rn


# ~* This is a tiny structure for each Quest of a given act *~


class Quest:
    def __init__(self, i_number, i_name):
        self.number = i_number
        self.name = i_name


# ~* Builds are sets of skills and runes *~


class Build:
    # ~* Constants *~
    # -->> How many skills/runes per build?
    num_active = 6
    num_runes = 6
    num_passive = 4
    num_rune_types = 5

    # -->> Let's get the Pixmaps for runes!
    rune_type = []

    # -->> This will give me reasonably random picks
    randomizer = random.SystemRandom()

    # -->> Null build elements
    null_active = None
    null_rune = None
    null_passive = None

    def __init__(self, i_dclass_info: DiabloClass, i_parent, i_arl_list=None):
        self.class_info = i_dclass_info
        self.parent = i_parent
        self.active = []
        self.rune = []
        self.passive = []

        if not Build.rune_type:
            self.load_runes()
        if not i_arl_list:
            self.load_blanks()
        else:
            self.active = i_arl_list[0]
            self.rune = i_arl_list[1]
            self.passive = i_arl_list[2]

    @staticmethod
    def load_runes():
        rune_filename = os.path.join('.', 'Neutral', 'runes.png')
        all_runes_pixmap = QtG.QPixmap(rune_filename)
        # -->> Rune rectangles
        rune_rect_list = [QtC.QRect(6, 2, 36, 46), QtC.QRect(53, 2, 44, 46), QtC.QRect(105, 2, 39, 46),
                          QtC.QRect(156, 3, 39, 45), QtC.QRect(204, 1, 39, 47)]

        for rect in rune_rect_list:
            scaled_size = QtC.QSize(all_runes_pixmap.copy(rect).width() * 2, all_runes_pixmap.copy(rect).height() * 2)
            Build.rune_type.append(all_runes_pixmap.copy(rect).scaled(scaled_size))

    def randomize_build(self):
        pop_active = self.class_info.active_skills
        pop_passive = self.class_info.passive_skills

        self.active = self.randomizer.sample(pop_active, self.num_active)
        rune_indexes = [self.randomizer.randint(1, self.num_rune_types) for _ in range(self.num_runes)]
        self.rune = [Build.rune_type[r_i - 1] for r_i in rune_indexes]
        self.passive = self.randomizer.sample(pop_passive, self.num_passive)

    @staticmethod
    def create_blanks():
        randomizer = random.SystemRandom()

        null_fonts = randomizer.sample(QtG.QFontDatabase().families(), 3)
        null_pixmap = []

        null_font_point_size = 84
        null_font_color = QtG.QColor(QtC.Qt.red)

        for i in range(3):
            null_text_pixmap = QtG.QPixmap(128, 128)  # Yeah, I know that ahead of time.
            null_text_pixmap.fill(QtG.QColor(QtC.Qt.transparent))
            null_painter = QtG.QPainter(null_text_pixmap)

            null_pen = QtG.QPen(null_font_color)
            null_pen.setWidth(1)
            null_painter.setPen(null_pen)
            null_painter.setFont(QtG.QFont(null_fonts[i], pointSize=null_font_point_size))
            null_painter.begin(null_text_pixmap)
            null_painter.drawText(QtC.QRect(0, 0, 128, 128), QtC.Qt.AlignCenter, "?")
            null_painter.end()

            null_pixmap.append(null_text_pixmap)

        Build.null_active = Skill('null', 'null', null_pixmap[0])
        Build.null_rune = null_pixmap[1]
        Build.null_passive = Skill('null', 'null', null_pixmap[2])

    def load_blanks(self):
        self.active = [Build.null_active] * self.num_active
        self.rune = [Build.null_rune] * self.num_runes
        self.passive = [Build.null_passive] * self.num_passive

    def is_null(self):
        return self.active[0] == Build.null_active

    def get_dclass_text(self):
        return self.parent.dclass.name.replace('_', ' ') + ' #' + str(self.parent.bs_index)


# ~* A BuildSet consists of builds for each quest (and some silly counters) *~


class BuildSet(QtC.QObject):
    buildset_denulled = QtC.pyqtSignal(DiabloClassEnum)

    def __init__(self, i_dclass: DiabloClassEnum, campaign: Campaign, i_class_info: DiabloClass, i_bs_index: int,
                 denull_function, initial_build_list=None):
        super().__init__()
        self.dclass = i_dclass
        self.bs_index = i_bs_index
        self.campaign = campaign
        self.build_dict = dict()

        quest_counter = 0
        for quest in campaign:
            if not initial_build_list:
                self.build_dict[quest] = Build(i_class_info, self)  # Assign an empty build for each quest
            else:
                self.build_dict[quest] = Build(i_class_info, self, initial_build_list[quest_counter])
            quest_counter += 1
        self.is_null = True
        self.buildset_denulled.connect(denull_function)  # Connect it to the thing spawning new (same dclass) null-build
        self.de_null()

    def de_null(self):
        if self.is_null:
            first_quest = [q for q in self.campaign][0]
            if not self.build_dict[first_quest].is_null():
                self.is_null = False
                self.buildset_denulled.emit(self.dclass)


# ~* BuildSetList contains all the BuildSets and facilitates navigation between them *~


class BuildSetList(QtC.QObject):
    buildsets = dict()
    cur_dclass = None
    cur_buildset = None

    can_switch = True  # This will be flipped when the animation is in progress, to prevent switching during it

    buildset_switched = QtC.pyqtSignal()

    def __init__(self, campaign: Campaign, dclass_dict: dict, i_buildsets=None):
        super().__init__()  # Make signals WORK!
        self.campaign = campaign
        self.dclass_dict = dclass_dict
        # ~* First create one buildset of null-builds for each class *~
        if not i_buildsets:
            for dclass in DiabloClassEnum:
                initial_null_buildset = BuildSet(dclass, campaign, dclass_dict[dclass], 1,
                                                 self.append_fresh_buildsets)
                BuildSetList.buildsets[dclass] = [initial_null_buildset]
        else:
            BuildSetList.buildsets = i_buildsets
            # >> Plug the buildsets into signals
            for dclass in DiabloClassEnum:
                for buildset in BuildSetList.buildsets[dclass]:
                    buildset.buildset_denulled.connect(self.append_fresh_buildsets)

        # >> Select a random class at the start
        dclass_iterable = [dclass for dclass in DiabloClassEnum]
        if not i_buildsets:
            BuildSetList.cur_dclass = random.SystemRandom().choice(dclass_iterable)
        BuildSetList.cur_buildset = 0

    @staticmethod
    def get_current_buildset():
        return BuildSetList.buildsets[BuildSetList.cur_dclass][BuildSetList.cur_buildset]

    @staticmethod
    def reset_campaign_position():
        Campaign.sel_act = 0
        Campaign.sel_quest = 0

    def sel_prev_buildset(self):
        if BuildSetList.cur_buildset > 0 and self.can_switch:
            BuildSetList.cur_buildset -= 1
            self.reset_campaign_position()
            self.buildset_switched.emit()

    def sel_next_buildset(self):
        if BuildSetList.cur_buildset < len(BuildSetList.buildsets[BuildSetList.cur_dclass]) - 1 and self.can_switch:
            BuildSetList.cur_buildset += 1
            self.reset_campaign_position()
            self.buildset_switched.emit()

    def sel_prev_dclass(self):
        dclass_list = [dclass for dclass in DiabloClassEnum]
        cur_dclass_index = dclass_list.index(BuildSetList.cur_dclass)
        if cur_dclass_index > 0 and self.can_switch:  # If it's not the FIRST class
            prev_dclass = dclass_list[cur_dclass_index - 1]
            # >> If you have the LAST BuildSet or bigger index than previous class, go to the last of the previous one.
            if (BuildSetList.cur_buildset != 0)\
                    and ((BuildSetList.cur_buildset == len(BuildSetList.buildsets[BuildSetList.cur_dclass]) - 1)
                         or (BuildSetList.cur_buildset > len(BuildSetList.buildsets[prev_dclass]) - 1)):
                BuildSetList.cur_buildset = len(BuildSetList.buildsets[prev_dclass]) - 1
            # >> ...otherwise just KEEP the cur_buildset index
            BuildSetList.cur_dclass = prev_dclass
            self.reset_campaign_position()
            self.buildset_switched.emit()

    def sel_next_dclass(self):
        dclass_list = [dclass for dclass in DiabloClassEnum]
        cur_dclass_index = dclass_list.index(BuildSetList.cur_dclass)
        if cur_dclass_index < len(dclass_list) - 1 and self.can_switch:  # If we don't have the LAST class selected
            next_dclass = dclass_list[cur_dclass_index + 1]
            # >> If you have the LAST BuildSet or bigger index than previous class, go to the last of the next one.
            if (BuildSetList.cur_buildset != 0)\
                    and ((BuildSetList.cur_buildset == len(BuildSetList.buildsets[BuildSetList.cur_dclass]) - 1)
                         or (BuildSetList.cur_buildset > len(BuildSetList.buildsets[next_dclass]) - 1)):
                BuildSetList.cur_buildset = len(BuildSetList.buildsets[next_dclass]) - 1
            # >> ...otherwise just KEEP the cur_buildset index
            BuildSetList.cur_dclass = next_dclass
            self.reset_campaign_position()
            self.buildset_switched.emit()

    def sel_prev_quest(self):
        if self.can_switch:
            self.campaign.sel_prev()

    def sel_next_quest(self):
        if not BuildSetList.get_current_buildset().build_dict[Campaign.get_current_quest()].is_null()\
                and self.can_switch:
            self.campaign.sel_next()

    def append_fresh_buildsets(self, dclass: DiabloClassEnum):
        next_index = len(self.buildsets[dclass]) + 1
        next_buildset = BuildSet(dclass, self.campaign, self.dclass_dict[dclass], next_index,
                                 self.append_fresh_buildsets)
        self.buildsets[dclass].append(next_buildset)

    def restore_switching(self):
        self.can_switch = True

# ~* This is the main class that gets instantiated as the program's (only?) window *~


class Diablo3Window(QtW.QGraphicsView):
    # ~* Hard-coded paths *~
    quest_name_path = 'Acts_and_Quests.txt'
    pickled_bsl_filename = 'buildset_list.pyc'

    # ~* Big global things *~
    # -->> All the acts & quests
    campaign = Campaign(quest_list_filename=quest_name_path)
    # -->> All the skills and runes
    dclass_dict = dict()
    # -->> All the sets of builds
    build_set_list = None
    # -->> All hotkeys
    hotkeys = []

    def __init__(self):
        # ~* Init the QGraphicsView class in general *~
        super().__init__()
        # ~* Get rid of the pesky borders! *~
        QtW.QGraphicsView.setFrameStyle(self, 0)
        self.setWindowTitle('Diablo 3 Randomizer by Vestin')
        self.setWindowIcon(
                     QtG.QIcon(random.SystemRandom().choice([dc for dc in DiabloClassEnum]).name + '/Images/class.png'))

        # ~* Load the list of skills *~
        for dclass in DiabloClassEnum:
            self.dclass_dict[dclass] = DiabloClass(dclass)
        # ~* Create static blank entries for the Build class *~
        Build.create_blanks()

        # ~* Create the initial list of null-buildsets *~
        Diablo3Window.build_set_list = BuildSetList(Diablo3Window.campaign, Diablo3Window.dclass_dict)
        # ~* Arrange the first scene *~
        self.randomizer_scene = RandomizerScene(self, Diablo3Window.build_set_list, self.campaign)
        # ~* Assign the scene to the view *~
        self.setScene(self.randomizer_scene)

        # ~* Set up hotkeys *~
        Diablo3Window.hotkeys = self.get_hotkey_list()

        # ~* Set up SpinnerContraptions for all classes *~
        # -->> Get the arrays of positions
        active_positions = [active.pos() for active in self.randomizer_scene.elements['active']]
        rune_positions = [rune.pos() for rune in self.randomizer_scene.elements['rune']]
        passive_positions = [passive.pos() for passive in self.randomizer_scene.elements['passive']]
        # -->> Get the heights of slots
        active_height = self.randomizer_scene.elements['active_border'][0].pixmap().height()
        rune_height = self.randomizer_scene.elements['rune_border'][0].pixmap().height()
        passive_height = self.randomizer_scene.elements['passive_border'][0].pixmap().height()

        self.spinner_contraption = dict()
        for dclass in DiabloClassEnum:
            self.spinner_contraption[dclass] = SpinnerContraption(self.dclass_dict, dclass,
                                                                  active_positions, active_height,
                                                                  rune_positions, rune_height,
                                                                  passive_positions, passive_height,
                                                                  self.randomizer_scene)
            self.spinner_contraption[dclass].animation_done.connect(self.randomizer_scene.play_build_ready)
            self.spinner_contraption[dclass].animation_done.connect(self.build_set_list.restore_switching)  # Reset it

        # >> Try loading the pickle. Maybe there's a saved version from earlier
        self.import_bsl()

        # ~* Finally - display the result *~
        self.show()  # Show this window

    def get_hotkey_list(self):
        hotkey_list = []
        # >> Fullscreen
        shortcut_fullscreen = QtW.QShortcut(self)
        shortcut_fullscreen.setKey(QtG.QKeySequence.FullScreen)
        shortcut_fullscreen.activated.connect(self.toggle_fullscreen)
        hotkey_list.append(shortcut_fullscreen)

        # >> Previous quest (and, therefore, build in a given buildset)
        shortcut_prev_quest = QtW.QShortcut(self)
        shortcut_prev_quest.setKey(QtG.QKeySequence.MoveToPreviousLine)
        shortcut_prev_quest.activated.connect(self.build_set_list.sel_prev_quest)
        hotkey_list.append(shortcut_prev_quest)

        # >> Next quest
        shortcut_next_quest = QtW.QShortcut(self)
        shortcut_next_quest.setKey(QtG.QKeySequence.MoveToNextLine)
        shortcut_next_quest.activated.connect(self.build_set_list.sel_next_quest)
        hotkey_list.append(shortcut_next_quest)

        # >> Previous dclass
        shortcut_prev_dclass = QtW.QShortcut(self)
        shortcut_prev_dclass.setKey(QtG.QKeySequence.MoveToPreviousChar)
        shortcut_prev_dclass.activated.connect(self.build_set_list.sel_prev_dclass)
        hotkey_list.append(shortcut_prev_dclass)

        # >> Next dclass
        shortcut_next_dclass = QtW.QShortcut(self)
        shortcut_next_dclass.setKey(QtG.QKeySequence.MoveToNextChar)
        shortcut_next_dclass.activated.connect(self.build_set_list.sel_next_dclass)
        hotkey_list.append(shortcut_next_dclass)

        # >> Previous BuildSet
        shortcut_prev_buildset = QtW.QShortcut(self)
        shortcut_prev_buildset.setKey(QtG.QKeySequence.MoveToPreviousPage)
        shortcut_prev_buildset.activated.connect(self.build_set_list.sel_prev_buildset)
        hotkey_list.append(shortcut_prev_buildset)

        # >> Next BuildSet
        shortcut_next_buildset = QtW.QShortcut(self)
        shortcut_next_buildset.setKey(QtG.QKeySequence.MoveToNextPage)
        shortcut_next_buildset.activated.connect(self.build_set_list.sel_next_buildset)
        hotkey_list.append(shortcut_next_buildset)

        # >> Roll the Dice
        shortcut_roll = QtW.QShortcut(self)
        shortcut_roll.setKey(QtC.Qt.Key_Space)
        shortcut_roll.activated.connect(self.randomizer_scene.roll_the_dice)
        hotkey_list.append(shortcut_roll)

        # >> Save the BuildSetList in the Pickle file
        shortcut_save_bsl = QtW.QShortcut(self)
        shortcut_save_bsl.setKey(QtG.QKeySequence.Save)
        shortcut_save_bsl.activated.connect(self.export_bsl)

        # >> Load the BuildSetList from the default Pickle file
        shortcut_load_bsl = QtW.QShortcut(self)
        shortcut_load_bsl.setKey(QtG.QKeySequence.Open)
        shortcut_load_bsl.activated.connect(self.import_bsl)

        return hotkey_list

    def toggle_fullscreen(self):
        if self.isFullScreen():
            self.showNormal()
        else:
            self.showFullScreen()

    def export_bsl(self):  # [DiabloClassEnum.value, [[[actives and runes], [passives]], [[],[]] ...] ]
        buildset_list_export = []
        for dclass in DiabloClassEnum:  # TODO: buildsets is a dict; go through dclass.
            current_class_buildset_list = self.build_set_list.buildsets[dclass]

            cur_class_export = []
            for buildset in current_class_buildset_list:
                cur_buildset_export = []
                for quest in self.campaign:
                    cur_build_active_export = []
                    for active_index in range(len(buildset.build_dict[quest].active)):
                        # >> Active
                        cur_active = buildset.build_dict[quest].active[active_index]
                        if cur_active == Build.null_active:
                            cur_build_active_export.append(-1)
                        else:
                            cur_build_active_export.append(
                                self.dclass_dict[buildset.dclass].active_skills.index(cur_active))
                        # >> Rune
                        cur_rune = buildset.build_dict[quest].rune[active_index]
                        if cur_rune == Build.null_rune:
                            cur_build_active_export.append(-1)
                        else:
                            cur_build_active_export.append(Build.rune_type.index(cur_rune))
                    cur_buildset_export.append(cur_build_active_export)
                    # >> Passive
                    cur_build_passive_export = []
                    for cur_passive in buildset.build_dict[quest].passive:
                        if cur_passive == Build.null_passive:
                            cur_build_passive_export.append(-1)
                        else:
                            cur_build_passive_export.append(
                                self.dclass_dict[buildset.dclass].passive_skills.index(cur_passive))
                    cur_buildset_export.append(cur_build_passive_export)
                cur_class_export.append(cur_buildset_export)
            buildset_list_export.append(cur_class_export)
        print(buildset_list_export)
        # At this point you pickle into a file. Done
        pickle_file = open(self.pickled_bsl_filename, 'wb')
        pickle.dump(obj=buildset_list_export, file=pickle_file, protocol=pickle.HIGHEST_PROTOCOL)
        pickle_file.close()

    def import_bsl(self):
        compressed_bsl = []
        if os.path.isfile(self.pickled_bsl_filename):
            pickled_bsl_file = open(self.pickled_bsl_filename, 'rb')
            compressed_bsl = pickle.load(pickled_bsl_file)
            pickled_bsl_file.close()

        current_user_selected_dclass = self.build_set_list.cur_dclass

        # ~* Create the replacement BSL & buildset dictionary *~
        new_buildset_dict = dict()
        for i, cur_dclass_bsl in enumerate(compressed_bsl):
            dclass_enum_entry = DiabloClassEnum(i)
            new_dclass_buildsets = []  # Simple list that houses buildsets
            buildset_num = 1
            for cur_buildset in cur_dclass_bsl:
                new_initial_buildset_list = []
                for active_passive_index in range(0, len(cur_buildset), 2):
                    new_actives = []
                    new_runes = []
                    new_passives = []
                    for cur_active_rune_index in range(0, len(cur_buildset[active_passive_index]), 2):
                        active_val = cur_buildset[active_passive_index][cur_active_rune_index]
                        rune_val = cur_buildset[active_passive_index][cur_active_rune_index + 1]
                        if active_val != -1:
                            new_actives.append(self.dclass_dict[dclass_enum_entry].active_skills[active_val])
                        else:
                            new_actives.append(Build.null_active)
                        if rune_val != -1:
                            new_runes.append(Build.rune_type[rune_val])
                        else:
                            new_runes.append(Build.null_rune)
                    for passive_val in cur_buildset[active_passive_index + 1]:
                        if passive_val != -1:
                            new_passives.append(self.dclass_dict[dclass_enum_entry].passive_skills[passive_val])
                        else:
                            new_passives.append(Build.null_passive)
                    new_build_list = [new_actives, new_runes, new_passives]
                    new_initial_buildset_list.append(new_build_list)
                new_buildset = BuildSet(dclass_enum_entry, self.campaign, self.dclass_dict[dclass_enum_entry],
                                        buildset_num, (lambda: None), new_initial_buildset_list)
                buildset_num += 1
                new_dclass_buildsets.append(new_buildset)
            new_buildset_dict[dclass_enum_entry] = new_dclass_buildsets
        new_bsl = BuildSetList(self.campaign, self.dclass_dict, new_buildset_dict)
        new_bsl.cur_dclass = current_user_selected_dclass
        self.build_set_list = new_bsl
        self.randomizer_scene.update_whatever_change()


class RandomizerScene(QtW.QGraphicsScene):
    # ~* Magic numbers for the layout *~
    # -->> Scene parameters
    scene_width = 1920
    scene_height = 1080
    # -->> Scaling parameters for newly loaded Pixmaps
    skill_size = QtC.QSize(128, 128)
    border_size = QtC.QSize(162, 162)

    # ~* All the scene elements will be held here, either as a single QGraphicsItem (or subtype) or as a list *~
    elements = {}

    def __init__(self, parent, cur_bsl: BuildSetList, campaign: Campaign):
        super().__init__(parent)
        self.setParent(parent)
        # ~* The important stuff scene needs to know: BuildSetList and Campaign *~
        self.buildsetlist = cur_bsl    # Our beloved BuildSetList, holding all the picked skills and runes
        self.campaign = campaign       # The campaign entry; lists acts and quests
        self.build = self.get_build(self.buildsetlist, self.campaign)  # The build the scene needs to currently display
        # >> Set the size of the screen
        self.setSceneRect(QtC.QRectF(0, 0, self.scene_width, self.scene_height))
        self.bg_images = None
        self.initial_scene_setup()

        # ~* Connecting signals *~
        # >> Bind the signal for quest switching
        self.campaign.quest_switched.connect(self.update_whatever_change)
        # >> Update everything after a different build has been selected
        self.buildsetlist.buildset_switched.connect(self.update_whatever_change)

        # ~* Set up the sound effects for spinning *~
        sound_effect_spin_beep_path = os.path.join('.', 'Neutral', 'beep_spin.wav')
        sound_effect_jitter_beep_path = os.path.join('.', 'Neutral', 'beep_spin.wav')
        sound_effect_build_ready_path = os.path.join('.', 'Neutral', 'build_ready.wav')
        self.sound_effect_spin_beep = QtM.QSoundEffect()
        self.sound_effect_spin_beep.setSource(QtC.QUrl.fromLocalFile(sound_effect_spin_beep_path))
        self.sound_effect_jitter_beep = QtM.QSoundEffect()
        self.sound_effect_jitter_beep.setSource(QtC.QUrl.fromLocalFile(sound_effect_jitter_beep_path))
        self.sound_effect_build_ready = QtM.QSoundEffect()
        self.sound_effect_build_ready.setSource(QtC.QUrl.fromLocalFile(sound_effect_build_ready_path))

    @staticmethod
    def get_build(buildsetlist, campaign):
        return buildsetlist.get_current_buildset().build_dict[campaign.get_current_quest()]

    # returns (active_margin, active_tween, passive_margin, passive_tween)
    def initial_scene_setup(self, active_margin_ratio=3.0, passive_margin_ratio=3.0,
                            vertical_offset_proportions=(0.05, 0.07, 0.25, 0.08, 0.15, 0.30, 0.10)):

        # ~* Acquire the 4 border pixmaps *~
        # -->> Get the path and lead the file
        four_borders_path = os.path.join('.', 'Neutral', 'borders.png')
        four_borders_pixmap = QtG.QPixmap(four_borders_path)
        # -->> Crop 4 separate pieces
        border_dark_inactive_rect = QtC.QRect(0, 0,
                                              four_borders_pixmap.width() // 2, four_borders_pixmap.height() // 2)
        # border_dark_active_rect = QtC.QRect(0, four_borders_pixmap.height() // 2,
        #                                    four_borders_pixmap.width() // 2, four_borders_pixmap.height() // 2)
        border_gold_inactive_rect = QtC.QRect(four_borders_pixmap.width() // 2, 0,
                                              four_borders_pixmap.width() // 2, four_borders_pixmap.height() // 2)
        # border_gold_active_rect = QtC.QRect(four_borders_pixmap.width() // 2, four_borders_pixmap.height() // 2,
        #                                    four_borders_pixmap.width() // 2, four_borders_pixmap.height() // 2)
        # -->> Assign 4 separate cropping rectangles
        border_dark_inactive_crop = four_borders_pixmap.copy(border_dark_inactive_rect).scaled(self.border_size)
        # border_dark_active_crop = four_borders_pixmap.copy(border_dark_active_rect)
        border_gold_inactive_crop = four_borders_pixmap.copy(border_gold_inactive_rect).scaled(self.border_size)
        # border_gold_active_crop = four_borders_pixmap.copy(border_gold_active_rect)
        # --> Get 4 border pixmaps!
        border_dark_inactive = QtG.QPixmap(border_dark_inactive_crop).scaled(self.border_size)
        # border_dark_active = QtG.QPixmap(border_dark_active_crop)
        border_gold_inactive = QtG.QPixmap(border_gold_inactive_crop).scaled(self.border_size)
        # border_gold_active = QtG.QPixmap(border_gold_active_crop)

        # ~* Load hole covers *~
        small_hole_cover_path = os.path.join('.', 'Neutral', 'Hole_cover_small.png')
        small_hole_cover = QtG.QPixmap(small_hole_cover_path)
        big_hole_cover_path = os.path.join('.', 'Neutral', 'Hole_cover_big.png')
        big_hole_cover = QtG.QPixmap(big_hole_cover_path)

        # -->> Get active skill border pixmap
        active_border_path = os.path.join('.', 'Neutral', 'active_frame.png')
        active_border = QtG.QPixmap(active_border_path).scaled(self.skill_size)

        # ~* Campaign info text (curent act & quest names) *~
        camp_text_font = random.SystemRandom().choice(QtG.QFontDatabase().families())
        camp_text_text = self.campaign.get_campaign_text()
        camp_text_size = 52
        camp_text_object = self.addSimpleText(camp_text_text, QtG.QFont(camp_text_font, camp_text_size))
        camp_text_x_coord = (self.scene_width - camp_text_object.boundingRect().width()) // 2
        camp_text_object.setX(camp_text_x_coord)
        self.elements['camp_text'] = camp_text_object

        # ~* Build info (class name and class' build number) *~
        dclass_text_font = random.SystemRandom().choice(QtG.QFontDatabase().families())
        dclass_text_text = self.build.get_dclass_text()
        dclass_text_size = 52
        dclass_text_object = self.addSimpleText(dclass_text_text, QtG.QFont(dclass_text_font, dclass_text_size))
        dclass_text_x_coord = (self.scene_width - dclass_text_object.boundingRect().width()) // 2
        dclass_text_object.setX(dclass_text_x_coord)
        self.elements['dclass_text'] = dclass_text_object

        # ~* Calculate spacing for active skill icons *~
        active_width = self.build.active[0].image.width()
        active_height = self.build.active[0].image.height()
        active_space_left = self.scene_width - (active_width * self.build.num_active)
        active_tween = active_space_left // (self.build.num_active - 1 + (2 * active_margin_ratio))
        active_margin = (active_space_left - active_tween * (self.build.num_active - 1)) // 2

        # ~* Calculate spacing for passive skill icons *~
        passive_width = self.build.passive[0].image.width()
        # passive_height = self.build.passive[0].image.height() * self.skill_scale_factor
        passive_space_left = self.scene_width - (passive_width * self.build.num_passive)
        passive_tween = passive_space_left // (self.build.num_passive - 1 + (2 * passive_margin_ratio))
        passive_margin = (passive_space_left - passive_tween * (self.build.num_passive - 1)) // 2

        # ~* Display the active skills and their runes (with borders) *~
        active_list = []
        active_border_list = []
        border_z_level = 1.0
        active_hole_cover_list = []
        hole_cover_z_level = -2.0
        rune_border_list = []
        rule_hole_cover_list = []
        rune_list = []
        for active_index in range(len(self.build.active)):
            cur_active = self.addPixmap(self.build.active[active_index].image)
            cur_active.setToolTip(self.build.active[active_index].name
                                  + ' (' + self.build.active[active_index].type + ')')
            cur_active_border = self.addPixmap(active_border)
            cur_active_hole_cover = self.addPixmap(small_hole_cover)
            cur_r_border = self.addPixmap(border_dark_inactive)
            cur_rune_hole_cover = self.addPixmap(big_hole_cover)
            cur_rune = self.addPixmap(self.build.rune[active_index])

            cur_active.setX(active_margin + active_index * (active_tween + active_width))
            cur_active_border.setX(active_margin + active_index * (active_tween + active_width))
            cur_active_border.setZValue(border_z_level)

            cur_active_hole_cover.setX(active_margin + active_index * (active_tween + active_width))
            cur_active_hole_cover.setZValue(hole_cover_z_level)

            cur_r_border.setX(active_margin + active_index * (active_tween + active_width) - 17)  # Trust me on this.
            cur_r_border.setZValue(border_z_level)

            cur_rune_hole_cover.setX(active_margin + active_index * (active_tween + active_width) - 17)
            cur_rune_hole_cover.setZValue(hole_cover_z_level)

            rune_border_padding = cur_r_border.boundingRect().width() // 2 - cur_rune.boundingRect().width() // 2
            cur_rune.setX(cur_r_border.x() + rune_border_padding)

            active_list.append(cur_active)
            active_border_list.append(cur_active_border)
            active_hole_cover_list.append(cur_active_hole_cover)
            rune_border_list.append(cur_r_border)
            rule_hole_cover_list.append(cur_rune_hole_cover)
            rune_list.append(cur_rune)
        self.elements['active'] = active_list
        self.elements['active_border'] = active_border_list
        self.elements['active_hole_cover'] = active_hole_cover_list
        self.elements['rune_border'] = rune_border_list
        self.elements['rune_hole_cover'] = rule_hole_cover_list
        self.elements['rune'] = rune_list

        # ~* Display the passive skills and their borders *~
        passive_list = []
        passive_border_list = []
        passive_hole_cover_list = []
        for passive_index in range(len(self.build.passive)):
            cur_passive = self.addPixmap(self.build.passive[passive_index].image)
            cur_passive.setToolTip(self.build.passive[passive_index].name
                                   + ' (' + self.build.passive[passive_index].type + ')')
            cur_p_border = self.addPixmap(border_gold_inactive)
            cur_p_hole_cover = self.addPixmap(big_hole_cover)
            cur_passive.setX(passive_margin + passive_index * (passive_tween + passive_width))
            cur_p_border.setX(passive_margin + passive_index * (passive_tween + passive_width) - (passive_width // 8))
            cur_p_border.setZValue(border_z_level)
            cur_p_hole_cover.setX(passive_margin + passive_index * (passive_tween + passive_width)-(passive_width // 8))
            cur_p_hole_cover.setZValue(hole_cover_z_level)

            passive_list.append(cur_passive)
            passive_border_list.append(cur_p_border)
            passive_hole_cover_list.append(cur_p_hole_cover)
        self.elements['passive'] = passive_list
        self.elements['passive_border'] = passive_border_list
        self.elements['passive_hole_cover'] = passive_hole_cover_list

        # ~* Add the 'ROLL!' button *~
        roll_button = QtW.QPushButton('ROLL!')
        roll_button.setFixedSize(159, 54)
        roll_button.setStyleSheet('QPushButton{ border-image: url(Neutral/Roll_button.png);}'
                                  'QPushButton:pressed{border-image: url(Neutral/Roll_button_pressed.png); }'
                                  '*{ background-color: rgba(0,0,0,10); }'
                                  'QPushButton{font-size: 18px; color: rgba(133,133,0,255);}'
                                  'QPushButton:pressed{font-size: 26px; color: rgba(255,255,0,255);}')
        roll_button_object = self.addWidget(roll_button)
        roll_button_x_coord = (self.scene_width - roll_button_object.boundingRect().width()) // 2
        roll_button_object.setX(roll_button_x_coord)
        self.elements['roll_button'] = roll_button_object
        # >> Connect the button to the rolling method
        roll_button.pressed.connect(self.roll_the_dice)

        object_heights = (camp_text_object.boundingRect().height(),
                          dclass_text_object.boundingRect().height(),
                          active_height, border_dark_inactive.height(),
                          border_gold_inactive.height(),
                          roll_button_object.boundingRect().height())
        vertical_offsets = (self.get_vertical_offsets(vertical_offset_proportions, self.scene_height,
                                                      *object_heights))

        # ~* Apply vertical offsets *~
        cur_v_offset = vertical_offsets[0]
        self.elements['camp_text'].setY(cur_v_offset)
        cur_v_offset += object_heights[0] + vertical_offsets[1]
        self.elements['dclass_text'].setY(cur_v_offset)
        cur_v_offset += object_heights[1] + vertical_offsets[2]
        for active in self.elements['active']:
            active.setY(cur_v_offset)
        for active_border in self.elements['active_border']:
            active_border.setY(cur_v_offset)
        for small_hole_cover in self.elements['active_hole_cover']:
            small_hole_cover.setY(cur_v_offset)
        cur_v_offset += object_heights[2] + vertical_offsets[3]
        for rune in self.elements['rune']:
            rune.setY(cur_v_offset + (object_heights[3] // 2) - self.elements['rune'][0].boundingRect().height() // 2)
        for rune_border in self.elements['rune_border']:
            rune_border.setY(cur_v_offset)
        for rune_hole_cover in self.elements['rune_hole_cover']:
            rune_hole_cover.setY(cur_v_offset)
        cur_v_offset += object_heights[3] + vertical_offsets[4]
        for passive in self.elements['passive']:
            passive.setY(cur_v_offset + object_heights[4] // 8)
        for passive_border in self.elements['passive_border']:
            passive_border.setY(cur_v_offset)
        for passive_hole_cover in self.elements['passive_hole_cover']:
            passive_hole_cover.setY(cur_v_offset)
        cur_v_offset += object_heights[4] + vertical_offsets[5]
        self.elements['roll_button'].setY(cur_v_offset)

        # ~* Get the background image for the relevant class, then cut holes in it*~
        # ~* Get all the background images, cut holes in each one, store them in a dictionary*~
        self.bg_images = dict()
        for dclass in DiabloClassEnum:
            bg_image_path = os.path.join('.', dclass.name, 'Images', 'bg.jpg')
            bg_cropped = QtG.QPixmap(bg_image_path).copy(QtC.QRect(0, 0, self.scene_width, self.scene_height))
            self.cut_holes(bg_cropped)
            self.bg_images[dclass] = bg_cropped
        # >> Load the current background image onto the scene. The rest will wait in the dictionary.
        cur_bg_image = self.bg_images[self.build.parent.dclass]
        bg_image_object = self.addPixmap(cur_bg_image)
        # >> Setting background depth
        background_depth = -1.0
        bg_image_object.setZValue(background_depth)
        self.elements['bg_image'] = bg_image_object

        self.elements['spinnable'] = []  # This will get populated with spinner stuff :S

    @staticmethod
    def get_vertical_offsets(vop: tuple, scene_height: int, camp_text_h: int, dclass_text_h: int,
                             active_h: int, rune_h, passive_h: int, button_h: int):
        vertical_offsets = []
        vertical_space_left = scene_height - camp_text_h - dclass_text_h - active_h - rune_h - passive_h - button_h
        for proportion in vop:
            offset_pix_value = int((vertical_space_left * proportion) // 1)
            vertical_offsets.append(offset_pix_value)
        return vertical_offsets

    def cut_holes(self, perforatee: QtG.QPixmap):
        the_mask = QtG.QBitmap(perforatee)
        the_mask.fill(QtC.Qt.color1)
        mask_painter = QtG.QPainter(the_mask)
        mask_painter.begin(the_mask)

        # ~* Go through active elements and make appropriately sized holes *~
        for active in self.elements['active']:
            for mini_rect in active.boundingRegion(active.sceneTransform()).rects():
                mask_painter.fillRect(mini_rect, QtC.Qt.color0)

        border_offset = 18
        for rune_box in self.elements['rune_border']:
            for mini_rect in rune_box.boundingRegion(rune_box.sceneTransform()).rects():
                adjusted_rectangles = QtC.QRect(mini_rect.x() + border_offset, mini_rect.y() + border_offset, 128, 126)
                mask_painter.fillRect(adjusted_rectangles, QtC.Qt.color0)

        for passive_box in self.elements['passive_border']:
            for mini_rect in passive_box.boundingRegion(passive_box.sceneTransform()).rects():
                adjusted_rectangles = QtC.QRect(mini_rect.x() + border_offset, mini_rect.y() + border_offset, 128,
                                                126)
                mask_painter.fillRect(adjusted_rectangles, QtC.Qt.color0)

        mask_painter.end()
        perforatee.setMask(the_mask)

    def roll_the_dice(self):
        if self.build.is_null():
            # Stop switching for the duration of the animation
            self.buildsetlist.can_switch = False
            # Make COPIES and hand these COPIES off to the Spinners
            active_question_marks = [QtW.QGraphicsPixmapItem(aqm.pixmap()) for aqm in self.elements['active']]
            rune_question_marks = [QtW.QGraphicsPixmapItem(rqm.pixmap()) for rqm in self.elements['rune']]
            passive_question_marks = [QtW.QGraphicsPixmapItem(pqm.pixmap()) for pqm in self.elements['passive']]
            # Set the positions and make the actual versions invisible
            for ai, aqm in enumerate(active_question_marks):
                aqm.setPos(self.elements['active'][ai].pos())
                self.elements['active'][ai].setVisible(False)
            for ri, rqm in enumerate(rune_question_marks):
                rqm.setPos(self.elements['rune'][ri].pos())
                self.elements['rune'][ri].setVisible(False)
            for pi, pqm in enumerate(passive_question_marks):
                pqm.setPos(self.elements['passive'][pi].pos())
                self.elements['passive'][pi].setVisible(False)

            self.build.randomize_build()
            self.build.parent.de_null()

            self.parent().spinner_contraption[BuildSetList.cur_dclass].set_qms(active_question_marks,
                                                                               rune_question_marks,
                                                                               passive_question_marks,
                                                                               self)

            self.parent().spinner_contraption[BuildSetList.cur_dclass].set_targets()
            self.parent().spinner_contraption[BuildSetList.cur_dclass].flip_the_switch()
            self.update_whatever_change()

    def update_whatever_change(self):
        # >> Update the selected build
        self.build = self.get_build(self.buildsetlist, Campaign)
        # >> Update the buildset index

        # >> Update quest information
        self.update_camp_text()
        # >> Update dclass name
        self.update_dclass_text()
        # >> Update background
        self.update_background()
        # >> Update active skill and rune information
        self.update_actives_and_runes()
        # >> Update passive skill information
        self.update_passives()
        # >> Finally, refresh the scene
        self.update(QtC.QRectF(0, 0, self.width(), self.height()))

    def update_camp_text(self):
        self.elements['camp_text'].setText(self.campaign.get_campaign_text())

    def update_dclass_text(self):
        self.elements['dclass_text'].setText(self.build.get_dclass_text())

    def update_background(self):
        self.elements['bg_image'].setPixmap(self.bg_images[self.build.parent.dclass])

    def update_actives_and_runes(self):
        for active_index in range(len(self.elements['active'])):
            self.elements['active'][active_index].setPixmap(self.build.active[active_index].image)
            self.elements['active'][active_index].setToolTip(self.build.active[active_index].name
                                                             + ' (' + self.build.active[active_index].type + ')')
            rune_pos_now = self.elements['rune'][active_index].pos()
            rune_size_half = QtC.QSize(self.elements['rune'][active_index].boundingRect().width()//2,
                                       self.elements['rune'][active_index].boundingRect().height()//2)
            rune_new_size = QtC.QSize(self.build.rune[active_index].width(), self.build.rune[active_index].height())
            rune_new_pos = QtC.QPointF(rune_pos_now.x() + rune_size_half.width() - rune_new_size.width() // 2,
                                       rune_pos_now.y() + rune_size_half.height() - rune_new_size.height() // 2)
            self.elements['rune'][active_index].setPos(rune_new_pos)
            self.elements['rune'][active_index].setPixmap(self.build.rune[active_index])

    def update_passives(self):
        for passive_index in range(len(self.elements['passive'])):
            self.elements['passive'][passive_index].setPixmap(self.build.passive[passive_index].image)
            self.elements['passive'][passive_index].setToolTip(self.build.passive[passive_index].name
                                                               + ' (' + self.build.passive[passive_index].type + ')')

    def show_randomized(self, i_type, i_index):
        self.elements[i_type][i_index].setVisible(True)

    def play_spin_beep(self):
        self.sound_effect_spin_beep.play()

    def play_jitter_beep(self):
        self.sound_effect_jitter_beep.play()

    def play_build_ready(self):
        self.sound_effect_build_ready.play()


class SpinnerContraption(QtC.QObject):
    animation_done = QtC.pyqtSignal()

    def __init__(self, dclass_dict: dict, spinner_contraption_dclass: DiabloClassEnum,
                 active_coords: list, active_height: float,
                 rune_coords: list, rune_height: float,
                 passive_coords: list, passive_height: float,
                 the_scene: QtW.QGraphicsScene,
                 framerate: int = 120, rev_min: int = 2, rev_max: int = 4):
        super().__init__()

        # ~* Get references to both your own dclass and the dict to look skills up *~
        self.dclass = spinner_contraption_dclass
        self.dclass_dict = dclass_dict

        # ~* Convert the question marks into invisible QGraphicsPixmapItem objects *~
        question_mark_active = QtW.QGraphicsPixmapItem(Build.null_active.image)
        question_mark_rune = QtW.QGraphicsPixmapItem(Build.null_rune)
        question_mark_passive = QtW.QGraphicsPixmapItem(Build.null_passive.image)
        # ~* Make them all invisible *~
        question_mark_active.setVisible(False)
        question_mark_rune.setVisible(False)
        question_mark_passive.setVisible(False)

        # ~* Convert the spinnables into a similar array *~
        spinnables_active = [QtW.QGraphicsPixmapItem(active_skill.image)
                             for active_skill in dclass_dict[spinner_contraption_dclass].active_skills]
        spinnables_rune = [QtW.QGraphicsPixmapItem(rune)
                           for rune in Build.rune_type]
        spinnables_passive = [QtW.QGraphicsPixmapItem(passive_skill.image)
                              for passive_skill in dclass_dict[spinner_contraption_dclass].passive_skills]

        # ~* Insert question mark into lists of spinnables accordingly *~
        spinnables_active.insert(0, question_mark_active)
        spinnables_rune.insert(0, question_mark_rune)
        spinnables_passive.insert(0, question_mark_passive)

        # ~* Make them all invisible *~
        for active in spinnables_active:
            active.setVisible(False)
        for rune in spinnables_rune:
            rune.setVisible(False)
        for passive in spinnables_passive:
            passive.setVisible(False)

        # <<

        interval = 1000 / framerate  # framerate in Hz into change interval, in miliseconds

        # ~* Fill 3 arrays with their respective Spinners *~
        self.active_spinners = []
        self.rune_spinners = []
        self.passive_spinners = []

        for active_index in range(len(active_coords)):
            new_active_spinner = Spinner(the_scene, the_scene.elements['active_hole_cover'][active_index],
                                         'active', active_index,
                                         active_coords[active_index], active_height, spinnables_active,
                                         interval, i_full_revolutions_min=rev_min, i_full_revolutions_max=rev_max)
            # -->> Plug the signals into the sound slot
            new_active_spinner.next_element.connect(the_scene.play_spin_beep)
            new_active_spinner.jitter_flip.connect(the_scene.play_jitter_beep)
            new_active_spinner.target_reached.connect(the_scene.show_randomized)
            self.active_spinners.append(new_active_spinner)
        for rune_index in range(len(rune_coords)):
            new_rune_spinner = Spinner(the_scene, the_scene.elements['rune_hole_cover'][rune_index],
                                       'rune', rune_index,
                                       rune_coords[rune_index], rune_height, spinnables_rune,
                                       interval, i_full_revolutions_min=rev_min, i_full_revolutions_max=rev_max)
            # -->> Plug the signals into the sound slot
            new_rune_spinner.next_element.connect(the_scene.play_spin_beep)
            new_rune_spinner.jitter_flip.connect(the_scene.play_jitter_beep)
            new_rune_spinner.target_reached.connect(the_scene.show_randomized)
            self.rune_spinners.append(new_rune_spinner)
        for passive_index in range(len(passive_coords)):
            new_passive_spinner = Spinner(the_scene, the_scene.elements['passive_hole_cover'][passive_index],
                                          'passive', passive_index,
                                          passive_coords[passive_index], passive_height, spinnables_passive,
                                          interval, i_full_revolutions_min=rev_min, i_full_revolutions_max=rev_max)
            # -->> Plug the signals into the sound slot
            new_passive_spinner.next_element.connect(the_scene.play_spin_beep)
            new_passive_spinner.jitter_flip.connect(the_scene.play_jitter_beep)
            new_passive_spinner.target_reached.connect(the_scene.show_randomized)
            self.passive_spinners.append(new_passive_spinner)

        # ~* Daisy chain the Spinners *~
        for spinner_index, spinner in enumerate(self.active_spinners):
            spinner.target_reached.connect(self.rune_spinners[spinner_index].start_spinning)
        for rs_index in range(len(self.rune_spinners) - 1):
            self.rune_spinners[rs_index].target_reached.connect(self.active_spinners[rs_index + 1].start_spinning)
        self.rune_spinners[len(self.rune_spinners) - 1].target_reached.connect(self.passive_spinners[0].start_spinning)
        for p_index in range(len(self.passive_spinners) - 1):
            self.passive_spinners[p_index].target_reached.connect(self.passive_spinners[p_index + 1].start_spinning)
        self.passive_spinners[len(self.passive_spinners) - 1].target_reached.connect(self.animation_done.emit)  # DONE!

    @staticmethod
    def get_skill_id(skill, skill_list):
        return skill_list.index(skill) + 1

    def set_targets(self):
        cur_bs = BuildSetList.buildsets[BuildSetList.cur_dclass][BuildSetList.cur_buildset]
        cur_active = cur_bs.build_dict[Campaign.get_current_quest()].active
        cur_rune = cur_bs.build_dict[Campaign.get_current_quest()].rune
        cur_passive = cur_bs.build_dict[Campaign.get_current_quest()].passive

        for a_spinner_index in range(len(self.active_spinners)):
            a_target = self.get_skill_id(cur_active[a_spinner_index], self.dclass_dict[self.dclass].active_skills)
            self.active_spinners[a_spinner_index].set_target(a_target)
        for r_spinner_index in range(len(self.rune_spinners)):
            r_target = self.get_skill_id(cur_rune[r_spinner_index], Build.rune_type)
            self.rune_spinners[r_spinner_index].set_target(r_target)
        for p_spinner_index in range(len(self.passive_spinners)):
            p_target = self.get_skill_id(cur_passive[p_spinner_index], self.dclass_dict[self.dclass].passive_skills)
            self.passive_spinners[p_spinner_index].set_target(p_target)

    def flip_the_switch(self):
        self.active_spinners[0].start_spinning()

    def set_qms(self, active_qms, rune_qms, passive_qms, the_scene):
        for asi in range(len(self.active_spinners)):
            self.active_spinners[asi].spinnables[0] = active_qms[asi]
            the_scene.addItem(active_qms[asi])
            the_scene.elements['spinnable'].append(active_qms[asi])
        for rsi in range(len(self.rune_spinners)):
            self.rune_spinners[rsi].spinnables[0] = rune_qms[rsi]
            the_scene.addItem(rune_qms[rsi])
            the_scene.elements['spinnable'].append(rune_qms[rsi])
        for psi in range(len(self.passive_spinners)):
            self.passive_spinners[psi].spinnables[0] = passive_qms[psi]
            the_scene.addItem(passive_qms[psi])
            the_scene.elements['spinnable'].append(passive_qms[psi])


class Spinner(QtC.QObject):
    next_element = QtC.pyqtSignal()
    jitter_flip = QtC.pyqtSignal()
    target_reached = QtC.pyqtSignal(str, int)

    def __init__(self, the_scene: QtW.QGraphicsScene, personal_hole_cover: QtW.QGraphicsPixmapItem,
                 i_type: str, i_id: int,
                 i_coords: QtC.QPointF, i_height: float, i_spinnables: list,
                 i_interval: float, i_pix_speed: int = -20,
                 i_full_revolutions_min: int = 2, i_full_revolutions_max: int = 4, i_revolution_slowdown: float = 0.5,
                 i_jitter_max: float = 0.75, i_jitter_factor: float = -0.9):
        super().__init__()
        # -->> Identification of Spinner's identity in relation to the build
        self.type = i_type
        self.id = i_id
        # -->> X,Y of upper left corner; how tall the slot is; list of spinnable elements
        self.coords = i_coords
        self.height = i_height
        self.spinnables = []
        self.hole_cover = personal_hole_cover

        # -->> Z levels for various steps
        self.z_strip_up = -1.0
        self.z_hole_up = -2.0
        self.z_strip_down = -3.0
        self.z_hole_down = -4.0

        # -->> Make private copies of the spinnables
        for spinnable in i_spinnables:
            self.spinnables.append(QtW.QGraphicsPixmapItem(spinnable.pixmap()))
        # -->> Make them invisible
        for spinnable in self.spinnables:
            spinnable.setVisible(False)

        if len(self.spinnables) == 6:
            self.box_offset = (self.height - self.spinnables[0].boundingRect().height()) // 2
        else:
            self.box_offset = 0

        # -->> How many pixels per update; how many full revolutions
        self.pix_speed_start = i_pix_speed
        self.pix_speed = self.pix_speed_start

        self.revolutions_min = i_full_revolutions_min
        self.revolutions_max = i_full_revolutions_max

        self.target_revolutions = self.roll_revolutions()

        self.revolution_slowdown = i_revolution_slowdown
        self.revolutions = 0
        self.jitter_max_start = i_jitter_max * self.height
        self.jitter_max = self.jitter_max_start
        self.jitter_factor = i_jitter_factor
        # -->> The timer itself; setting up the update interval
        self.timer = QtC.QTimer()
        self.timer.setInterval(i_interval)
        self.timer.timeout.connect(self.slide_tape_up)

        # -->> The position on the tape, the currently marked one
        self.target = None
        self.marker = -1

        # ~* Add everything to the scene *~
        for index, spinnable in enumerate(self.spinnables):
            the_scene.addItem(spinnable)
            the_scene.elements['spinnable'].append(spinnable)
            spinnable.setPos(self.coords.x(), self.coords.y() + index * self.height)

        # ~* Do I have runes? Because I might need to adjust my runes... *~
        if len(self.spinnables) == 6:
            for rune_index in range(1, len(self.spinnables)):

                new_rune_pos = self.adjust_rune_position(self.spinnables[rune_index])
                self.spinnables[rune_index].setPos(new_rune_pos)

    def reset_tape(self):
        self.marker = -1
        self.revolutions = 0
        self.target_revolutions = self.roll_revolutions()
        self.jitter_max = self.jitter_max_start
        self.pix_speed = self.pix_speed_start

        self.timer.disconnect()
        self.timer.timeout.connect(self.slide_tape_up)

        for index, spinnable in enumerate(self.spinnables):
            spinnable.setVisible(False)
            spinnable.setPos(self.coords.x(), self.coords.y() + index * self.height)
            if len(self.spinnables) == 6 and index > 0:
                new_rune_pos = self.adjust_rune_position(self.spinnables[index])
                self.spinnables[index].setPos(new_rune_pos)

    def roll_revolutions(self) -> int:
        return random.SystemRandom().randint(self.revolutions_min, self.revolutions_max)

    def adjust_rune_position(self, cur_rune):
        rune_zero = self.spinnables[0]

        rune_pos_now = cur_rune.pos()
        rune_size_half = QtC.QSize(rune_zero.boundingRect().width() // 2,
                                   rune_zero.boundingRect().height() // 2)
        rune_new_size = QtC.QSize(cur_rune.boundingRect().width(), cur_rune.boundingRect().height())
        rune_new_pos = QtC.QPointF(rune_pos_now.x() + rune_size_half.width() - rune_new_size.width() // 2,
                                   rune_pos_now.y() + rune_size_half.height() - rune_new_size.height() // 2)
        return rune_new_pos

    def lower_hole_and_strip(self):
        self.hole_cover.setZValue(self.z_hole_down)
        for spinnable in self.spinnables:
            spinnable.setZValue(self.z_strip_down)

    def raise_hole_and_strip(self):
        for spinnable in self.spinnables:
            spinnable.setZValue(self.z_strip_up)
        self.hole_cover.setZValue(self.z_hole_up)

    def set_target(self, target_value):
        self.target = target_value

    def start_spinning(self):
        self.spinnables[0].setVisible(True)
        self.spinnables[1].setVisible(True)
        self.lower_hole_and_strip()
        self.timer.start()

    def slide_tape_up(self):
        slack = self.spinnables[self.marker + 1].y() + self.height - self.box_offset - self.coords.y()

        prev_index = ((self.marker - 1) % (len(self.spinnables) - 1)) + 1  # Last thing at the back of the tape
        selection = (self.marker + 1) % (len(self.spinnables) - 1) + 1  # The thing currently visible in the window

        if slack >= 0:  # If the marker still have a way to go, just push everything up.
            for spinnable in self.spinnables:
                spinnable.moveBy(0, self.pix_speed)
        else:
            # ~* Emit the signal (for sound effect) *~
            self.next_element.emit()
            # ~* Is this the element we are looking for? *~
            if self.revolutions == self.target_revolutions and selection == self.target:
                    self.marker = selection
                    self.jitter()
                    self.timer.timeout.disconnect()
                    self.timer.timeout.connect(self.jitter)
                    last_visible_index = self.marker % (len(self.spinnables) - 1) + 1
                    self.spinnables[last_visible_index].setVisible(True)
            else:
                # -->> Rewound element goes invisible for now
                self.spinnables[self.marker + 1].setVisible(False)
                # -->> Place the rewound element at the very end of the tape
                self.spinnables[self.marker + 1].setPos(self.spinnables[self.marker + 1].x(),
                                                        self.spinnables[prev_index].y() + self.height)
                # -->> If the last element is in the frame, increment revolution counter
                # -->> and SLOW DOWN the revolution speed
                if self.marker == (len(self.spinnables) - 2):  # The 2 is important! We only go through #0 once!
                    self.revolutions += 1
                    self.pix_speed = self.pix_speed * self.revolution_slowdown
                # -->> Select the next element as 'marked'
                self.marker = (self.marker + 1) % (len(self.spinnables) - 1)
                # -->> Make the next element to appear visible
                last_visible_index = (self.marker + 1) % (len(self.spinnables) - 1) + 1
                self.spinnables[last_visible_index].setVisible(True)

    def jitter(self):
        prev_i = (self.marker - 2) % (len(self.spinnables) - 1) + 1
        next_i = self.marker % (len(self.spinnables) - 1) + 1
        # 1. Increase the distance in one direction all the way to jitter_max
        # 2. Then half that in the other direction, until the distance becomes < 1
        # 3. All this time - multiply the speed by jitter_factor with every switch of directions.
        distance = self.coords.y() - self.spinnables[self.marker].y() + self.box_offset
        if abs(self.jitter_max) < 0.5:
            self.timer.stop()
            self.target_reached.emit(self.type, self.id)  # Send the signal!

            self.spinnables[prev_i].setVisible(False)
            self.spinnables[next_i].setVisible(False)

            self.raise_hole_and_strip()
            self.reset_tape()
        elif (0.0 < self.jitter_max <= distance) or (0.0 > self.jitter_max >= distance):
            self.pix_speed *= self.jitter_factor
            self.jitter_max /= -2
            self.jitter_flip.emit()
        else:
            self.spinnables[prev_i].moveBy(0, self.pix_speed)
            self.spinnables[self.marker].moveBy(0, self.pix_speed)
            self.spinnables[next_i].moveBy(0, self.pix_speed)


def main():
    # Create the application
    d3_random_app = QtW.QApplication(sys.argv)
    # Create new window
    d3_win = Diablo3Window()
    # Set the window
    d3_random_app.setActiveWindow(d3_win)
    # Run the application
    sys.exit(d3_random_app.exec_())


if __name__ == "__main__":
    main()
