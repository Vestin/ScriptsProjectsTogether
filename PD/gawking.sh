# CHECK: 2 arguments
if [ "$#" -ne 2 ]
  then
    echo "syntax: ytplbackup <streamlink_path> <youtube_playlist>"
    exit 2
fi

# CHECK: proper link to streamlink
if [ ! -f "$1" ]
  then
    echo "Streamlink path invalid!"
    exit 3
fi

#PLAYLIST_ID=PLOZimZEeOJXAQ751_yGCwUczKGEUKG7oG
API_KEY="AIzaSyBdsEtj1Iz7A7T199QUT-wCdhejJW0O0Uo"
STREAMLINK_PATH=$1
PLAYLIST_ID=$2

#Get the playlist's name
PLAYLIST_NAME=$((curl -s "https://www.googleapis.com/youtube/v3/playlists?part=snippet&id=$PLAYLIST_ID&fields=items%2Fsnippet%2Ftitle&key=$API_KEY") | gawk 'match($0, /"title": "([^"]+)"/, a) {print a[1]}')
echo "Selected playlist: \"$PLAYLIST_NAME\"."

#Get the video ids
#VIDEO_ID=($(curl -s "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&maxResults=50&playlistId=$PLAYLIST_ID&fields=items%2FcontentDetails%2FvideoId&key=$API_KEY" | gawk 'match($0, /"videoId": "([^"]+)"/, a) {print a[1]}'))

#Page-aware version
VID_ID=($(curl -s "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&maxResults=50&playlistId=$PLAYLIST_ID&fields=items%2FcontentDetails%2FvideoId%2CnextPageToken&key=$API_KEY" | gawk 'match($0, /"videoId": "([^"]+)"/, array) {print array[1]}'))

NEXT_PAGE_TOKEN=($(curl -s "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&maxResults=50&playlistId=$PLAYLIST_ID&fields=items%2FcontentDetails%2FvideoId%2CnextPageToken&key=$API_KEY" | gawk 'match($0, /"nextPageToken": "([^"]+)"/, array) {print array[1]}'))


for vid in ${VID_ID[@]}
do
    echo "VID_ID: $vid"
done
echo "NPT: $NEXT_PAGE_TOKEN"

while [ $NEXT_PAGE_TOKEN ]
do
  echo "ANOTHER PAGE!"
  MORE_VID=($(curl -s "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&maxResults=50&pageToken=$NEXT_PAGE_TOKEN&playlistId=$PLAYLIST_ID&fields=items%2FcontentDetails%2FvideoId%2CnextPageToken&key=$API_KEY" | gawk 'match($0, /"videoId": "([^"]+)"/, array) {print array[1]}'))
  
  for another_vid in ${MORE_VID[@]}
  do
    echo "+ $another_vid"
    VID_ID+=($another_vid)
  done
  
  echo "NUMBER OF VIDS: ${#VID_ID[@]}"
  
  NEXT_PAGE_TOKEN=($(curl -s "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&maxResults=50&pageToken=$NEXT_PAGE_TOKEN&playlistId=$PLAYLIST_ID&fields=items%2FcontentDetails%2FvideoId%2CnextPageToken&key=$API_KEY" | gawk 'match($0, /"nextPageToken": "([^"]+)"/, array) {print array[1]}'))
  echo $NEXT_PAGE_TOKEN
done

#ASK FOR CONFIRMATION
NUMBER_OF_VIDS=${#VID_ID[@]}
echo "Number of videos: $NUMBER_OF_VIDS. Download them all (y/n)?"
read PROCEED
if [ $PROCEED == "y" ] || [ $PROCEED == "Y" ]
  then
    echo "Downloading..."
else
  exit 1
fi

#For each video_id...
ITER=1
YOUTUBE_STRING="https://www.youtube.com/watch?v="
CMD_TAIL=" best -o Video_"
CMD_EXT=".mp4"

for video in ${VID_ID[@]}
do
  DL_CMD="$STREAMLINK_PATH ""$YOUTUBE_STRING""$video""$CMD_TAIL""$ITER""$CMD_EXT"
  eval $DL_CMD
  ITER=$(($ITER+1))
done
echo "Everything has been downloaded!"