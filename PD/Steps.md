The program needs two things:
* streamlink path
* youtube playlist URL (or just the playlist code)



## Procedure

Run it like this:

```ytplbackup [streamlink_path] <youtube_playlist> [target_dir]``` 

If it finds no youtube_playlist link, it should **prompt** for one

​	If the user just presses enter it should exit and tell him the syntax!

If it does get it (somehow) it should check for the streamlink path

​	If it has been provided, run the actual thing

​	If it's not provided give the user the option to input it or just type in "y" to allow the OS to SCAN for it

### The meat of it

Once the program has both the playlist link and the streamlink path, it should do the following:

* Strip the id from the playlist link (if necessary and it's not just the id itself)
* Using YouTube API request the playlists' name
* Using YouTube API, request a list of videos belonging to that playlist
* Display ```"Playlist name: $playlist_name\nNumber of videos: $num_of_videos\n\nDOWNLOAD THEM ALL (y/n)?"```
* If no, exit
* If yes, go through the list of videos in a loop
  * for each video echo out ```"Downloading video: $video name"```
  * then call streamlink on that video and download it to the current directory (unless a directory name has been provided, in which case: download there)

I think that's it ^^!