RESPONSE=$(<new_page_version.txt)

VID_ID=$(echo "$RESPONSE" | gawk 'match($0, /"videoId": "([^"]+)"/, array) {print array[1]}')
NEXT_PAGE_TOKEN=$(echo "$RESPONSE" | gawk 'match($0, /"nextPageToken": "([^"]+)"/, array) {print array[1]}')

for line in ${VID_ID[@]}
do
  echo $line
done

if [ $NEXT_PAGE_TOKEN ]
  then
    echo "EXISTS"
  else
    echo "NOPE"
fi
echo $NEXT_PAGE_TOKEN