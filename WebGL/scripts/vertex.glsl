#version 300 es
// "location" musi byc takie same jak po stronie CPU!!!
in vec3 vertex_position;
in vec3 vertex_normal;
in vec2 vertex_tex_coord;

out vec3 fragment_coord;
out vec3 fragment_normal;
out vec2 fragment_tex_coord;

layout(std140) uniform Matrices
{
    mat4 model_matrix;
    mat4 mvp_matrix;
};
void main()
{
    gl_Position = mvp_matrix * vec4(vertex_position, 1);

    fragment_coord = vec3(model_matrix * vec4(vertex_position, 1.0));
    fragment_normal = transpose(inverse(mat3(model_matrix))) * vertex_normal;
    fragment_tex_coord = vertex_tex_coord;
}