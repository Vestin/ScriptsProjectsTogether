"use strict";

//~* Global variables *~
let gl;
let program;
let alt_program;

let camera_ubo;
let matrices_ubo;

// LIGHT UBOs
let light_ambient_ubo;
let light_directional_ubo;
let light_point_ubo;
let light_spotlight_ubo;
let material_ubo;
// END LIGHT UBOs

// LIGHT DATA ARRAYS
let light_ambient_data = new Float32Array([
    0.11, 0.01, 0.01, 0.00
]);
let light_directional_data = new Float32Array([
    1.00, 0.00, 0.00, 0.00,

     1.00,  1.00,  1.00, 0.00,
    -1.00, -1.00,  0.00, 0.00,

    0.00,  0.00,  0.00, 0.00,
    0.00,  0.00,  0.00, 0.00,

    0.00,  0.00,  0.00, 0.00,
    0.00,  0.00,  0.00, 0.00
]);
let light_point_data = new Float32Array([
    1.00, 0.00, 0.00, 0.00,

    1.00, 0.00, 0.00,
    2.00,
    1.00, 2.00, 1.00, 0.00,

    0.00, 0.00, 0.00,
    0.00,
    0.00, 0.00, 0.00, 0.00,

    0.00, 0.00, 0.00,
    0.00,
    0.00, 0.00, 0.00, 0.00
]);
let light_spotlight_data = new Float32Array([
    1.00, 0.00, 0.00, 0.00,

    0.00,  1.00,  0.00,  // color
    3.0,
    0.00,  2.00,  0.00,  // position
    Math.cos(glMatrix.toRadian(20.00 / 2.0)),
    0.00,  1.00,  0.00, 0.00,

    0.00,  0.00,  0.00,
    0.00,
    0.00,  0.00,  0.00,
    0.00,
    0.00,  0.00,  0.00, 0.00,

    0.00,  0.00,  0.00,
    0.00,
    0.00,  0.00,  0.00,
    0.00,
    0.00,  0.00,  0.00, 0.00
]);
// END DATA ARRAYS

let cameraObject;

let texture_array;
let sceneGraph;

let theGame;

//---## A crappy (NON-REDUNDANT) cube
const crappyCubeVertices = new Float32Array([
    -1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 0.25, 0.75,  // 0
     1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 0.50, 0.75,  // 1
     1.0, -1.0,-1.0, 0.0, 0.0, 0.0, 0.50, 0.50,  // 2
    -1.0, -1.0,-1.0, 0.0, 0.0, 0.0, 0.25, 0.50,  // 3
    -1.0,  1.0, 1.0, 0.0, 0.0, 0.0, 0.25, 1.0,  // 4
     1.0,  1.0, 1.0, 0.0, 0.0, 0.0, 0.50, 1.0,  // 5
     1.0,  1.0,-1.0, 0.0, 0.0, 0.0, 0.50, 0.25,  // 6
    -1.0,  1.0,-1.0, 0.0, 0.0, 0.0, 0.25, 0.25,  // 7
]);
const crappyCubeIndices = new Uint16Array([
    0, 2, 1,
    0, 3, 2,
    4, 5, 6,
    4, 6, 7,
    1, 6, 5,
    1, 2, 6,
    0, 1, 5,
    0, 5, 4,
    3, 4, 7,
    3, 0, 4,
    3, 6, 2,
    3, 7, 6
]);

//~* Stuff (data) declaration *~
//---> The models
//---### The Pyramid
//---> Format: pos_x, pos_y, pos_z, n_x, n_y, n_z, tex_u, tex_v
const pyramidVertices = new Float32Array([
    -0.5,  0.0,  0.5, -1.41,  0.0,  1.41, 0.0, 0.0,   // 0  - front left  (FRONT) | 0
    -0.5,  0.0,  0.5, -1.41,  0.0,  1.41, 1.0, 0.0,   // 02 - front left  (LEFT)  | 1
     0.5,  0.0,  0.5,  1.41,  0.0,  1.41, 1.0, 0.0,   // 1  - front right (FRONT) | 2
     0.5,  0.0,  0.5,  1.41,  0.0,  1.41, 0.0, 0.0,   // 12 - front right (RIGHT) | 3
     0.5,  0.0, -0.5,  1.41,  0.0, -1.41, 1.0, 0.0,   // 2  - back right  (RIGHT) | 4
     0.5,  0.0, -0.5,  1.41,  0.0, -1.41, 0.0, 0.0,   // 22 - back right  (BACK)  | 5
    -0.5,  0.0, -0.5, -1.41,  0.0, -1.41, 1.0, 0.0,   // 3  - back left   (BACK)  | 6
    -0.5,  0.0, -0.5, -1.41,  0.0, -1.41, 0.0, 0.0,   // 32 - back left   (LEFT)  | 7
     0.0,  0.7,  0.0,   0.0,  1.0,   0.0, 0.5, 1.0,   // 4  - top                 | 8
     0.5,  0.0, -0.5,  1.41,  0.0, -1.41, 1.0, 1.0,   // 2  - back right  (RIGHT) | 9
    -0.5,  0.0, -0.5, -1.41,  0.0, -1.41, 0.0, 1.0,   // 3  - back left   (BACK)  |10
    -0.5,  0.0,  0.5, -1.41,  0.0,  1.41, 0.0, 0.0,   // 0  - front left  (FRONT) |11 (0)
     0.5,  0.0,  0.5,  1.41,  0.0,  1.41, 1.0, 0.0    // 1  - front right (FRONT) |12 (2)
]);  // The array of vertices
const pyramidIndices = new Uint16Array([
    0,  2,  8,  // FRONT
    3,  4,  8,  // RIGHT
    5,  6,  8,  // BACK
    7,  1,  8,  // LEFT
    11,  10, 9,  // BOTTOM BACK
    11,  9,  12   // BOTTOM FRONT
]);    // The indeces of the triangles

//---### The Cross
const crossVertices = new Float32Array([
    //dol
    -0.166,	-0.5,	-0.5,        0.0, 1.0, 0.0, 2/8, 8/8,		//0     T
    0.166,	-0.5,	-0.5,        0.0, 1.0, 0.0, 3/8, 8/8,		//1     T
    0.166,	-0.5,	-0.166,	     0.0, 1.0, 0.0, 3/8, 7/8,       //2     T
    0.5,		-0.5,	-0.166,	 0.0, 1.0, 0.0, 4/8, 7/8,       //3     T
    0.5,		-0.5,	0.166,	 0.0, 1.0, 0.0, 4/8, 6/8,       //4     T
    0.166,	-0.5,	0.166,	     0.0, 1.0, 0.0, 3/8, 6/8,       //5     T
    0.166,	-0.5,	0.5,		 0.0, 1.0, 0.0, 3/8, 5/8,       //6     T
    -0.166,	-0.5,	0.5,		 0.0, 1.0, 0.0, 2/8, 5/8,       //7     T
    -0.166,	-0.5,	0.166,	     0.0, 1.0, 0.0, 2/8, 6/8,       //8     T
    -0.5,		-0.5,	0.166,	 0.0, 1.0, 0.0, 1/8, 6/8,       //9     T
    -0.5,		-0.5,	-0.166,	 0.0, 1.0, 0.0, 1/8, 7/8,       //10    T
    -0.166,	-0.5,	-0.166,	     0.0, 1.0, 0.0, 2/8, 7/8,       //11    T

//gora

    -0.166,	0.5,	-0.5,		 0.0, 1.0, 0.0, 2/8, 1/8,       //12    T
    0.166,	0.5,	-0.5,		 0.0, 1.0, 0.0, 3/8, 1/8,       //13    T
    0.166,	0.5,	-0.166,	     0.0, 1.0, 0.0, 3/8, 2/8,       //14    T
    0.5,		0.5,	-0.166,	 0.0, 1.0, 0.0, 4/8, 2/8,       //15    T
    0.5,		0.5,	0.166,	 0.0, 1.0, 0.0, 4/8, 3/8,       //16    T
    0.166,	0.5,	0.166,	     0.0, 1.0, 0.0, 3/8, 3/8,       //17    T
    0.166,	0.5,	0.5,		 0.0, 1.0, 0.0, 3/8, 4/8,       //18    T
    -0.166,	0.5,	0.5,		 0.0, 1.0, 0.0, 2/8, 4/8,       //19    T
    -0.166,	0.5,	0.166,	     0.0, 1.0, 0.0, 2/8, 3/8,       //20    T
    -0.5,		0.5,	0.166,	 0.0, 1.0, 0.0, 1/8, 3/8,       //21    T
    -0.5,		0.5,	-0.166,	 0.0, 1.0, 0.0, 1/8, 2/8,       //22    T
    -0.166,	0.5,	-0.166,	     0.0, 1.0, 0.0, 2/8, 2/8,       //23    T

//boczne

    -0.166,	-0.5,	-0.5,		0.0, 1.0, 0.0, 2/8, 8/8,         //0	#24  T
    -0.166,	-0.5,	-0.166,   	0.0, 1.0, 0.0, 2/8, 7/8,         //11	#25  T
    -0.166,	0.5,	-0.5,		0.0, 1.0, 0.0, 1/8, 8/8,         //12	#26  T
    -0.166,	0.5,	-0.166,	    0.0, 1.0, 0.0, 1/8, 7/8,         //23	#27  T

    -0.166,	-0.5,	-0.166,  	0.0, 1.0, 0.0, 2/8, 1/8,         //11	#28  T
    -0.5,		-0.5,	-0.166,	0.0, 1.0, 0.0, 1/8, 1/8,         //10	#29  T
    -0.166,	0.5,	-0.166,	    0.0, 1.0, 0.0, 2/8, 2/8,         //23	#30  T
    -0.5,		0.5,	-0.166,	0.0, 1.0, 0.0, 1/8, 2/8,         //22	#31  T

    -0.5,		-0.5,	-0.166,	0.0, 1.0, 0.0, 0/8, 2/8,         //10	#32  T
    -0.5,		-0.5,	0.166,	0.0, 1.0, 0.0, 0/8, 3/8,         //9	#33  T
    -0.5,		0.5,	-0.166,	0.0, 1.0, 0.0, 1/8, 2/8,         //22	#34  T
    -0.5,		0.5,	0.166,	0.0, 1.0, 0.0, 1/8, 3/8,         //21	#35  T

    -0.5,		-0.5,	0.166,	0.0, 1.0, 0.0, 1/8, 4/8,         //9	#36  T
    -0.166,	-0.5,	0.166,	    0.0, 1.0, 0.0, 2/8, 4/8,         //8	#37  T
    -0.5,		0.5,	0.166,	0.0, 1.0, 0.0, 1/8, 3/8,         //21	#38  T
    -0.166,	0.5,	0.166,	    0.0, 1.0, 0.0, 2/8, 3/8,         //20	#39  T

    -0.166,	-0.5,	0.166,	    0.0, 1.0, 0.0, 2/8, 6/8,         //8	#40  T
    -0.166,	-0.5,	0.5,		0.0, 1.0, 0.0, 2/8, 5/8,         //7	#41  T
    -0.166,	0.5,	0.166,	    0.0, 1.0, 0.0, 1/8, 6/8,         //20	#42  T
    -0.166,	0.5,	0.5,		0.0, 1.0, 0.0, 1/8, 5/8,         //19	#43  T

    -0.166,	-0.5,	0.5,		0.0, 1.0, 0.0, 2/8, 5/8,         //7	#44  T
    0.166,	-0.5,	0.5,		0.0, 1.0, 0.0, 3/8, 5/8,         //6	#45  T
    -0.166,	0.5,	0.5,		0.0, 1.0, 0.0, 2/8, 4/8,         //19	#46  T
    0.166,	0.5,	0.5,		0.0, 1.0, 0.0, 3/8, 4/8,         //18	#47  T

    0.166,	-0.5,	0.5,		0.0, 1.0, 0.0, 3/8, 5/8,         //6	#48  T
    0.166,	-0.5,	0.166,	    0.0, 1.0, 0.0, 3/8, 6/8,         //5	#49  T
    0.166,	0.5,	0.5,		0.0, 1.0, 0.0, 4/8, 5/8,         //18	#50  T
    0.166,	0.5,	0.166,	    0.0, 1.0, 0.0, 4/8, 6/8,         //17	#51  T

    0.166,	-0.5,	0.166,	    0.0, 1.0, 0.0, 3/8, 4/8,         //5	#52  T
    0.5,		-0.5,	0.166,	0.0, 1.0, 0.0, 4/8, 4/8,         //4	#53  T
    0.166,	0.5,	0.166,	    0.0, 1.0, 0.0, 3/8, 3/8,         //17	#54  T
    0.5,		0.5,	0.166,	0.0, 1.0, 0.0, 4/8, 3/8,         //16	#55  T

    0.5,		-0.5,	0.166,	0.0, 1.0, 0.0, 5/8, 3/8,         //4	#56  T
    0.5,		-0.5,	-0.166,	0.0, 1.0, 0.0, 5/8, 2/8,         //3	#57  T
    0.5,		0.5,	0.166,	0.0, 1.0, 0.0, 4/8, 3/8,         //16	#58  T
    0.5,		0.5,	-0.166,	0.0, 1.0, 0.0, 4/8, 2/8,         //15	#59  T

    0.5,		-0.5,	-0.166,	0.0, 1.0, 0.0, 4/8, 1/8,         //3	#60  T
    0.166,	-0.5,	-0.166,	    0.0, 1.0, 0.0, 3/8, 1/8,         //2	#61  T
    0.5,		0.5,	-0.166,	0.0, 1.0, 0.0, 4/8, 2/8,         //15	#62  T
    0.166,	0.5,	-0.166,	    0.0, 1.0, 0.0, 3/8, 2/8,         //14	#63  T

    0.166,	-0.5,	-0.166,	    0.0, 1.0, 0.0, 3/8, 7/8,         //2	#64  T
    0.166,	-0.5,	-0.5,		0.0, 1.0, 0.0, 3/8, 8/8,         //1	#65  T
    0.166,	0.5,	-0.166,	    0.0, 1.0, 0.0, 4/8, 7/8,         //14	#66  T
    0.166,	0.5,	-0.5,		0.0, 1.0, 0.0, 4/8, 8/8,         //13	#67  T

    0.166,	-0.5,	-0.5,		0.0, 1.0, 0.0, 3/8, 0/8,         //1	#68  T
    -0.166,	-0.5,	-0.5,		0.0, 1.0, 0.0, 2/8, 0/8,         //0	#69  T
    0.166,	0.5,	-0.5,		0.0, 1.0, 0.0, 3/8, 1/8,         //13	#70  T
    -0.166,	0.5,	-0.5,		0.0, 1.0, 0.0, 2/8, 1/8          //12	#71  T 
]);
const crossIndices = new Uint16Array([
    //dol
    0,		1,		11,
    1,		2,		11,
    10,	11,	9,
    11,	8,		9,
    11,	2,		8,
    2,		5,		8,
    2,		3,		5,
    3,		4,		5,
    8,		5,		7,
    5,		6,		7,

//gora
    23,	13,	12,
    23,	14,	13,
    21,	23,	22,
    21,	20,	23,
    20,	14,	23,
    20,	17,	14,
    17,	15,	14,
    17,	16,	15,
    19,	17,	20,
    19,	18,	17,

//boczne
    24,	25,	26,
    26,	25,	27,
    28,	29,	30,
    30,	29,	31,
    32,	33,	34,
    34,	33,	35,
    36,	37,	38,
    38,	37,	39,
    40,	41,	42,
    42,	41,	43,
    44,	45,	46,
    46,	45,	47,
    48,	49,	50,
    50,	49,	51,
    52,	53,	54,
    54,	53,	55,
    56,	57,	58,
    58,	57,	59,
    60,	61,	62,
    62,	61,	63,
    64,	65,	66,
    66,	65,	67,
    68,	69,	70,
    70,	69,	71
]);

//---### The Naught
const naughtVertices = new Float32Array([
//dol
    0.25,			-0.5,		-0.5,     0.0, 1.0, 0.0, 5/8, 2/8,                  	//0
    0.5,			-0.5,		-0.25,    0.0, 1.0, 0.0, 6/8, 3/8,                    	//1
    0.5,			-0.5,		0.25,     0.0, 1.0, 0.0, 6/8, 5/8,                   	//2
    0.25,			-0.5,		0.5,      0.0, 1.0, 0.0, 5/8, 6/8,                  	//3
    -0.25,		-0.5,		0.5,	      0.0, 1.0, 0.0, 3/8, 6/8,                  	//4
    -0.5,			-0.5,		0.25,     0.0, 1.0, 0.0, 2/8, 5/8,                   	//5
    -0.5,			-0.5,		-0.25,    0.0, 1.0, 0.0, 2/8, 3/8,                    	//6
    -0.25,		-0.5,		-0.5,	      0.0, 1.0, 0.0, 3/8, 2/8,                  	//7
    0.0,			-0.5,		-0.25,    0.0, 1.0, 0.0, 4/8, 3/8,                    	//8
    0.25,			-0.5,		0.0,      0.0, 1.0, 0.0, 5/8, 4/8,                  	//9
    0.0,			-0.5,		0.25,     0.0, 1.0, 0.0, 4/8, 5/8,                   	//10
    -0.25,		-0.5,		0.0,	      0.0, 1.0, 0.0, 3/8, 4/8,                  	//11
//gora
    0.25,			0.5,		-0.5,     0.0, 1.0, 0.0, 5/8, 2/8,                   	//12
    0.5,			0.5,		-0.25,    0.0, 1.0, 0.0, 6/8, 3/8,                    	//13
    0.5,			0.5,		0.25,     0.0, 1.0, 0.0, 6/8, 5/8,                   	//14
    0.25,			0.5,		0.5,      0.0, 1.0, 0.0, 5/8, 6/8,                  	//15
    -0.25,		0.5,		0.5,	      0.0, 1.0, 0.0, 3/8, 6/8,                  	//16
    -0.5,			0.5,		0.25,     0.0, 1.0, 0.0, 2/8, 5/8,                   	//17
    -0.5,			0.5,		-0.25,    0.0, 1.0, 0.0, 2/8, 3/8,                    	//18
    -0.25,		0.5,		-0.5,	      0.0, 1.0, 0.0, 3/8, 2/8,                  	//19
    0.0,			0.5,		-0.25,    0.0, 1.0, 0.0, 4/8, 3/8,                    	//20
    0.25,			0.5,		0.0,      0.0, 1.0, 0.0, 5/8, 4/8,                  	//21
    0.0,			0.5,		0.25,     0.0, 1.0, 0.0, 4/8, 5/8,                   	//22
    -0.25,		0.5,		0.0,	      0.0, 1.0, 0.0, 3/8, 4/8                   	//23
// //bok
//     0.5,			-0.5,		-0.25,    0.0, 1.0, 0.0, 0.0, 0.0,                    	//1	#24
//     0.25,			-0.5,		-0.5,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//0	#25
//     0.5,			0.5,		-0.25,    0.0, 1.0, 0.0, 0.0, 0.0,                    	//13	#26
//     0.25,			0.5,		-0.5,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//12	#27
//                                           0.0, 1.0, 0.0, 0.0, 0.0,
//     0.5,			-0.5,		0.25,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//2	#28
//     0.5,			-0.5,		-0.25,    0.0, 1.0, 0.0, 0.0, 0.0,                    	//1	#29
//     0.5,			0.5,		0.25,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//14	#30
//     0.5,			0.5,		-0.25,    0.0, 1.0, 0.0, 0.0, 0.0,                    	//13	#31
//                                           0.0, 1.0, 0.0, 0.0, 0.0,
//     0.25,			-0.5,		0.5,      0.0, 1.0, 0.0, 0.0, 0.0,                  	//3	#32
//     0.5,			-0.5,		0.25,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//2	#33
//     0.25,			0.5,		0.5,      0.0, 1.0, 0.0, 0.0, 0.0,                  	//15	#34
//     0.5,			0.5,		0.25,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//14	#35
//                                           0.0, 1.0, 0.0, 0.0, 0.0,
//     -0.25,		-0.5,		0.5,	      0.0, 1.0, 0.0, 0.0, 0.0,                  	//4	#36
//     0.25,			-0.5,		0.5,      0.0, 1.0, 0.0, 0.0, 0.0,                  	//3	#37
//     -0.25,		0.5,		0.5,	      0.0, 1.0, 0.0, 0.0, 0.0,                  	//16	#38
//     0.25,			0.5,		0.5,      0.0, 1.0, 0.0, 0.0, 0.0,                  	//15	#39
//                                           0.0, 1.0, 0.0, 0.0, 0.0,
//     -0.5,			-0.5,		0.25,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//5	#40
//     -0.25,		-0.5,		0.5,	      0.0, 1.0, 0.0, 0.0, 0.0,                  	//4	#41
//     -0.5,			0.5,		0.25,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//17	#42
//     -0.25,		0.5,		0.5,	      0.0, 1.0, 0.0, 0.0, 0.0,                  	//16	#43
//                                           0.0, 1.0, 0.0, 0.0, 0.0,
//     -0.5,			-0.5,		-0.25,    0.0, 1.0, 0.0, 0.0, 0.0,                    	//6	#44
//     -0.5,			-0.5,		0.25,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//5	#45
//     -0.5,			0.5,		-0.25,    0.0, 1.0, 0.0, 0.0, 0.0,                    	//18	#46
//     -0.5,			0.5,		0.25,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//17	#47
//                                           0.0, 1.0, 0.0, 0.0, 0.0,
//     -0.25,		-0.5,		-0.5,	      0.0, 1.0, 0.0, 0.0, 0.0,                  	//7	#48
//     -0.5,			-0.5,		-0.25,    0.0, 1.0, 0.0, 0.0, 0.0,                    	//6	#49
//     -0.25,		0.5,		-0.5,	      0.0, 1.0, 0.0, 0.0, 0.0,                  	//19	#50
//     -0.5,			0.5,		-0.25,    0.0, 1.0, 0.0, 0.0, 0.0,                    	//18	#51
//                                           0.0, 1.0, 0.0, 0.0, 0.0,
//     0.25,			-0.5,		-0.5,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//0	#52
//     -0.25,		-0.5,		-0.5,	      0.0, 1.0, 0.0, 0.0, 0.0,                  	//7	#53
//     0.25,			0.5,		-0.5,     0.0, 1.0, 0.0, 0.0, 0.0,                   	//12	#54
//     -0.25,		0.5,		-0.5,	      0.0, 1.0, 0.0, 0.0, 0.0                   	//19	#55
]);
const naughtIndices = new Uint16Array([
//dol
    0,		8,		7,
    0,		1,		8,
    1,		9,		8,
    1,		2,		9,
    9,		2,		10,
    10,	2,		3,
    10,	3,		4,
    10,	4,		5,
    11,	10,	5,
    11,	5,		6,
    11,	6,		8,
    8,		6,		7,
//gora
    20,	12,	19,
    20,	13,	12,
    20,	21,	13,
    21,	14,	13,
    21,	22,	14,
    22,	15,	14,
    22,	16,	15,
    22,	17,	16,
    22,	23,	17,
    23,	18,	17,
    23,	20,	18,
    20,	19,	18,
//bok
    // BACK RIGHT
    1,	0,	13,
    13,	0,	12,
    // RIGHT
    2,	1,	14,
    14,	1,	13,
    // FRONT RIGHT
    3,	2,	15,
    15,	2,	14,
    // FRONT
    4,	3,	16,
    16,	3,	15,
    // FRONT LEFT
    5,	4,	17,
    17,	4,	16,
    // LEFT
    6,	5,	18,
    18,	5,	17,
    // BACK LEFT
    7,	6,	19,
    19,	6,	18,
    // BACK
    0,	7,	12,
    12,	7,	19,

    // INSIDE TOP-LEFT
    11, 8, 23,
    23, 8, 20,
    // INSIDE TOP-RIGHT
    8, 9, 20,
    20, 9, 21,
    // INSIDE BOTTOM-LEFT
    9, 10, 21,
    21, 10, 22,
    // INSIDE BOTTOM-RIGHT
    10, 11, 22,
    22, 11, 23
]);

//---## Tablecloth
const tableclothVertices = new Float32Array([
    -1.00,			0.00,		 1.00,     0.0, 1.0, 0.0, 0.0, 1.0, //0
     1.00,			0.00,		 1.00,     0.0, 1.0, 0.0, 1.0, 1.0, //1
     1.00,			0.00,		-1.00,     0.0, 1.0, 0.0, 1.0, 0.0, //2
    -1.00,			0.00,		-1.00,     0.0, 1.0, 0.0, 0.0, 0.0, //3
]);
const tableclothIndices = new Uint16Array([
    0, 1, 3,
    1, 2, 3
]);

//~* Textures *~
const texture_name_list = ["./eye_sq.png",
    "./carpet2_1024.jpg",
    "./res/textures/TexturesCom_Fiberglass0026_1_seamless_S.jpg",
    "./res/textures/TexturesCom_Electronics0032_M.jpg",
    "./res/textures/TempGrid.png",
    "./res/textures/TempGridNaught.png",
    null
];

//~* Class declaration *~
class SceneNode{
    constructor(sceneArray){
        this.thingy_model = sceneArray[0];
        this.last_timestamp = 0.0;

        this.initial_scaling = vec3.fromValues(sceneArray[1][0], sceneArray[1][1], sceneArray[1][2]);
        this.delta_scaling = vec3.fromValues(sceneArray[2][0], sceneArray[2][1], sceneArray[2][2]);

        this.initial_rotation = vec3.fromValues(sceneArray[3][0], sceneArray[3][1], sceneArray[3][2]);
        this.delta_rotation = vec3.fromValues(sceneArray[4][0], sceneArray[4][1], sceneArray[4][2]);

        this.initial_translation = vec3.fromValues(sceneArray[5][0], sceneArray[5][1], sceneArray[5][2]);
        this.delta_translation = vec3.fromValues(sceneArray[6][0], sceneArray[6][1],sceneArray[6][2]);

        this.initial_rotation_self = vec3.fromValues(sceneArray[7][0], sceneArray[7][1], sceneArray[7][2]);
        this.delta_rotation_self = vec3.fromValues(sceneArray[8][0], sceneArray[8][1], sceneArray[8][2]);
        this.children = [];
        for (let child_i = 0; child_i < sceneArray[9].length; child_i++){
            this.children.push(new SceneNode(sceneArray[9][child_i]));
        }
    }
    draw(currentModelMatrix, timestamp){
        if (!currentModelMatrix) currentModelMatrix = mat4.create();
        //~* Calculate how things are supposed to move *~
        this.updateValues(timestamp);
        this.last_timestamp = timestamp;

        //~* Get the angle of turning into a quaternion *~
        let aimingAngle = quat.create();
        quat.fromEuler(aimingAngle, this.initial_rotation[0], this.initial_rotation[1], this.initial_rotation[2]);
        //~*Turning around its own center*~
        let spurious_angle_self = quat.create();
        quat.fromEuler(spurious_angle_self, this.initial_rotation_self[0], this.initial_rotation_self[1], this.initial_rotation_self[2]);

        //~* Turn, translate, SAVE, turn, scale *~
        let allOpsMatrix = mat4.create();
        mat4.copy(allOpsMatrix, currentModelMatrix); // It's passed by value. You want a local copy
        //~* Turn, aiming *~
        let aimingRotation = mat4.create();
        mat4.fromQuat(aimingRotation, aimingAngle);
        mat4.multiply(allOpsMatrix, allOpsMatrix, aimingRotation);
        //~* Translate *~
        mat4.translate(allOpsMatrix, allOpsMatrix, this.initial_translation);
        //---> Save the state to return to children!
        let relativeModelMatrix = mat4.create();
        mat4.copy(relativeModelMatrix, allOpsMatrix);
        //~* Get rotation around own center and scaling *~
        let selfOps = mat4.create();
        mat4.fromRotationTranslationScale(selfOps, spurious_angle_self, vec3.fromValues(0,0,0), this.initial_scaling);

        //Apply general ops
        //mat4.multiply(allOpsMatrix, allOpsMatrix, currentModelMatrix);
        //Apply the self-ops
        mat4.multiply(allOpsMatrix, allOpsMatrix, selfOps);

        //~* Draw the result *~
        if (this.thingy_model) this.thingy_model.tDraw(allOpsMatrix);

        //~* Perform the same recursively for all child nodes *~
        for (let child_i = 0; child_i < this.children.length; child_i++){
            this.children[child_i].draw(relativeModelMatrix, timestamp);
        }
    }

    updateValues(timestamp){
        let delta_time = timestamp - this.last_timestamp;

        vec3.scaleAndAdd(this.initial_scaling, this.initial_scaling, this.delta_scaling, delta_time);
        vec3.scaleAndAdd(this.initial_rotation, this.initial_rotation, this.delta_rotation, delta_time);
        vec3.scaleAndAdd(this.initial_translation, this.initial_translation, this.delta_translation, delta_time);
        vec3.scaleAndAdd(this.initial_rotation_self, this.initial_rotation_self, this.delta_rotation_self, delta_time);
    }
}

class Thingy{
    constructor(vertex_arr, index_arr, texture_arr, spec_i, spec_p, metal){
        Thingy.generate_normals(vertex_arr, index_arr);

        this.vao = null;
        //~* Vertex Buffer Object *~
        this.vbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.bufferData(gl.ARRAY_BUFFER, vertex_arr, gl.STATIC_DRAW);
        //gl.bindBuffer(gl.ARRAY_BUFFER, null);
        //~* Element Array Object *~
        this.eao = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.eao);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, index_arr, gl.STATIC_DRAW);
        this.element_count = index_arr.length;
        //gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

        //~* Attributes *~

        let vertex_position_location = gl.getAttribLocation(program, "vertex_position");
        let vertex_normal_location = gl.getAttribLocation(program, "vertex_normal");
        let vertex_texture_coord_location = gl.getAttribLocation(program, "vertex_tex_coord");
        //~* Vertex Attribute Object *~
        this.vao = gl.createVertexArray();
        gl.bindVertexArray(this.vao);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.eao);
        gl.enableVertexAttribArray(vertex_position_location);
        gl.vertexAttribPointer(vertex_position_location, 3, gl.FLOAT, false, 8*4, 0);
        gl.enableVertexAttribArray(vertex_normal_location);
        gl.vertexAttribPointer(vertex_normal_location, 3, gl.FLOAT, false, 8*4, 3*4);
        gl.enableVertexAttribArray(vertex_texture_coord_location);
        gl.vertexAttribPointer(vertex_texture_coord_location, 2, gl.FLOAT, false, 8*4, 6*4);
        gl.bindVertexArray(null);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
        this.textures = texture_arr;

        this.materialArray = new Float32Array([spec_i, spec_p, metal, 0.0]);
    };
    tDraw(currentModelMatrix){

         //~* Activate this object's model to be drawn *~
        gl.bindVertexArray(this.vao);

        //~* Update the buffer data *~
        let mvpMatrix = mat4.create();
        mat4.multiply(mvpMatrix, cameraObject.getViewProjectionMatrix(), currentModelMatrix);

        //--->> Update matrices!
        gl.bindBuffer(gl.UNIFORM_BUFFER, matrices_ubo);
        // gl.bufferData(gl.UNIFORM_BUFFER, both_matrices, gl.DYNAMIC_DRAW);
        gl.bufferSubData(gl.UNIFORM_BUFFER, 0, currentModelMatrix, 0, 0);
        gl.bufferSubData(gl.UNIFORM_BUFFER, 4*4*4, mvpMatrix, 0, 0);

        //~* BIND THE NEW LIGHT ARRAYS! *~
        gl.bindBuffer(gl.UNIFORM_BUFFER, light_ambient_ubo);
        gl.bufferData(gl.UNIFORM_BUFFER, light_ambient_data, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.UNIFORM_BUFFER, light_directional_ubo);
        gl.bufferData(gl.UNIFORM_BUFFER, light_directional_data, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.UNIFORM_BUFFER, light_point_ubo);
        gl.bufferData(gl.UNIFORM_BUFFER, light_point_data, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.UNIFORM_BUFFER, light_spotlight_ubo);
        gl.bufferData(gl.UNIFORM_BUFFER, light_spotlight_data, gl.DYNAMIC_DRAW);

        //~* BIND that juicy material data *~
        gl.bindBuffer(gl.UNIFORM_BUFFER, material_ubo);
        gl.bufferData(gl.UNIFORM_BUFFER, this.materialArray, gl.DYNAMIC_DRAW);

        //~* Bind the textures to be drawn *~
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture_array[this.textures[0]].gl_texture);
        gl.uniform1i(gl.getUniformLocation(program, "eye_texture"), 0);
        gl.activeTexture(gl.TEXTURE0 +  1);
        gl.bindTexture(gl.TEXTURE_2D, texture_array[this.textures[1]].gl_texture);
        gl.uniform1i(gl.getUniformLocation(program, "some_other_texture"), 1);

        //~* Finally, put the vertices at their destination *~
        if (this.textures[1] === 0){
            gl.useProgram(alt_program);
            gl.bindVertexArray(this.vao);
        }
        gl.drawElements(gl.TRIANGLES, this.element_count, gl.UNSIGNED_SHORT, 0);
        if (this.textures[1] === 0) gl.useProgram(program);
    }
    static generate_normals(vertex_array, index_array){
        let vabs = 8; //vertex array block size
        let averagables = [];
        for (let i=0; i< index_array.length; i++){
            averagables.push([]);
        }
        // console.log(averagables);

        for (let i=0; i< index_array.length; i+=3){
            let row_one = vabs * index_array[i];
            let row_two = vabs * index_array[i+1];
            let row_three = vabs * index_array[i+2];

            let left_point = vec3.fromValues(vertex_array[row_one], vertex_array[row_one+1], vertex_array[row_one+2]);
            let middle_point = vec3.fromValues(vertex_array[row_two], vertex_array[row_two+1], vertex_array[row_two+2]);
            let right_point = vec3.fromValues(vertex_array[row_three], vertex_array[row_three+1], vertex_array[row_three+2]);

            let left_vector = vec3.create();
            let right_vector = vec3.create();

            vec3.sub(left_vector, left_point, middle_point);
            vec3.sub(right_vector, right_point, middle_point);
            vec3.normalize(left_vector, left_vector);
            vec3.normalize(right_vector, right_vector);

            let normal = vec3.create();
            vec3.cross(normal, right_vector, left_vector);
            vec3.normalize(normal, normal);

            averagables[index_array[i]].push(normal);
            averagables[index_array[i + 1]].push(normal);
            averagables[index_array[i + 2]].push(normal);
        }

        for (let i=0; i< averagables.length; i++){
            let averaged_normal = vec3.create();
            for (let j=0; j<averagables[i].length; j++) vec3.add(averaged_normal, averaged_normal, averagables[i][j]);
            vec3.normalize(averaged_normal, averaged_normal);

            let field_of_index = (vabs * i) + 3;

            vertex_array[field_of_index] = averaged_normal[0];
            vertex_array[field_of_index + 1] = averaged_normal[1];
            vertex_array[field_of_index + 2] = averaged_normal[2];
        }
    }
}

class Texture{
    constructor(image_uri){
        let that = this;

        const width = 1;
        const height = 1;

        if (!Texture.generated_monstrosity){
            let curPixel = new Uint8Array(4);
            Texture.generated_monstrosity = new Uint8Array(height*width*4);
            //Texture.generated_monstrosity = new Uint8Array([255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255]);
            for (let v=0; v<height; v++){
                for (let u=0; u<(width*4); u+=4){
                    // I give up. There's very little point in this anyway.
                    // Now this makes even LESS sense... On a positive note - smooth.
                    curPixel[0] = Math.floor(Math.random() * Math.floor(255));
                    curPixel[1] = Math.floor(Math.random() * Math.floor(255));
                    curPixel[2] = Math.floor(Math.random() * Math.floor(255));
                    curPixel[3] = 255;

                    Texture.generated_monstrosity[(width * 4 * v) + u] = curPixel[0];
                    Texture.generated_monstrosity[(width * 4 * v) + u + 1] = curPixel[1];
                    Texture.generated_monstrosity[(width * 4 * v) + u + 2] = curPixel[2];
                    Texture.generated_monstrosity[(width * 4 * v) + u + 3] = curPixel[3];
                }
            }
        }

        const level = 0;
        const internalFormat = gl.RGBA;
        const srcFormat = gl.RGBA;
        const srcType = gl.UNSIGNED_BYTE;
        let active_tex_index = gl.TEXTURE0 + Texture.numOfTextures;
        // Load up the temporary, single-pixel texture
        this.gl_texture = gl.createTexture();
        gl.activeTexture(active_tex_index);
        gl.bindTexture(gl.TEXTURE_2D, this.gl_texture);
        const border = 0;
        gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
            width, height, border, srcFormat, srcType,
            Texture.generated_monstrosity);
        gl.generateMipmap(gl.TEXTURE_2D);

        // This part concerns the image which shall eventually (asynchronously) be acquired.
        if (image_uri){
            let source_image = new Image();
            source_image.src = image_uri;
            source_image.onload = function () {
                gl.activeTexture(active_tex_index);
                gl.bindTexture(gl.TEXTURE_2D, that.gl_texture);
                gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
                    srcFormat, srcType, source_image);
                gl.generateMipmap(gl.TEXTURE_2D);
            };
        }
        Texture.numOfTextures++; // Increment the static texture counter
    }
}

function getSampler(){
    let only_sampler = gl.createSampler();

    gl.samplerParameteri(only_sampler, gl.TEXTURE_WRAP_S, gl.REPEAT);
    gl.samplerParameteri(only_sampler, gl.TEXTURE_WRAP_T, gl.REPEAT);
    gl.samplerParameteri(only_sampler, gl.TEXTURE_WRAP_R, gl.REPEAT);
    gl.samplerParameteri(only_sampler, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
    gl.samplerParameteri(only_sampler, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

    return only_sampler;
}

class ViewProjection{
    constructor(){
        this.camera_pos = [1., 3., 5.];
        this.camera_look = [1., 0., 1.];
        this.camera_up = [0., 1., 0.];

        this.viewProjectionMatrix = mat4.create();
        this.viewMatrix = mat4.create();
        this.projectionMatrix = mat4.create();

        mat4.perspective(this.projectionMatrix, Math.PI/4., gl.drawingBufferWidth/gl.drawingBufferHeight, 0.1, 300);
        this.setCamera();
    }

    getViewProjectionMatrix(){
        return this.viewProjectionMatrix; // TODO: check whether this is needed at all!
    }

    getCameraPosition(){
        return this.camera_pos;
    }

    setCamera(i_pos = this.camera_pos, i_look = this.camera_look, i_up = this.camera_up){
        this.camera_pos = new Float32Array(i_pos);
        this.camera_look = new Float32Array(i_look);
        this.camera_up = new Float32Array(i_up);  // Assign the positional variables

        mat4.lookAt(this.viewMatrix, i_pos, i_look, i_up);  // Assign a new View Matrix
        mat4.multiply(this.viewProjectionMatrix, this.projectionMatrix, this.viewMatrix);  // Recalculate VP
    }
}

class NaughtsAndCrossesGame{
    constructor(){
        // 0 - empty, 1 - naught, 2 - cross
        this.board = [
            [0,0,0],
            [0,0,0],
            [0,0,0]
        ];
        this.selected = [0, 0];
        this.active_player = 1;
        this.models = [];
        this.playable = true;
        // This is where "DRY" goes to die. I know. Move along.
        let modelPyramid = new Thingy(pyramidVertices, pyramidIndices, [2,2], 1.0, 100.0, 0.75);
        let modelNaught = new Thingy(naughtVertices, naughtIndices, [3,3], 1.0, 200.0, 0.25);
        let modelCross = new Thingy(crossVertices, crossIndices, [4,4], 1.0, 200.0, 0.25);
        this.models.push(modelPyramid, modelNaught, modelCross);
    }

    next_player(){
        this.active_player = (this.active_player % 2) + 1;
    }

    make_move(){
        // console.log(this.board);
        // console.log(this.board[this.selected[1]][this.selected[1]]);
        if (this.board[this.selected[0]][this.selected[1]] === 0 && this.playable){
            let modelNum = this.selected[1]*3 + this.selected[0];
            this.board[this.selected[0]][this.selected[1]] = this.active_player;
            sceneGraph.children[modelNum].thingy_model = this.models[this.active_player];
            sceneGraph.children[modelNum].initial_scaling[1] = 0.15;
            let victory = this.check_victory();
            if (this.is_board_full() || victory !== 0){
                NaughtsAndCrossesGame.victory_display(victory);
                this.playable = false;
            }
            this.next_player();
        }
    }

    move_selected(x_delta, y_delta){
        this.selected = [(3 + (this.selected[0] + x_delta)) % 3, (3 + (this.selected[1] + y_delta)) % 3];
        light_spotlight_data[8] = this.selected[0]; light_spotlight_data[10] = this.selected[1] % 3;
        sceneGraph.children[9].initial_translation[0] = this.selected[0];
        sceneGraph.children[9].initial_translation[2] = this.selected[1];
    }

    check_victory(){
        // Returns 0 for no victory, 1 for naught victory, 2 for cross victory

        // Checks columns and rows
        for (let i=0; i<3; i++){
            if (this.board[i][0] === this.board[i][1] && this.board[i][1] === this.board[i][2] && this.board[i][1] !== 0){
                return this.board[i][1];
            }
            if (this.board[0][i] === this.board[1][i] && this.board[1][i] === this.board[2][i] && this.board[1][i] !== 0){
                return this.board[1][i];
            }
        }
        // Check the diagonals
        if (this.board[0][0] === this.board[1][1] && this.board[1][1] === this.board[2][2] && this.board[1][1] !== 0){
            return this.board[1][1];
        }
        if (this.board[0][2] === this.board[1][1] && this.board[1][1] === this.board[2][0] && this.board[1][1] !== 0){
            return this.board[1][1];
        }
        return 0; // If no victory, return 0
    }

    is_board_full(){
        for(let y=0; y<3; y++){
            for(let x=0; x<3; x++){
                if (this.board[x][y] === 0) return false;
            }
        }
        return true;
    }

    static victory_display(victory_value){
        if (victory_value !== 0){
            let victorIndex = 9 + victory_value;
            sceneGraph.children[victorIndex].initial_translation[0] = 1;
            sceneGraph.children[victorIndex].initial_translation[1] = 0.5;
            sceneGraph.children[victorIndex].initial_translation[2] = 1;
            sceneGraph.children[victorIndex].initial_translation[2] = 1;
            sceneGraph.children[victorIndex].initial_rotation_self[0] = 0;
            sceneGraph.children[victorIndex].initial_rotation_self[1] = 0;
            sceneGraph.children[victorIndex].initial_rotation_self[2] = 0;
            sceneGraph.children[victorIndex].delta_rotation_self[2] = 0.0;
            sceneGraph.children[victorIndex].delta_rotation_self[1] = 0.5;
        } else {
            let naughtIndex = 10;
            let crossIndex = 11;

            sceneGraph.children[naughtIndex].initial_translation[0] = 1;
            sceneGraph.children[naughtIndex].initial_translation[1] = 0.5;
            sceneGraph.children[naughtIndex].initial_translation[2] = 1.0;
            sceneGraph.children[naughtIndex].initial_rotation_self[0] = 0;
            sceneGraph.children[naughtIndex].initial_rotation_self[1] = 0;
            sceneGraph.children[naughtIndex].initial_rotation_self[2] = 0;
            sceneGraph.children[naughtIndex].delta_rotation_self[2] = 0.0;
            sceneGraph.children[naughtIndex].delta_rotation_self[1] = 0.333;

            sceneGraph.children[crossIndex].initial_translation[0] = 1;
            sceneGraph.children[crossIndex].initial_translation[1] = 0.75;
            sceneGraph.children[crossIndex].initial_translation[2] = 1.0;
            sceneGraph.children[crossIndex].initial_rotation_self[0] = 90;
            sceneGraph.children[crossIndex].initial_rotation_self[1] = 0;
            sceneGraph.children[crossIndex].initial_rotation_self[2] = 0;
            sceneGraph.children[crossIndex].delta_rotation_self[2] = 0.0;
            sceneGraph.children[crossIndex].delta_rotation_self[1] = -0.333;
        }
    }

    reset_board(){
        for (let i=0; i<9; i++){
            this.board = [
                [0,0,0],
                [0,0,0],
                [0,0,0]
            ];
            this.selected = [0, 0];
            this.active_player = 1;
            light_spotlight_data[8] = this.selected[0]; light_spotlight_data[10] = this.selected[1] % 3;
            sceneGraph.children[9].initial_translation[0] = this.selected[0];
            sceneGraph.children[9].initial_translation[2] = this.selected[1];

            sceneGraph.children[i].thingy_model = this.models[0];
            sceneGraph.children[i].initial_scaling[1] = 0.5;

            let naughtIndex = 10;
            let crossIndex = 11;

            sceneGraph.children[naughtIndex].initial_translation[0] = -2;
            sceneGraph.children[naughtIndex].initial_translation[1] = 0.0;
            sceneGraph.children[naughtIndex].initial_translation[2] = -3.0;
            sceneGraph.children[naughtIndex].initial_rotation_self[0] = 90;
            sceneGraph.children[naughtIndex].initial_rotation_self[1] = 0;
            sceneGraph.children[naughtIndex].initial_rotation_self[2] = 0;
            sceneGraph.children[naughtIndex].delta_rotation_self[2] = -0.1;
            sceneGraph.children[naughtIndex].delta_rotation_self[1] = 0.0;

            sceneGraph.children[crossIndex].initial_translation[0] = 4.0;
            sceneGraph.children[crossIndex].initial_translation[1] = 0.0;
            sceneGraph.children[crossIndex].initial_translation[2] = -3.0;
            sceneGraph.children[crossIndex].initial_rotation_self[0] = 90;
            sceneGraph.children[crossIndex].initial_rotation_self[1] = 0;
            sceneGraph.children[crossIndex].initial_rotation_self[2] = 0;
            sceneGraph.children[crossIndex].delta_rotation_self[2] = 0.1;
            sceneGraph.children[crossIndex].delta_rotation_self[1] = 0.0;

            this.playable = true;
        }
    }
}

//~* Global function declarations *~

function init() {
    // ~* Inicjalizacja WebGL2 *~
    {
        try {
            const canvas = document.querySelector("#glcanvas");
            gl = canvas.getContext("webgl2");
        }
        catch (e) {
            console.log(e.toString());
        }

        if (!gl) {
            alert("Unable to initialize WebGL.");
            return;
        }
    }

    //~* Setting up WebGL variables *~
    {
        gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.enable(gl.DEPTH_TEST);
        gl.enable(gl.CULL_FACE);
        gl.cullFace(gl.BACK);
        gl.enable(gl.BLEND);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    }

    //~* Set up the shaders and GPU program *~
    initShadersAndProgram();

    //~* Class Intialization *~
    //~* Get the Sampler *~
    let only_sampler = getSampler();

    Texture.numOfTextures = 0;  //Set the starting texture index to 0.
    texture_array = [];
    for (let i=0; i<texture_name_list.length; i++) texture_array.push(new Texture(texture_name_list[i]))
    //Bind THE sampler
    for (let i=0; i < texture_array.length; i++) gl.bindSampler(i, only_sampler);

    //~* View-Projection Matrix *~
    cameraObject = new ViewProjection();

    // pobranie ubi
    const matrices_ubi = gl.getUniformBlockIndex(program, "Matrices");
    const camera_ubi = gl.getUniformBlockIndex(program, "Camera");

    const light_ambient_ubi = gl.getUniformBlockIndex(program, "LightAmbientUniform");
    const light_directional_ubi = gl.getUniformBlockIndex(program, "LightDirectionalUniform");
    const light_point_ubi = gl.getUniformBlockIndex(program, "LightPointUniform");
    const light_spotlight_ubi = gl.getUniformBlockIndex(program, "LightSpotlightUniform");

    const material_ubi = gl.getUniformBlockIndex(program, "MaterialUniform");

    // przyporzadkowanie ubi do ubb
    let matrices_ubb = 0;
    gl.uniformBlockBinding(program, matrices_ubi, matrices_ubb);
    let camera_ubb = 1;
    gl.uniformBlockBinding(program, camera_ubi, camera_ubb);
    let light_ambient_ubb = 2;
    gl.uniformBlockBinding(program, light_ambient_ubi, light_ambient_ubb);
    let light_directional_ubb = 3;
    gl.uniformBlockBinding(program, light_directional_ubi, light_directional_ubb);
    let light_point_ubb = 4;
    gl.uniformBlockBinding(program, light_point_ubi, light_point_ubb);
    let light_spotlight_ubb = 5;
    gl.uniformBlockBinding(program, light_spotlight_ubi, light_spotlight_ubb);

    let material_ubb = 6;
    gl.uniformBlockBinding(program, material_ubi, material_ubb);

    // tworzenie UBO
    //--->> Matrices
    matrices_ubo = gl.createBuffer();
    gl.bindBuffer(gl.UNIFORM_BUFFER, matrices_ubo);
    gl.bufferData(gl.UNIFORM_BUFFER, 2*4*4*4, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.UNIFORM_BUFFER, null);
    //--->> The camera position
    camera_ubo = gl.createBuffer();
    gl.bindBuffer(gl.UNIFORM_BUFFER, camera_ubo);
    gl.bufferData(gl.UNIFORM_BUFFER, cameraObject.getCameraPosition(), gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.UNIFORM_BUFFER, null);
    //--->> Data for lights
    // ---$ Ambient Light
    light_ambient_ubo = gl.createBuffer();
    gl.bindBuffer(gl.UNIFORM_BUFFER, light_ambient_ubo);
    gl.bufferData(gl.UNIFORM_BUFFER, light_ambient_data, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.UNIFORM_BUFFER, null);

    // ---$ Directional Light
    light_directional_ubo = gl.createBuffer();
    gl.bindBuffer(gl.UNIFORM_BUFFER, light_directional_ubo);
    gl.bufferData(gl.UNIFORM_BUFFER, light_directional_data, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.UNIFORM_BUFFER, null);

    // ---$ Point Light
    light_point_ubo = gl.createBuffer();
    gl.bindBuffer(gl.UNIFORM_BUFFER, light_point_ubo);
    gl.bufferData(gl.UNIFORM_BUFFER, light_point_data, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.UNIFORM_BUFFER, null);

    // ---$ Spotlight
    light_spotlight_ubo = gl.createBuffer();
    gl.bindBuffer(gl.UNIFORM_BUFFER, light_spotlight_ubo);
    gl.bufferData(gl.UNIFORM_BUFFER, light_spotlight_data, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.UNIFORM_BUFFER, null);

    //--->> Material!
    material_ubo = gl.createBuffer();
    gl.bindBuffer(gl.UNIFORM_BUFFER, material_ubo);
    gl.bufferData(gl.UNIFORM_BUFFER, 4*4, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.UNIFORM_BUFFER, null);

    // ustawienia danych dla funkcji draw*
    gl.useProgram(program);
    gl.bindBufferBase(gl.UNIFORM_BUFFER, matrices_ubb, matrices_ubo);
    gl.bindBufferBase(gl.UNIFORM_BUFFER, camera_ubb, camera_ubo);
    // The Lights! (...) That thing I've created... it's gone D:! Who could've penetrated my impenetrable fortress ?!
    gl.bindBufferBase(gl.UNIFORM_BUFFER, light_ambient_ubb, light_ambient_ubo);
    gl.bindBufferBase(gl.UNIFORM_BUFFER, light_directional_ubb, light_directional_ubo);
    gl.bindBufferBase(gl.UNIFORM_BUFFER, light_point_ubb, light_point_ubo);
    gl.bindBufferBase(gl.UNIFORM_BUFFER, light_spotlight_ubb, light_spotlight_ubo);
    // The material
    gl.bindBufferBase(gl.UNIFORM_BUFFER, material_ubb, material_ubo);

    createSceneGraph();
    theGame = new NaughtsAndCrossesGame();
    window.onkeypress = handle_buttons;  //TODO: implement change on keydown and stop on key up
}

function handle_buttons(keyStruct){
    // console.log(keyStruct);
    switch (keyStruct.code){

        // ~* LIGHT CONTROL *~
        // case "KeyQ":
        //     singleLightArray[8] -= 0.1;
        //     break;
        // case "KeyW":
        //     singleLightArray[8] += 0.1;
        //     break;
        // case "KeyE":
        //     singleLightArray[9] -= 0.1;
        //     break;
        // case "KeyR":
        //     singleLightArray[9] += 0.1;
        //     break;
        // case "KeyA":
        //     singleLightArray[10] -= 0.1;
        //     break;
        // case "KeyS":
        //     singleLightArray[10] += 0.1;
        //     break;
        // case "KeyZ":
        //     singleLightArray[11] -= 0.1;
        //     break;
        // case "KeyX":
        //     singleLightArray[11] += 0.1;
        //     break;

        // ~* GAME CONTROL *~
        case "Numpad4":
            theGame.move_selected(-1, 0);
            break;
        case "Numpad6":
            theGame.move_selected(1, 0);
            break;
        case "Numpad2":
            theGame.move_selected(0, 1);
            break;
        case "Numpad8":
            theGame.move_selected(0, -1);
            break;
        case "Space":
        case "Enter":
            theGame.make_move();
            break;
        case "KeyL":
            theGame.reset_board();
            break;

        //~* Camera Controls *~
        case "KeyW":
            cameraObject.setCamera([cameraObject.camera_pos[0], cameraObject.camera_pos[1], cameraObject.camera_pos[2] - 0.1]);
            break;
        case "KeyS":
            cameraObject.setCamera([cameraObject.camera_pos[0], cameraObject.camera_pos[1], cameraObject.camera_pos[2] + 0.1]);
            break;
        case "KeyA":
            cameraObject.setCamera([cameraObject.camera_pos[0] - 0.1, cameraObject.camera_pos[1], cameraObject.camera_pos[2]]);
            break;
        case "KeyD":
            cameraObject.setCamera([cameraObject.camera_pos[0] + 0.1, cameraObject.camera_pos[1], cameraObject.camera_pos[2]]);
            break;
        case "KeyE":
            cameraObject.setCamera([cameraObject.camera_pos[0], cameraObject.camera_pos[1]  - 0.1, cameraObject.camera_pos[2]]);
            break;
        case "KeyQ":
            cameraObject.setCamera([cameraObject.camera_pos[0], cameraObject.camera_pos[1] + 0.1, cameraObject.camera_pos[2]]);
            break;


        case "KeyR":
            cameraObject.setCamera([1., 3., 5.]);
            break;

    }
    // console.log(singleLightArray);
    // console.log(theGame.board, theGame.selected);

}

function initShadersAndProgram(){
    // ~* kompilacja shader-ow *~
    // vertex shader (GLSL)
    let vs_source = document.getElementById("shader-vs").import.body.innerText;
    // let alt_vs_source = document.getElementById("alt-shader-vs").import.body.innerText;

    // fragment shader (GLSL)
    let fs_source = document.getElementById("shader-fs").import.body.innerText;
    let alt_fs_source = document.getElementById("alt-shader-fs").import.body.innerText;

    let vertex_shader = createShader(gl, gl.VERTEX_SHADER, vs_source);
    // let alt_vertex_shader = createShader(gl, gl.VERTEX_SHADER, alt_vs_source);
    let fragment_shader = createShader(gl, gl.FRAGMENT_SHADER, fs_source);
    let alt_fragment_shader = createShader(gl, gl.FRAGMENT_SHADER, alt_fs_source);
    program = createProgram(gl, vertex_shader, fragment_shader);
    alt_program = createProgram(gl, vertex_shader, alt_fragment_shader);

}

function createShader(gl, type, source)
{
    const shader = gl.createShader(type);
    gl.shaderSource(shader, source);
    gl.compileShader(shader);
    const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if(success)
    {
        return shader;
    }

    console.log(gl.getShaderInfoLog(shader));
    gl.deleteShader(shader);
}

function createProgram(gl, vertex_shader, fragment_shader)
{
    let program = gl.createProgram();
    gl.attachShader(program, vertex_shader);
    gl.attachShader(program, fragment_shader);
    gl.linkProgram(program);
    let success = gl.getProgramParameter(program, gl.LINK_STATUS);
    if(success)
    {
        return program;
    }

    console.log(gl.getProgramInfoLog(program));
    gl.deleteProgram(program);
}

function createSceneGraph(){
    let sceneGraphArray = [
        null,
        [1.0, 1.0, 1.0], [0.0, 0.0, 0.0],  // Scaling: initial and delta
        [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
        [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
        [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Self Rotation: initial and delta]
        [
            [new Thingy(pyramidVertices, pyramidIndices, [2,2], 1.0, 100.0, 0.75),
                [0.5, 0.5, 0.5], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [0.0, 0.0, 0.0], [0.0, -0.033, 0.0],  // Self Rotation: initial and delta
                []],
            [new Thingy(pyramidVertices, pyramidIndices, [2,2], 1.0, 100.0, 0.75),
                [0.5, 0.5, 0.5], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [1.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [0.0, 0.0, 0.0], [0.0, -0.033, 0.0],  // Self Rotation: initial and delta
                []],
            [new Thingy(pyramidVertices, pyramidIndices, [2,2], 1.0, 100.0, 0.75),
                [0.5, 0.5, 0.5], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [2.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [0.0, 0.0, 0.0], [0.0, -0.033, 0.0],  // Self Rotation: initial and delta
                []],
            [new Thingy(pyramidVertices, pyramidIndices, [2,2], 1.0, 100.0, 0.75),
                [0.5, 0.5, 0.5], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [0.0, 0.0, 1.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [0.0, 0.0, 0.0], [0.0, -0.033, 0.0],  // Self Rotation: initial and delta
                []],
            [new Thingy(pyramidVertices, pyramidIndices, [2, 2], 1.0, 100.0, 0.75),
                [0.5, 0.5, 0.5], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [1.0, 0.0, 1.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [0.0, 0.0, 0.0], [0.0, -0.033, 0.0],  // Self Rotation: initial and delta
                []],
            [new Thingy(pyramidVertices, pyramidIndices, [2,2], 1.0, 100.0, 0.75),
                [0.5, 0.5, 0.5], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [2.0, 0.0, 1.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [0.0, 0.0, 0.0], [0.0, -0.033, 0.0],  // Self Rotation: initial and delta
                []],
            [new Thingy(pyramidVertices, pyramidIndices, [2,2], 1.0, 100.0, 0.75),
                [0.5, 0.5, 0.5], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [0.0, 0.0, 2.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [0.0, 0.0, 0.0], [0.0, -0.033, 0.0],  // Self Rotation: initial and delta
                []],
            [new Thingy(pyramidVertices, pyramidIndices, [2,2], 1.0, 100.0, 0.75),
                [0.5, 0.5, 0.5], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [1.0, 0.0, 2.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [0.0, 0.0, 0.0], [0.0, -0.033, 0.0],  // Self Rotation: initial and delta
                []],
            [new Thingy(pyramidVertices, pyramidIndices, [2,2], 1.0, 100.0, 0.75),
                [0.5, 0.5, 0.5], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [2.0, 0.0, 2.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [0.0, 0.0, 0.0], [0.0, -0.033, 0.0],  // Self Rotation: initial and delta
                []],
            [new Thingy(crappyCubeVertices, crappyCubeIndices, [6,2], 1.0, 50.0, 0.95),
                [0.1, 0.1, 0.1], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [0.0, 1.5, 0.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [0.0, 0.0, 0.0], [1.0, 0.0, 1.0],  // Self Rotation: initial and delta
                []],
            [new Thingy(naughtVertices, naughtIndices, [3,3], 1.0, 200.0, 0.25),
                [1.0, 0.2, 1.0], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [-2.0, 0.0, -3.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [90.0, 0.0, 0.0], [0.0, 0.0, -0.1],  // Self Rotation: initial and delta
                []],
            [new Thingy(crossVertices, crossIndices, [4,4], 1.0, 200.0, 0.25),
                [1.0, 0.2, 1.0], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [4.0, 0.0, -3.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [90.0, 0.0, 0.0], [0.0, 0.0, 0.1],  // Self Rotation: initial and delta
                []],
            [new Thingy(tableclothVertices, tableclothIndices, [0,0], 1.0, 100.0, 0.00),
                [10.0, 1.0, 10.0], [0.0, 0.0, 0.0],  // Scaling: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Rotation: initial and delta
                [1.0, -2.0, 1.0], [0.0, 0.0, 0.0],  // Translation: initial and delta
                [0.0, 0.0, 0.0], [0.0, 0.0, 0.0],  // Self Rotation: initial and delta
                []]
        ]
    ];

    sceneGraph = new SceneNode(sceneGraphArray);
}

function draw(timestamp)  // The loop in which the drawing takes place
{
    gl.bindBuffer(gl.UNIFORM_BUFFER, matrices_ubo);
    // wyczyszczenie ekranu
    gl.clear(gl.COLOR_BUFFER_BIT);

    sceneGraph.draw(null, timestamp);

    window.requestAnimationFrame(draw);
}

function main()  // Initialize everything, then start drawing
{
    init();
    window.requestAnimationFrame(draw); //
}

main();  // Do the single thing - run the main function of the program
