#version 300 es

precision mediump float;
in vec3 fragment_coord;
in vec3 fragment_normal;
in vec2 fragment_tex_coord;
out vec4 frag_color;

void main()
{
    float sth = mod(fragment_tex_coord.y * 100.0, 10.0) / 20.0;
    float we =  mod(fragment_tex_coord.x * 100.0, 10.0) / 50.0;

    frag_color = vec4(sth, we, 0.0, 1.0);
}