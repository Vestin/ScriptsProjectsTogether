#version 300 es

precision mediump float;
in vec3 fragment_coord;
in vec3 fragment_normal;
in vec2 fragment_tex_coord;
out vec4 frag_color;
uniform sampler2D eye_texture;
uniform sampler2D some_other_texture;
layout(std140) uniform Camera
{
    vec3 cam_position;
};

struct LightDirectionalStruct{
    vec3 color;
    vec3 direction;
};

struct LightPointStruct{
    vec3 color;
    float radius;
    vec3 position;
};

struct LightSpotlightStruct{
    vec3 color;
    float radius;
    vec3 position;
    float cos_angle;
    vec3 direction;// Pointing TOWARDS the spotlight!
};

layout(std140) uniform LightAmbientUniform
{
    vec3 color;
} lightAmbient;

layout(std140) uniform LightDirectionalUniform
{
    float lightDirectionalCount;
    LightDirectionalStruct lightDirectional[3];
};

layout(std140) uniform LightPointUniform
{
    float lightPointCount;
    LightPointStruct lightPoint[3];
};

layout(std140) uniform LightSpotlightUniform
{
    float lightSpotlightCount;
    LightSpotlightStruct lightSpotlight[3];
};

layout(std140) uniform MaterialUniform
{
    vec3 material;
};

float linear_distance_multiplier(float distanceToLight, float radius);

void main()
{
    // Some useful variables
    float specular_intensity = material[0];
    float specular_power     = material[1];
    float metallicity        = material[2]; // - Hello, Tyrannosaurus Alan! I'm filling my shaders with metallicity!
                                            // - Eh o_0? https://www.youtube.com/watch?v=DppL5WtEW5c


    vec3 normal = normalize(fragment_normal);
    vec3 toCamera = normalize(cam_position - fragment_coord);
    vec3 half_vector;

    // The totality of colors to eventually use
    vec3 total_color;
    vec3 ambient_light_sum = vec3(0.0, 0.0, 0.0);
    vec3 diffused_light_sum = vec3(0.0, 0.0, 0.0);
    vec3 specular_light_sum = vec3(0.0, 0.0, 0.0);

    // Handle THE SINGLE AMBIENT light
    ambient_light_sum = lightAmbient.color;

    //Handle all DIRECTIONAL lights
    for (int i=0; i < int(lightDirectionalCount); i++){
        float directional_diffused_intensity = clamp(dot(-lightDirectional[i].direction, normal), 0.0, 1.0);
        diffused_light_sum += (directional_diffused_intensity * lightDirectional[i].color);

        half_vector = normalize(toCamera - lightDirectional[i].direction);
        float directional_specular_intensity =
          clamp(specular_intensity * pow( clamp(dot(half_vector, normal), 0.0, 1.0), specular_power ), 0.0, 1.0);

    //--> Metallicity kicks in!
        diffused_light_sum += (specular_intensity * directional_specular_intensity * lightDirectional[i].color) * metallicity;
        specular_light_sum += (specular_intensity * directional_specular_intensity * lightDirectional[i].color) * (1.0 - metallicity);
    }

    //Handle all POINT lights
    for (int i=0; i < int(lightPointCount); i++){
        vec3 toLight = normalize(lightPoint[i].position - fragment_coord);
        float distanceToLight = distance(fragment_coord, lightPoint[i].position);

        float distance_dampening = linear_distance_multiplier(distanceToLight, lightPoint[i].radius);

        float point_diffused_intensity = distance_dampening * clamp(dot(toLight, normal), 0.0, 1.0);
        diffused_light_sum += point_diffused_intensity * lightPoint[i].color;

        half_vector = normalize(toCamera + lightPoint[i].position);
        float point_specular_intensity =
        distance_dampening *
        clamp(specular_intensity * pow( clamp(dot(half_vector, normal), 0.0, 1.0), specular_power ), 0.0, 1.0);

    //--> Metallicity kicks in!
        diffused_light_sum += (specular_intensity * point_specular_intensity * lightPoint[i].color) * metallicity;
        specular_light_sum += (specular_intensity * point_specular_intensity * lightPoint[i].color) * (1.0 - metallicity);
    }

    //Handle all SPOTLIGHTS
    for (int i=0; i < int(lightSpotlightCount); i++){
        vec3 toLight = normalize(lightSpotlight[i].position - fragment_coord);
        float distanceToLight = distance(fragment_coord, lightSpotlight[i].position);

        float distance_dampening = linear_distance_multiplier(distanceToLight, lightSpotlight[i].radius);

        float spotlight_cos = dot(toLight, normalize(lightSpotlight[i].direction));
        float angle_dampening = max(0.0, (1.0 - ((1.0 - spotlight_cos) / (1.0 - (lightSpotlight[i].cos_angle)))));

        float spotlight_diffused_intensity =
                                     distance_dampening * angle_dampening * clamp(dot(toLight, normal), 0.0, 1.0);

        diffused_light_sum += spotlight_diffused_intensity * lightSpotlight[i].color;

        half_vector = normalize(toCamera + lightSpotlight[i].position);
        float spotlight_specular_intensity =
        distance_dampening * angle_dampening *
                clamp(specular_intensity * pow( clamp(dot(half_vector, normal), 0.0, 1.0), specular_power ), 0.0, 1.0);

    //--> Metallicity kicks in!
        diffused_light_sum += (specular_intensity * spotlight_specular_intensity * lightSpotlight[i].color) * metallicity;
        specular_light_sum += (specular_intensity * spotlight_specular_intensity * lightSpotlight[i].color) * (1.0 - metallicity);
    }

    vec3 texture_color  = vec3(texture(eye_texture, fragment_tex_coord));

    total_color = texture_color * (ambient_light_sum + diffused_light_sum) + specular_light_sum;

    frag_color = vec4(total_color, 1.0);
}
// Tak, w moim świecie jasność spada liniowo. Nie, nie chce mi się tego zmieniać.
float linear_distance_multiplier(float distanceToLight, float radius){
    return clamp((1.0 - (distanceToLight / radius)), 0.0, 1.0);
}