#version 300 es
// "location" musi byc takie same jak po stronie CPU!!!
in vec3 vertex_position;
in vec3 vertex_normal;
in vec2 vertex_tex_coord;

out vec3 fragment_coord;
out vec3 fragment_normal;
out vec2 fragment_tex_coord;
void main()
{
    gl_Position = vec4(fragment_coord, 1.0);
    fragment_coord = vertex_position;
    fragment_normal = vertex_normal;
    fragment_tex_coord = vertex_tex_coord;
}